__version__ = tuple([int(x) for x in
                     '$Revision: 1.3 $'.split()[1].split('.')])
__author__ = "Kael Fischer and Julio Carlos Menendez"

from types import *
import new

def getIterable(obj, filterNones=True):
    
    if obj is None:
        return []
    try:
        obj.__iter__
    except AttributeError:
        return (obj,)
    if filterNones:
        obj = filter(lambda x: x is not None, obj)
    return obj        

def flatten(l,ltypes=(ListType,TupleType)):       
    """flatten a n-dimentional sequence.

    by Mike C. Fletcher
    """
    ltype = type(l)
    l=list(l)
    i=0
    while i<len(l):
        while isinstance(l[i],ltypes):
            if not l[i]:
                l.pop(i)
                i-=1
                break
            else:
                l[i:i+1]=l[i]
        i+=1
    return ltype(l)

