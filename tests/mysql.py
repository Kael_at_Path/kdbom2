import unittest
import random

import MySQLdb

from kdbom2.drivers import mysql
from __init__ import *


class TestMysqlDriver(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        self.conn = MySQLdb.Connection(read_default_file='~/.my.cnf')
        setupTestDB()
        self.db = mysql.DB(db=DB_NAME, defaultsFile='~/.my.cnf')
        self.table = self.db[TABLE_NAME]

    def setUp(self):
        """
        """
        self.cursor = mysql.Cursor(self.db)

    def testInsertAndDelete(self):
        """
        """
        qry = mysql.Query(self.table)
        num_rows = random.randint(0, 1000)
        values = [(i, random.randint(1000, 3000)) \
                  for i in xrange(num_rows)]
        nativeCursor = self.conn.cursor()
        for pair in values:
            qry.insert(pairs={self.table.field1: pair[0],
                              self.table.field2: pair[1]})
            self.cursor.execute(str(qry))
            qry.reset()

            sql = """SELECT field2 FROM %s
            WHERE field1 = %d""" % (self.table, pair[0])
            nativeCursor.execute(sql)
            row = nativeCursor.fetchone()
            self.assertEqual(pair[1], row[0])

            qry.delete()
            qry.where(criteria={self.table.field1: pair[0]})
            self.cursor.execute(str(qry))
            qry.reset()

            sql = """SELECT COUNT(*) FROM %s
            WHERE field1 = %d""" % (self.table, pair[0])
            nativeCursor.execute(sql)
            row = nativeCursor.fetchone()
            self.assertEqual(row[0], 0)

    def testUpdate(self):
        """
        """
        qry = mysql.Query(self.table)
        num_rows = random.randint(0, 1000)
        values = [(i, random.randint(1000, 3000)) \
                  for i in xrange(num_rows)]
        nativeCursor = self.conn.cursor()
        for pair in values:
            qry.insert(pairs={self.table.field1: pair[0],
                              self.table.field2: pair[1]})
            self.cursor.execute(str(qry))
            qry.reset()

            newVal = random.randint(4000, 6000)
            qry.update(pairs={self.table.field2: newVal})
            qry.where(criteria={self.table.field1: pair[0]})
            self.cursor.execute(str(qry))
            qry.reset()

            qry.select([], self.table.field2)
            qry.where(criteria={self.table.field1: pair[0]})
            row = self.cursor.fetchone(str(qry))
            qry.reset()
            
            self.assertEqual(row[0], newVal)


suite = unittest.TestLoader().loadTestsFromTestCase(TestMysqlDriver)                    

if __name__ == '__main__':
    unittest.main()
