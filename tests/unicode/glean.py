from kdbom2 import kdbom

stDB=kdbom.tryDBconnect('Social_Track',use_unicode=True,
                        verboseFailure=True,
                        read_default_file='/r1/home/julio/.my.cnf',
                        debugConnect=True, serversPorts=(('kf-db1', 3306),))
stDB.connection.set_character_set('utf8')
stDB.execute('SET NAMES utf8')
stDB.execute('SET CHARACTER SET utf8')
stDB.execute('SET character_set_connection=utf8')

class User (kdbom.KSqlObject):
    """
    """
    _table=stDB.T_User
    _strField=_table.Screen_Name

class Status (kdbom.KSqlObject):
    """
    """
    _table=stDB.T_Status

    @classmethod
    def insertStatus(cls,status):
        """Take a tweepy status object and insert it,
        and return the object.
        """
        try:
            author=User(ID_String=status.author.id_str)
        except kdbom.KdbomLookupError:
            author=User(
                ID_String=status.author.id_str,
                Created_At=status.author.created_at ,
                Profile_Txt_Color=status.author.profile_text_color,
                Statuses_Count=status.author.statuses_count,
                URL=status.author.url,
                Location=status.author.location,
                Description=status.author.description,
                Screen_Name=status.author.screen_name,
                Name=status.author.name,
                Followers=status.author.followers_count,
                insertOnly=True)

        rv = cls(
            ID_String=status.id_str,
            Created_At=status.created_at ,
            Author_ID=author.ID(),
            Txt=status.text,
            Source=status.source,
            Source_URL=status.source_url,
            Retweet_Count=status.retweet_count,
            Reply_To_Status=status.in_reply_to_status_id_str,
            Reply_To_User=status.in_reply_to_user_id_str,
            insertOnly=True)
        return rv

class EntityType (kdbom.KSqlObject):
    """
    """
    _table=stDB.T_Entity_Type


class Entity (kdbom.KSqlObject):
    """
    """
    _table=stDB.T_Entity

