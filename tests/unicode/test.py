#!/usr/local/bin/python

import shelve
import glean

wdb=shelve.open('wind.db')


def insertStatuses():
    """Populate tables with tweets in wind.db
    """
    for s in wdb.itervalues():
        glean.Status.insertStatus(s)
        
        
if __name__ == '__main__':
    insertStatuses()
