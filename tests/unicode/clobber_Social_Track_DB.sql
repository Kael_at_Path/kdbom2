-- MySQL dump 10.13  Distrib 5.5.15, for FreeBSD9.0 (amd64)
--
-- Host: marble    Database: Social_Track
-- ------------------------------------------------------
-- Server version	5.5.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `Social_Track`
--

/*!40000 DROP DATABASE IF EXISTS `Social_Track`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `Social_Track` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `Social_Track`;

--
-- Table structure for table `T_Entity`
--

DROP TABLE IF EXISTS `T_Entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_Entity` (
  `T_Entity_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `T_Entity_Type_ID` int(10) unsigned NOT NULL,
  `Attribute_Value` varchar(2048) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`T_Entity_ID`),
  KEY `fk_Entity_T_Entity_Type1` (`T_Entity_Type_ID`),
  CONSTRAINT `fk_Entity_T_Entity_Type1` FOREIGN KEY (`T_Entity_Type_ID`) REFERENCES `T_Entity_Type` (`T_Entity_Type_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `T_Entity_Type`
--

DROP TABLE IF EXISTS `T_Entity_Type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_Entity_Type` (
  `T_Entity_Type_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Stored_Attribute` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`T_Entity_Type_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `T_Status`
--

DROP TABLE IF EXISTS `T_Status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_Status` (
  `T_Status_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Author_ID` int(10) unsigned NOT NULL,
  `ID_String` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Txt` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Created_At` datetime DEFAULT NULL,
  `Source` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Source_URL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Retweet_Count` int(10) unsigned DEFAULT NULL,
  `Reply_To_Status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Reply_To_User` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`T_Status_ID`),
  KEY `Author` (`Author_ID`),
  CONSTRAINT `Author` FOREIGN KEY (`Author_ID`) REFERENCES `T_User` (`T_User_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `T_Status_has_Entity`
--

DROP TABLE IF EXISTS `T_Status_has_Entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_Status_has_Entity` (
  `T_Status_ID` int(10) unsigned NOT NULL,
  `Entity_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`T_Status_ID`,`Entity_ID`),
  KEY `fk_T_Status_has_Entity_Entity1` (`Entity_ID`),
  KEY `fk_T_Status_has_Entity_T_Status1` (`T_Status_ID`),
  CONSTRAINT `fk_T_Status_has_Entity_Entity1` FOREIGN KEY (`Entity_ID`) REFERENCES `T_Entity` (`T_Entity_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_T_Status_has_Entity_T_Status1` FOREIGN KEY (`T_Status_ID`) REFERENCES `T_Status` (`T_Status_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `T_User`
--

DROP TABLE IF EXISTS `T_User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `T_User` (
  `T_User_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_String` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Created_At` datetime DEFAULT NULL,
  `Profile_Txt_Color` char(6) CHARACTER SET utf8 DEFAULT NULL,
  `Statuses_Count` int(10) unsigned DEFAULT NULL,
  `URL` varchar(2048) CHARACTER SET utf8 DEFAULT NULL,
  `Location` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Screen_Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Followers` int(11) DEFAULT NULL,
  PRIMARY KEY (`T_User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-12-08 11:48:50
