import sys
from functools import partial
import types
import random
import re
import unittest

import kdbom2
from kdbom2 import kdbom
from kdbom2.query import *
from kdbom2 import exceptions

DB_NAME = 'kdbom2_test_db'
DB_HOST = 'kf-db1'
DB_PORT = 3306

servers = ((DB_HOST, DB_PORT),)
DB = kdbom.tryDBcreate(DB_NAME, serversPorts=servers,
                       use_unicode=True)

class BaseTestCase(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        try:
            self.db = DB
            neededTables = ['test_table1', 'test_table2',
                            'test_table3', 'Item', 'Tag',
                            'Item_has_Tag']
            t = filter(lambda x: x in self.db.tables, neededTables)
            if len(t) != len(neededTables):
                self._createTestDatabase(self.db)
        except exceptions.KdbomDatabaseError:
            self.db = self._createTestDatabase()
        unittest.TestCase.__init__(self, *args, **kwargs)

    def _createTestDatabase(self, db=None):
        if db is None:
            db = DB
        sqlFilename = '%s_database.sql' % kdbom2.driver.NAME
        sqlContent = open(sqlFilename).read()
        for sqlCommand in sqlContent.split(';\n\n'):
            if len(sqlCommand.strip()) == 0:
                continue
            db.execute(sqlCommand)
        return db

    def _generateString(self, length=8):
        letters = 'abcdefghijklmnopqrstuvwxz'
        if length > len(letters):
            letters = letters * ((length/len(letters)) + 1)
        return ''.join(random.sample(letters, length))

    def _generateInteger(self):
        return random.randint(1, 1000)

    def _generateForeignId(self, table2):
        q = table2.query()
        q.selectID()
        q.orderBy(RAND())
        q.limit(1)
        rows = q.fetchall()
        return rows[0][0]

    _generateNone = lambda self: None

    def _findFieldInRelationships(self, field, table):
        rels = table.relationships
        for ftable, rel in rels.iteritems():
            if field == rel.child:
                return ftable
        return False

    def _populateTable(self, table, number=1000):
        fieldsGenerators = []
        fieldNames = []
        for fieldname in table.field_names:
            field = table.fields[fieldname]
            if field == table.primary_key:
                continue
            fieldNames.append(fieldname)
            fType = ''
            if field.isString:
                fieldsGenerators.append(self._generateString)
            elif field.isNumeric:
                ftable = self._findFieldInRelationships(field, table)
                if ftable:
                    fun = partial(self._generateForeignId, ftable)
                    fieldsGenerators.append(fun)
                else:
                    fieldsGenerators.append(self._generateInteger)
            else:
                fieldsGenerators.append(self._generateNone)

        values = [[g() for g in fieldsGenerators]
                  for x in xrange(number)]
        table.insertMany(fieldNames, values)
        return values

    def assertEqualRows(self, rowlist1, rowlist2):
        invalidLengthMsg = ('Length of row lists are different. '
                            '%d != %d')
        
        if type(rowlist1) in (types.ListType, types.TupleType):
            rl1Length = len(rowlist1)
            rowlist1gen = (x for x in rowlist1)
        elif type(rowlist1) is types.GeneratorType:
            rl1Length = None
            rowlist1gen = rowlist1
        else:
            raise Exception, ('Both arguments must be list, tuple or '
                              'generator type.')
        
        if type(rowlist2) in (types.ListType, types.TupleType):
            rl2Length = len(rowlist2)
            rowlist2gen = (x for x in rowlist2)
        elif type(rowlist2) is types.GeneratorType:
            rl2Length = None
            rowlist2gen = rowlist2
        else:
            raise Exception, ('Both arguments must be list, tuple or '
                              'generator type.')
        
        if rl1Length is not None and rl2Length is not None \
               and rl1Length != rl2Length:
            raise self.failureException((invalidLengthMsg %
                                         (rl1Length, rl2Length)))

        row1stopped = False
        row2stopped = False
        row1count, row2count = 0, 0
        while True:
            try:
                row1 = tuple(rowlist1gen.next())
                row1count += 1
            except StopIteration:
                row1stopped = True

            try:
                row2 = tuple(rowlist2gen.next())
                row2count += 1
            except StopIteration:
                row2stopped = True

            if row1stopped and row2stopped:
                break

            if (row1stopped and not row2stopped) \
                   or (row2stopped and not row1stopped):
                raise self.failureException((invalidLengthMsg %
                                             (row1count, row2count)))

            if row1 != row2:
                msg = ('Row lists are different. First element '
                       'that differs: %s != %s' % (str(row1),
                                                   str(row2)))
                raise self.failureException(msg)

    def assertRegex(self, text, expected_regex, msg=None):
        """Fail the test unless the text matches the regular expression."""
        if isinstance(expected_regex, str):
            assert expected_regex, "expected_regex must not be empty."
            expected_regex = re.compile(expected_regex)
        if not expected_regex.search(text):
            msg = msg or "Regex didn't match"
            msg = '%s: %r not found in %r' % (msg, expected_regex.pattern, text)
            raise self.failureException(msg)
