CREATE TABLE IF NOT EXISTS `test_table2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field1` int(11) DEFAULT NULL,
  `field2` int(11) DEFAULT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `id` (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `test_table1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field1` int(11) DEFAULT NULL,
  `field2` int(11) DEFAULT NULL,
  `field3` varchar(255) DEFAULT '',
  `test_table2_id` int(11) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `id` (`id`),
  KEY `test_table2_id` (`test_table2_id`),
  CONSTRAINT `test_table1_ibfk_1` FOREIGN KEY (`test_table2_id`) REFERENCES `test_table2` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `test_table3` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `Item` (
  `Item_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `Item_has_Tag` (
  `Item_has_Tag_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) NOT NULL,
  `Tag_ID` int(11) NOT NULL,
  PRIMARY KEY (`Item_has_Tag_ID`),
  KEY `fk_Item_has_Tag_Tag1` (`Tag_ID`),
  KEY `fk_Item_has_Tag_Item1` (`Item_ID`),
  CONSTRAINT `fk_Item_has_Tag_Item1` FOREIGN KEY (`Item_ID`) REFERENCES `Item` (`Item_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_has_Tag_Tag1` FOREIGN KEY (`Tag_ID`) REFERENCES `Tag` (`Tag_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `Tag` (
  `Tag_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Tag_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

