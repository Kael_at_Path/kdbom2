from kdbom2.query import *
from __init__ import *


class TestQuery(BaseTestCase):
    def __init__(self, *args, **kwargs):
        BaseTestCase.__init__(self, *args, **kwargs)
        self.table1 = self.db.test_table1
        self.table2 = self.db.test_table2
        
    def tearDown(self):
        self.table2.delete()

    def testSelect(self):
        query = self.table2.query()
        query.select((self.table2.field1, self.table2.field2))
        self.assertRegex(str(query), '^SELECT.*\s+FROM')

        values = self._populateTable(self.table2,
                                     random.randint(100, 1000))
        values = map(lambda x: x[:-1], values)
        rows = query.fetchall()
        self.assertEqualRows(values, rows)

    def testSelectDistinct(self):
        query = self.table2.query()
        query.selectDistinct(self.table2.field2)
        self.assertRegex(str(query),
                         '^SELECT\s+DISTINCT.*\s+FROM')

        values = self._populateTable(self.table2,
                                     random.randint(100, 1000))
        uniqueVals = set([])
        distinctVals = []
        for val in values:
            if val[1] not in uniqueVals:
                uniqueVals.add(val[1])
                distinctVals.append([val[1],])
            
        rows = query.fetchall()
        self.assertEqualRows(distinctVals, rows)

    def testSelectCount(self):
        query = self.table2.query()
        query.selectCount()
        self.assertRegex(str(query), '^SELECT\s+COUNT.*\s+FROM')

        values = self._populateTable(self.table2,
                                     random.randint(100, 1000))
        self.assertEqual(len(values), query.fetchall()[0][0])

    def testSelectCountDistinct(self):
        query = self.table2.query()
        query.selectCountDistinct(self.table2.field2)
        self.assertRegex(str(query),
                         '^SELECT\s+COUNT\(DISTINCT\(.*\s+FROM')

        values = self._populateTable(self.table2,
                                     random.randint(100, 1000))
        uniqueVals = set(map(lambda x: x[1], values))
        self.assertEqual(len(uniqueVals), query.fetchall()[0][0])

    def testSelectGroupBy(self):
        query = self.table2.query()
        query.selectGroupBy(self.table2.field2)
        self.assertRegex(str(query),
                         '^SELECT\s+.*\s+\nFROM.*\nGROUP BY\s+')
        
        query.select(COUNT('*'))
        values = self._populateTable(self.table2,
                                     random.randint(100, 1000))
        distinctValsCount = {}
        for val in values:
            if val[1] not in distinctValsCount:
                distinctValsCount[val[1]] = 0
            distinctValsCount[val[1]] += 1

        for row in query.fetchall():
            self.assertEqual(row[1], distinctValsCount[row[0]])

    def testCountBy(self):
        query = self.table2.query()
        query.countBy(self.table2.field2)
        self.assertRegex(str(query),
                         '^SELECT\s+.*,COUNT.*\s+\nFROM.*\nGROUP BY\s+')

        values = self._populateTable(self.table2,
                                     random.randint(100, 1000))
        distinctValsCount = {}
        for val in values:
            if val[1] not in distinctValsCount:
                distinctValsCount[val[1]] = 0
            distinctValsCount[val[1]] += 1

        for row in query.fetchall():
            self.assertEqual(row[1], distinctValsCount[row[0]])


suite = unittest.TestLoader().loadTestsFromTestCase(TestQuery)                    

if __name__ == '__main__':
    unittest.main()
