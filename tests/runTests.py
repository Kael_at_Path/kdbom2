#!/usr/bin/env python

import unittest

import kdbom2
import kdbom
import query

if __name__ == '__main__':
    suite = unittest.TestSuite([kdbom.suite, query.suite])
    unittest.TextTestRunner(verbosity=2).run(suite)
