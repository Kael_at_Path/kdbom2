import os
import sys
import unittest
import difflib
import random
from StringIO import StringIO

import kdbom2
from kdbom2 import kdbom, adapter
from kdbom2.drivers import sqlite
from kdbom2.exceptions import *

import plantChip


def calculateDiff(fileName1, fileName2):
    k1lines = open(fileName1).readlines()
    k2lines = open(fileName2).readlines()
    return ''.join(difflib.unified_diff(k1lines, k2lines))

def setupNewDBMySQL():
    newDB = None
    dbName = ''
    while newDB is None:
        dbName = 'ChipDesign_UPV_test_'
        dbName += ''.join(map(chr, random.sample(range(97, 123), 5)))
        try:
            plantChip.dDB.execute('CREATE DATABASE %s' % dbName)
            newDB = kdbom.tryDBconnect(dbName,
                                       reuseDBconnection=plantChip.dDB)
        except Exception, e:
            continue

    queries = ["""USE %s""" % dbName,
    """SET FOREIGN_KEY_CHECKS=0""",
    """CREATE TABLE `Assignment` (
    `Assignment_ID` int(11) NOT NULL AUTO_INCREMENT,
    `Candidate_ID` int(11) NOT NULL DEFAULT '0',
    `Taxon_ID` int(11) DEFAULT NULL,
    `Energy_Cutoff` float(9,3) NOT NULL,
    `Chip_ID` int(11) NOT NULL DEFAULT '1',
    PRIMARY KEY (`Assignment_ID`),
    UNIQUE KEY `Ref_01` (`Candidate_ID`,`Energy_Cutoff`,`Chip_ID`),
    KEY `taxon_chip_e` (`Taxon_ID`,`Chip_ID`,`Energy_Cutoff`),
    KEY `Ref_02` (`Chip_ID`),
    CONSTRAINT `Ref_01` FOREIGN KEY (`Candidate_ID`) REFERENCES `Candidate` (`Candidate_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `Ref_02` FOREIGN KEY (`Chip_ID`) REFERENCES `Chip` (`Chip_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    """,
    """CREATE TABLE `Candidate` (
    `Candidate_ID` int(11) NOT NULL AUTO_INCREMENT,
    `Uid` varchar(255) NOT NULL,
    `HSP_Table_Name` varchar(255) DEFAULT 'Hsp_MaxTax',
    `Host_Grp_30` tinyint(3) unsigned DEFAULT NULL,
    `Host_Grp_20` tinyint(3) unsigned DEFAULT NULL,
    `Batch` tinyint(3) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (`Candidate_ID`),
    UNIQUE KEY `new_index1` (`Uid`),
    KEY `batch` (`Batch`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    """,
    """CREATE TABLE `Chip` (
    `Chip_ID` int(11) NOT NULL AUTO_INCREMENT,
    `Name` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`Chip_ID`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    """,
    """INSERT INTO `Chip` (`Chip_ID`, `Name`)
    VALUES (1, 'test KDBOM');
    """,
    """CREATE TABLE `Chosen` (
    `Chosen_ID` int(11) NOT NULL AUTO_INCREMENT,
    `Design_Run_ID` int(11) NOT NULL DEFAULT '0',
    `Assignment_ID` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`Chosen_ID`),
    KEY `Ref_04` (`Design_Run_ID`),
    KEY `Ref_05` (`Assignment_ID`),
    CONSTRAINT `Ref_04` FOREIGN KEY (`Design_Run_ID`) REFERENCES `Design_Run` (`Design_Run_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `Ref_05` FOREIGN KEY (`Assignment_ID`) REFERENCES `Assignment` (`Assignment_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    """,
    """CREATE TABLE `Design_Run` (
    `Design_Run_ID` int(11) NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`Design_Run_ID`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    """,
    """INSERT INTO `Design_Run` (`Design_Run_ID`) VALUES (1);
    """,
    """CREATE TABLE `Hsp_MaxTax` (
    `Q` varchar(255) NOT NULL,
    `T_GI` bigint(20) unsigned NOT NULL,
    `Taxon_ID` bigint(20) unsigned NOT NULL,
    `Ident` float unsigned NOT NULL,
    `Length` int(10) unsigned NOT NULL,
    `Gaps` int(10) unsigned NOT NULL,
    `Mismatch` int(10) unsigned NOT NULL,
    `Q_Start` int(10) unsigned NOT NULL,
    `Q_End` int(10) unsigned NOT NULL,
    `T_Start` int(10) unsigned NOT NULL,
    `T_End` int(10) unsigned NOT NULL,
    `E` double unsigned NOT NULL,
    `Score` float unsigned NOT NULL,
    `dG` float DEFAULT NULL,
    `Gi_Count` int(11) DEFAULT NULL,
    PRIMARY KEY (`Q`,`Taxon_ID`),
    KEY `targetSE` (`T_GI`,`T_Start`,`T_End`),
    KEY `qgi` (`Q`(25),`Taxon_ID`,`dG`) USING BTREE,
    KEY `tgi` (`Taxon_ID`,`Q`(25),`dG`) USING BTREE
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    """,
    """SET FOREIGN_KEY_CHECKS=1""",]

    for qry in queries:
        newDB.execute(qry)
    newDB.refresh()

    return newDB

def setupNewDBSqlite3():
    stream = StringIO()
    adapter.toSqlite(plantChip.dDB, stream)
    sql = stream.getvalue().split(';')
    
    kdbom2.driver = sqlite
    reload(kdbom)

    dbName = ''
    while True:
        dbName = 'ChipDesign_UPV_test_'
        dbName += ''.join(map(chr, random.sample(range(97, 123), 5)))
        dbName += '.db'
        if os.path.exists(dbName):
            continue
        break
    
    newDB = kdbom.tryDBconnect(dbName)                           
    for qry in sql:
        newDB.execute(qry)
    newDB.refresh()
    return newDB

class ChipDesignTest(unittest.TestCase):
    def test1MakeCandidateTable(self):
        import makeCandidateTable
        makeCandidateTable.makeCandidateTable(True)
        d = calculateDiff('makeCandidateTable.py.k1.sql',
                          'testFile.k2.sql')
        self.assertEqual(len(d), 0)

    def test2InsertMaxTax(self):
        import insertGridBlast_dG_MaxTax
        insertGridBlast_dG_MaxTax.main(['ncbiCandidates_29.fasta.mbr.dG'],
                                       True)
        d = calculateDiff('insertGridBlast_dG_MaxTax.py.k1.sql',
                          'testFile.k2.sql')
        self.assertEqual(len(d), 0)

    def test3assignCandidates(self):
        import assignCandidates
        assignCandidates.main(True)
        d = calculateDiff('assignCandidates.py.k1.sql',
                          'testFile.k2.sql')
        self.assertEqual(len(d), 0)

    def test4chooseProbes(self):
        import chooseProbes
        f = open('testFile.out', 'w')
        chooseProbes.chooseProbes(f)
        f.close()
        d = calculateDiff('chooseProbes.py.k1.out', 'testFile.out')
        self.assertEqual(len(d), 0)

    def test5recordChoices(self):
        import recordChoices
        c, uids = recordChoices.recordChoices(['testFile.out'])
        f = open('tmp.out', 'w')
        print >> f, c
        print >> f, uids
        f.close()
        d = calculateDiff('recordChoices.py.k1.out', 'tmp.out')
        self.assertEqual(len(d), 0)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        db_system = sys.argv[1]
        del sys.argv[1]
    else:
        db_system = 'sqlite3'
    if db_system == 'mysql':
        newDB = setupNewDBMySQL()
    elif db_system == 'sqlite3':
        newDB = setupNewDBSqlite3()
        
    plantChip.dDB = newDB
    plantChip.CD.setDesignDB(newDB)

    unittest.main()
