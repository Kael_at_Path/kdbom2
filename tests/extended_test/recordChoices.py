#!/usr/local/bin/python 

import os
import sys

from plantChip import *

from utils import multiFile


ENERGY_CUTOFF = -30
chip = CD.defaultChip
Design_Run_ID=1


def getAssignmentID(chip,uid):
    """
    """
    Cid = CD.Candidate(uid).ID()
    return CD.Assignment(Candidate_ID=Cid,
                         Chip_ID = chip,
                         Energy_Cutoff = ENERGY_CUTOFF).ID()

def recordChoices(files=sys.argv[1:]):
    uids = []

    for l in multiFile(files):
        if l.startswith('>'):
            uids.append(l[1:-1])

    dDB.execute("TRUNCATE TABLE %s" % dDB.Chosen.dbQualName())

    CD.Chosen.insertMany(('Design_Run_ID','Assignment_ID'),uids,
                         callback=lambda x: (Design_Run_ID,getAssignmentID(chip,x)),
                         ignore=True)

    return (CD.Chosen.count(), uids)


if __name__ == '__main__':
    c, uids = recordChoices()
    print c, uids







