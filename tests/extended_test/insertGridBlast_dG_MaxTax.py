#!/usr/local/bin/python 
#
# 
#
import sys
import uTax

from plantChip import dDB,srTax, dump_dDB, kdbom


def rowGen (inPath):
    """
    """
    f=file(inPath)

    for l in f:
        x = l.strip().split('\t')
        yield (x[0],
               int(x[1]),
               float(x[2]),
               int(x[3]), 
               int(x[4]),
               int(x[5]),
               int(x[6]),
               int(x[7]),
               int(x[8]),
               int(x[9]),
               float(x[10]),
               float(x[11]),
               float(x[12]))


def addTaxID(row):
    x=list(row)
    gi= x[1]
    taxID = srTax.gi2taxid(gi)
    if taxID == None:
        taxID = 0
    return tuple(x[:2] + [taxID] + x[2:])


def addGiCount(row):
    x=list(row[0])
    giCt = len(row[1])
    x.append(giCt)
    return tuple(x)


def loadHsp_dGFile(tbl,dgPath):
    """
    """
    minEnergyRows={}
    for r in rowGen(dgPath):
        r=addTaxID(r)
        k=tuple([r[0],r[2]])
        e = r[12]
        gi =r[1]
        try:
            e0 = minEnergyRows[k][0][12]
            if e<e0:
                minEnergyRows[k][0]=r
            minEnergyRows[k][1].add(gi)
        except KeyError:
            minEnergyRows[k]=[r,set([gi])]
            

    print dgPath, len(minEnergyRows)
    print len(tbl.field_names), len((minEnergyRows.values()[0][0]))
    return tbl.insertMany(
        tbl.field_names,
        minEnergyRows.values(),
        callback=addGiCount,
        ignore=True,chunkSize=30000,
        partialCommits=True,
        )
    

def main(args, test=False):
    tbl=dDB.Hsp_MaxTax
    dDB.execute("TRUNCATE TABLE %s" % tbl._dbQualName())

    for f in args:
        loadHsp_dGFile(tbl,f)

    fname = None
    if test:
        fname = 'testFile'

    dump_dDB('k2', fname, tbl.name) 


  
####### LEAVE THIS ALONE ###########
# If run directly as a program
# this calls the main function.
# 
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))


