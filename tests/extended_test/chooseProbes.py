#!/usr/local/bin/python 

import os
import sys

from plantChip import *

from utils import batchList

NUMBATCHES=100
ECUTOFF = -30
COVERAGE = 10
MAX_TAX_OLIGOS = 25
ASSIGNMENT_FILTERS = (
    (lambda a: a.candidate().Host_Grp_20 <= 2,'Grp_20 1,2'),
    (lambda a: a.candidate().Host_Grp_30 <= 2,'Grp_30 1,2'))

def chooseProbes(output=sys.stdout):
    taxIDs = dDB.Assignment.Taxon_ID.distinctValues()
    taxIDs.sort()

    try:
        batchNum = int(sys.argv[-1])
    except ValueError:
        try:
            batchNum = int(os.environ['SGE_TASK_ID'])-1
        except KeyError:
            batchNum = None

    try:
        taxBatch = batchList(taxIDs,batchCount=NUMBATCHES)[batchNum]
    except:
        #raise
        #taxBatch = [CD.DesignTaxon(Name='Rhinovirus',Taxonomy_ID=15).ID()]
        taxBatch = taxIDs

    taxCt = 0
    print >> output, '### beep ###'
    for txnID in taxBatch:
        print >> output, txnID
        taxCt+=1
        txn=CD.DesignTaxon(txnID)
        print >> output, '=========='
        print >> output, "TAXON: %s" % txn
        print >> output, "RANK: %s\n" % txn.rank()
        txn.desiredCoverage=COVERAGE

        for aFilter,filterDesc in ASSIGNMENT_FILTERS:
            if txn.done:
                break
            print >> output, "Filter: %s"%filterDesc
            txn.setAssignmentPool(ECUTOFF,assFilterCallback=aFilter)
            print >> output, "POOL SIZE: %s"% len(txn.assignmentPool)
            while txn.chooseNext() != None:
                print >> output, txn.oligos[-1].candidate().fastaRecord()
                print >> output, ''

                if len(txn.oligos) >= MAX_TAX_OLIGOS:
                    txn.done
                    break

        print >> output, "REMANING POOL SIZE: %s"% len(txn.assignmentPool)
        print >> output, "FINAL OLIGO COUNT: %s" % len(txn.oligos)
        print >> output, "FINAL OLIGO PARENT GIs: %s " % txn.oligoParents
        print >> output, "FINAL BRANCH REPRESENTATION:\n%s" % txn.coverageStr('\t')

    print >> output, "# %s TAXA PROCESSED" % taxCt


if __name__ == '__main__':
    chooseProbes()
