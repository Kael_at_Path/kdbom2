#!/usr/local/bin/python 

import os
import sys

from plantChip import *


def main(test=False):

    dDB.execute("TRUNCATE TABLE %s" % dDB.Assignment.dbQualName())

    try:
        batchNum = int(sys.argv[-1])
    except ValueError:
        try:
            batchNum = int(os.environ['SGE_TASK_ID'])-1
        except KeyError:
            batchNum = 0

    energyCutoffs = (-20,-30,-40)

    for c in CD.Candidate.generator(Batch=batchNum):
        print c
        #print c.deleteAssignments()
        for dG in energyCutoffs:
            try:
                print dG, c.assignment(dG).taxon()
            except AttributeError, e:
                print dG, None

    fname = None
    if test:
        fname = 'testFile'

    dump_dDB('k2', fname, dDB.Assignment.name)


if __name__ == '__main__':
    main()
