#!/usr/local/bin/python
#
#
import re

import os
import sys

from kdbom2 import kdbom
from kdbom2.exceptions import *
from utils import multiFile

import uTax
from uTax import chipDesign as CD

dDB= kdbom.tryDBconnect('ChipDesign_UPV_K2',
                        fatal=True,
                        verboseFailure=True,
                        reuseDBconnection=uTax.utDB
                        )

srTax=uTax.Taxonomy('UTax 0.3')
CD.setDesignDB(dDB)

def dump_dDB(tag, fileName=None, table=''):
    if fileName is None:
        fileName = sys.argv[0]
    cmd = ('mysqldump --skip-dump-date --skip-comments -h kf-db1 %s %s > %s.%s.sql'%
           (dDB.name, table, fileName, tag))
    os.system(cmd)

def setDB(dbObj):
    CD.setDesignDB(dbObj)
    dDB = dbObj


def fixSpecialRecTitles(fd):
    """
    """
    for k,v in fd.items():

        tmpKey=k
        for eWrd in ('sequences','-at-','UCDavis',
                     'generated','complete','incomplete',
                     ',','.'):
            tmpKey=tmpKey.replace(eWrd,'')

        tmpKey=re.sub('-+','-',tmpKey)
        tmpKey=re.sub('\(.+\)','',tmpKey)

        newKey=tmpKey.split('/')[-1][:20]
        if len(newKey) == 3:
            newKey='-'.join((tmpKey.split('/')[-2:]))
            newKey=re.sub('-+','-',newKey)
        newKey=newKey.rstrip('-')
        newKey=newKey.rstrip('_')
            
        i=1            
        while newKey in fd:
            if '%s-%s' % (newKey,i) not in fd:
                newKey = '%s-%s' % (newKey,i) 
            i+=1
        fd[newKey]=fd[k]
        del fd[k]
        fd[newKey].title=newKey
    return fd


class ViroBlast (object):
    """
    """

    def __init__(self,mbrFiles=None,qFasta=None):
        """
        """
        self.hitDict={}
        self.fastaD = {}
        
        if mbrFiles != None:
            self.loadMBRs(mbrFiles)

        if qFasta != None:
            self.loadQueries(qFasta)
        

    def loadQueries(self,qFasta):
        self.fastaD=fasta.FastaDictionary(qFasta)

    def loadMBRs(self,mbrFiles):
        """
        """
        
        self.hitDict={}

        for l in multiFile(mbrFiles):
            if l.startswith('#'):
                continue

            f=l.split()
            candidate,hit = f[:2]

            canMatch = re.match('^(.+)_(nt\d+\.\d+)',candidate)
            try:
                source,segment=canMatch.groups()
            except:
                print l
                raise

            if source not in self.hitDict:
                self.hitDict[source]={}

            if segment not in self.hitDict[source]:
                self.hitDict[source][segment] = set()

            self.hitDict[source][segment].add(hit)
        


    def sourceChoices(self,sources=None,maxNumber=5):
        """
        """
        if sources == None:
            sources = self.hitDict.keys()

        try:
            sources.__iter__
        except AttributeError:
            sources = [sources]
        rv=[]
        for s in sources:
            segHits = [(len(v), k) for k,v in self.hitDict[s].items()]
            segHits.sort()
            segHits.reverse()
            rv.append((s,segHits[:maxNumber]))

        return rv    



plantOnlyFamilyNames = [ ]


ictvPlantGenusNames = [
        "Benyvirus",
        "Cheravirus",
        "Endornavirus",
        "Furovirus",
        "Hordeivirus",
        "Idaeovirus",
        "Ophiovirus",
        "Ourmiavirus",
        "Pecluvirus",
        "Pomovirus",
        "Sadwavirus",
        "Sobemovirus",
        "Tenuivirus",
        "Tobamovirus",
        "Tobravirus",
        "Umbravirus",
        "Varicosavirus",
        "Alfamovirus",
        "Ilarvirus",
        "Bromovirus",
        "Cucumovirus",
        "Oleavirus",
        "Tospovirus",
        "Caulimovirus",
        "Soymovirus",
        "Cavemovirus",
        "Tungrovirus",
        "Badnavirus",
        "Petuvirus",
        "Closterovirus",
        "Crinivirus",
        "Ampelovirus",
        "Comovirus",
        "Fabavirus",
        "Nepovirus",
        "Potexvirus",
        "Mandarivirus",
        "Allexivirus",
        "Carlavirus",
        "Foveavirus",
        "Capillovirus",
        "Vitivirus",
        "Trichovirus",
        "Mastrevirus",
        "Curtovirus",
        "Begomovirus",
        "Topocuvirus",
        "Luteovirus",
        "Polerovirus",
        "Enamovirus",
        "Metavirus",
        "Nanovirus",
        "Babuvirus",
        "Alphacryptovirus",
        "Betacryptovirus",
        "Potyvirus",
        "Rymovirus",
        "Bymovirus",
        "Macluravirus",
        "Ipomovirus",
        "Tritimovirus",
        "Pseudovirus",
        "Sirevirus",
        "Fijivirus",
        "Phytoreovirus",
        "Oryzavirus",
        "Cytorhabdovirus",
        "Nucleorhabdovirus",
        "Sequivirus",
        "Waikavirus",
        "Tombusvirus",
        "Carmovirus",
        "Necrovirus",
        "Dianthovirus",
        "Machlomovirus",
        "Avenavirus",
        "Aureusvirus",
        "Panicovirus",
        "Maculavirus",
        "Tymovirus",
        "Marafivirus", ]
