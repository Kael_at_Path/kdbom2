from glob import glob
import os.path
import sys

import re

#from uTax import *

from plantChip import dDB,dump_dDB
from sequence import fasta


candidates={}
def cRow(cUid):
    """
    """
    c=candidates[cUid]
    return ( cUid,
             len(c['u20']),
             len(c['u30']),
             int(c['batch']) )
             
             

def makeCandidateTable(test=False):
    dDB.execute("TRUNCATE TABLE %s" % dDB.Candidate._dbQualName())

    for c in fasta.FastaIterator('ncbiCandidates_29.fasta'):
        gi=int(c.title.split('_')[0])
        candidates[c.title] = { 'u20':set(),
                                'u30':set(),
                                'batch':0,
                                }
        if len(candidates) % 1000 == 0:
            print 'cCount',len(candidates)


    for fn in glob("ncbiCandidates_*.fasta.*_2009-*.mbr.dG"):
        plantHostName = fn.split('.fasta.')[-1].split('.')[-3]
        for l in file(fn):
            lf=l.strip().split()
            try:
                e=float(lf[-1])
            except:
                print l
                raise

            candidates[lf[0]]['batch']=re.search('_(\d\d)\.fasta',fn).group(1)
            if e <= -30:
                candidates[lf[0]]['u30'].add(plantHostName)
                candidates[lf[0]]['u20'].add(plantHostName)
            elif e <= -20:
                candidates[lf[0]]['u20'].add(plantHostName)

        print fn

    dDB.Candidate.insertMany(('Uid','Host_Grp_20','Host_Grp_30','Batch'),
                             candidates.keys(),cRow)


    fname = None
    if test:
        fname = 'testFile'

    dump_dDB('k2', fname, 'Candidate')

if __name__ == '__main__':
    makeCandidateTable()
