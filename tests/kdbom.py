import random
import time
import types
import cPickle as pickle

import kdbom2
from kdbom2 import kdbom
from __init__ import *


class TestTable(BaseTestCase):
    def __init__(self, *args, **kwargs):
        BaseTestCase.__init__(self, *args, **kwargs)
        self.table = self.db.test_table2

    def tearDown(self):
        self.db.test_table1.delete()
        self.db.test_table2.delete()
        
    def testInsert(self):
        num_rows = random.randint(0, 1000)
        values = [(i, random.randint(1000, 3000)) \
                  for i in xrange(num_rows)]
        for pair in values:
            self.table.insert({self.table.field1: pair[0],
                               self.table.field2: pair[1]})

        for pair in values:
            row = self.table(expectOneRecord=True,
                             returnRecords=True,
                             criteria={self.table.field1: pair[0]})[0]
            self.assertEqual(row.data['field2'], pair[1])
            
    def testInsertMany(self):
        num_rows = random.randint(0, 1000)
        values = [(i, random.randint(1000, 3000)) \
                  for i in xrange(num_rows)]
        self.table.insertMany([self.table.field1, self.table.field2],
                              values, chunkSize=50)

        for pair in values:
            row = self.table(expectOneRecord=True,
                             returnRecords=True,
                             criteria={self.table.field1: pair[0]})[0]
            self.assertEqual(row.data['field2'], pair[1])
            
    def testDelete(self):
        num_rows = random.randint(0, 1000)
        values = [(i, random.randint(1000, 3000)) \
                  for i in xrange(num_rows)]
        for pair in values:
            self.table.insert({self.table.field1: pair[0],
                               self.table.field2: pair[1]})

        for pair in values:
            self.table.delete(criteria={self.table.field1: pair[0]})

        self.assertEqual(self.table.count(), 0)

    def testUpdate(self):
        """
        """
        values = [(i, random.randint(1000, 3000)) \
                  for i in xrange(3)]
        for pair in values:
            self.table.insert({self.table.field1: pair[0],
                               self.table.field2: pair[1]})

        newValues = [random.randint(1, 1000) for i in xrange(3)]
        for pair in values:
            i = pair[0]-1
            self.table.update({self.table.field2: newValues[i]},
                              {self.table.field1: pair[0]})

        for pair in values:
            select = '%s' % self.table.field2
            row = self.table(selectExpr=select,
                             criteria={self.table.field1: pair[0]})[0]
            currentVal = row[0]
            self.assertEqual(newValues[pair[0]-1], currentVal)

    def testCount(self):
        values = self._populateTable(self.table)
        self.assertEqual(self.table.count(), len(values))

    def testPKvaluesAndGetValues(self):
        values = self._populateTable(self.table)
        for i,x in enumerate(self.table.PKvalues()):
            row = self.table.getValues(x)
            self.assertEqualRows([values[i][0:2],], [row[1:3],])

    def testIdValueDict(self):
        values = self._populateTable(self.table)
        for i, x in enumerate(self.table.PKvalues()):
            d = self.table.idValueDict(self.table.field2,
                                       **{self.table.primary_key._name:x})
            self.assertEqual(d, {x: values[i][1]})

    def testIdValueTuples(self):
        values = self._populateTable(self.table)
        tuples = self.table.idValueTuples(self.table.field2)
        for i, x in enumerate(self.table.PKvalues()):
            self.assertEqualRows((tuples[i],), ((x, values[i][1]),))

    def testGenerator(self):
        values = self._populateTable(self.table)
        
        gen = self.table.generator()
        self.assertTrue(hasattr(gen, 'next'))
        self.assertEqual(len(values), len(list(gen)))

    def testCall_expectOneRecordArg(self):
        values = self._populateTable(self.table, 100)

        oneRow = random.choice(values)
        rec = self.table(criteria={self.table.field1:oneRow[0]},
                         returnRecords=True)[0]
        rec.update({'field1': 1234567890})
        rows = self.table(criteria={self.table.field1:1234567890},
                          expectOneRecord=True)
        self.assertEqual(len(rows), 1)
        self.assertEqual(type(rows[0]), kdbom.Record)
        self.assertEqual(rows[0].field1, 1234567890)
        self.assertEqual(rows[0].field2, oneRow[1])

    def testCall_returnLastRecordArg(self):
        values = []
        for i in xrange(random.randint(1, 100)):
            time.sleep(1)
            values.append(self._populateTable(self.table, 1)[0])
        
        lastRow = values[-1]
        rows = self.table(returnLastRecord=True)
        self.assertEqual(len(rows), 1)
        self.assertEqual(type(rows[0]), kdbom.Record)
        self.assertEqual(rows[0].field1, lastRow[0])
        self.assertEqual(rows[0].field2, lastRow[1])

    def testCall_returnFirstRecordArg(self):
        values = self._populateTable(self.table, 10)
        
        firstRow = values[0]
        rows = self.table(returnFirstRecord=True)

        self.assertEqual(len(rows), 1)
        self.assertEqual(type(rows[0]), kdbom.Record)
        self.assertEqual(rows[0].field1, firstRow[0])
        self.assertEqual(rows[0].field2, firstRow[1])

    def testCall_selectExprAndReturnTuples(self):
        values = self._populateTable(self.table)

        rows = self.table(selectExpr="%s, %s" % \
                          (self.table.field1,
                           self.table.field2,),
                          returnRecords=False)
        self.assertEqualRows(map(lambda x: x[:-1], values), rows)

    def testCall_criteriaAndKwargs(self):
        values = self._populateTable(self.table, random.randint(1, 100))

        for val1, val2, val3 in values:
            rows = self.table(criteria={self.table.field1:val1,
                                        self.table.field2:val2},
                              returnRecords=True)
            self.assertEqual(type(rows[0]), kdbom.Record)

            rows = self.table(returnRecords=True,
                              **{str(self.table.field1): val1,
                                 str(self.table.field2):val2})
            self.assertEqual(type(rows[0]), kdbom.Record)
            
        
class TestField(BaseTestCase):
    def __init__(self, *args, **kwargs):
        BaseTestCase.__init__(self, *args, **kwargs)
        self.table = self.db.test_table2

    def tearDown(self):
        self.db.test_table1.delete()
        self.db.test_table2.delete()

    def testAggregateFunctions(self):
        values = self._populateTable(self.table)

        val = random.choice(values)[1]
        count = len(filter(lambda x: x[1] == val, values))
        d = {self.table.field2._name: val}
        self.assertEqual(count, self.table.field1.count(**d))

    def testDistinctValues(self):
        values = self._populateTable(self.table)

        distinct = sorted(list(set([x[1] for x in values])))
        self.assertEqual(distinct,
                         sorted(self.table.field2.distinctValues()))

    def testValueList(self):
        values = self._populateTable(self.table)

        self.assertEqual(sorted([x[1] for x in values]),
                         sorted(self.table.field2.valueList()))

    def testIsUnique(self):
        self.assertTrue(self.table.primary_key.isUnique())

    def testSearch(self):
        values = self._populateTable(self.table)

        temp = random.sample(values,
                             random.randint(1, len(values)))
        for v in temp:
            count = len(filter(lambda x: x[1] == v[1], values))
            recs = self.table.field2.search(v[1])
            self.assertEqual(count, len(recs))

    def testSubSearch(self):
        self._populateTable(self.table, 10)
        values = self._populateTable(self.db.test_table1, 100)

        strStart = random.choice(values)[2][:1]
        count = len(filter(lambda x: x[2].startswith(strStart),
                           values))
        rows = self.db.test_table1.field3.subSearch('%s%%' % strStart)
        self.assertEqual(count, len(rows))


class TestRecord(BaseTestCase):
    def __init__(self, *args, **kwargs):
        BaseTestCase.__init__(self, *args, **kwargs)
        self.table = self.db.test_table2

    def tearDown(self):
        self.db.test_table1.delete()
        self.db.test_table2.delete()

    def testTouchTimestamp(self):
        row = self._populateTable(self.table, 1)[0]

        rec = self.table(criteria={self.table.field1._name: row[0]},
                         returnRecords=True)[0]
        time.sleep(2)
        curTs = rec.Timestamp
        rec.touchTimestamp()

        rec2 = self.table(criteria={self.table.field1._name: row[0]},
                         returnRecords=True)[0]
        self.assertTrue(curTs < rec2.Timestamp)

    def testUpdate(self):
        values = self._populateTable(self.table)
        row = random.choice(values)

        rec = self.table(criteria={self.table.field1._name: row[0]},
                         returnRecords=True)[0]
        rec.update({self.table.field2: 123456})

        rec2 = self.table(criteria={self.table.primary_key: rec.PKvalue},
                          returnRecords=True)[0]
        self.assertEqual(rec2.field2, 123456)


class Item(kdbom.KSqlObject):
    _table = DB.Item
    _tagTable = DB.Tag
    _tagLinkTable = DB.Item_has_Tag


class test_table3(kdbom.KSqlObject):
    _table = DB.test_table3

    
class TestKSqlObject(BaseTestCase):

    def __init__(self, *args, **kwargs):
        BaseTestCase.__init__(self, *args, **kwargs)

    def setUp(self):
        DB.Item_has_Tag.delete()
        DB.Item.delete()
        DB.Tag.delete()

    def _randomItemAndTags(self):
        items = {}
        for i in xrange(random.randint(10, 50)):
            items[self._generateString()] = set()

        Item._table.insertMany(['Name',], items)

        nItems = len(items)
        tags = {}
        
        for i in xrange(nItems/2):
            tagName = self._generateString()
            tags[tagName] = set()
            c = random.randint(0, nItems)
            while c >= 0:
                itemName = random.choice(items.keys())
                c -= 1
                tags[tagName].add(itemName)
                items[itemName].add(tagName)
                item = Item(Name=itemName)
                item.applyTag(tagName)
        return items, tags

    def testApplyTag(self):
        items, tags = self._randomItemAndTags()
        for it, ts in items.iteritems():
            item = Item(Name=it)
            self.assertEqual(sorted(list(set(ts))), sorted(item.listTags()))

    def testUnlinkTag(self):
        items, tags = self._randomItemAndTags()
        rItems = [random.choice(items.keys()) for x in xrange(len(items)/2)]
        for it in rItems:
            ts = items[it]
            rTag = random.choice(list(items[it]))
            try:
                ts.remove(rTag)
            except KeyError:
                pass
            item = Item(Name=it)
            r = item.unlinkTag(rTag)
            self.assertEqual(sorted(list(ts)),
                             sorted(item.listTags()))

    def testUnlinkAllTags(self):
        items, tags = self._randomItemAndTags()
        item = Item(Name=random.choice(items.keys()))
        item.unlinkAllTags()
        items[item.Name] = set()
        for t in tags.iterkeys():
            try:
                tags[t].remove(item.Name)
            except KeyError:
                pass
        self.assertEqual(len(item.listTags()), 0)        

    def testUnlinkAllTags(self):
        items, tags = self._randomItemAndTags()
        # create some tags without linking (checkTags)
        randomTags = [self._generateString()
                      for i in xrange(random.randint(5, 10))]
        Item.checkTags(randomTags)
        tags.update(dict([(x, set()) for x in randomTags]))
        self.assertEqual(len(tags), Item._tagTable.count())

    def testKnownTags(self):
        items, tags = self._randomItemAndTags()        
        self.assertEqual(sorted(tags.keys()), sorted(Item.knownTags()))

    def testTagId(self):
        randomTags = [self._generateString()
                      for i in xrange(random.randint(5, 10))]
        tagIds = dict([(x, Item._tagTable.insert(dict(Name=x)))
                       for x in randomTags])
        for tn, tid in tagIds.iteritems():
            self.assertEqual(tid, Item.tagId(tn))

    def testTagIDdict(self):
        items, tags = self._randomItemAndTags()
        tagIDdict = Item.tagIDdict()
        self.assertEqual(len(tags), len(tagIDdict))
        self.assertTrue(all([type(x) is str
                             for x in tagIDdict.keys()]))
        self.assertTrue(all([type(x) is long
                             for x in tagIDdict.values()]))

    def testIdsByTag(self):
        items, tags = self._randomItemAndTags()

        randomTags = random.sample(tags.keys(),
                                   random.randint(5, len(tags.keys())))
        itemsWithTags = set()
        for tag in randomTags:
            itemsWithTags.update(tags[tag])

        itemIds = []
        for item in itemsWithTags:
            itemIds.append(Item(Name=item).ID())

        resultIdsByTag = Item.idsByTag(includeTags=randomTags)
        self.assertTrue(isinstance(resultIdsByTag,
                                   types.GeneratorType))
        self.assertEqual(sorted(itemIds),
                         sorted(list(resultIdsByTag)))


    def testIdsByTag_withExcludeTags(self):
        items, tags = self._randomItemAndTags()

        randomTags = random.sample(tags.keys(),
                                   random.randint(5, len(tags.keys())))
        includeTags = randomTags[:len(randomTags)/2]
        excludeTags = randomTags[len(randomTags)/2:]
        
        itemsWithTags = set()
        for tag in includeTags:
            itemsWithTags.update(tags[tag])

        for tag in excludeTags:
            itemsWithTags.difference_update(tags[tag])

        itemIds = []
        for item in itemsWithTags:
            itemIds.append(Item(Name=item).ID())

        resultIdsByTag = Item.idsByTag(includeTags=includeTags,
                                       excludeTags=excludeTags)
        self.assertTrue(isinstance(resultIdsByTag,
                                   types.GeneratorType))
        self.assertEqual(sorted(itemIds),
                         sorted(list(resultIdsByTag)))

    def _testGetByTag(self):
        items, tags = self._randomItemAndTags()

        randomTags = random.sample(tags.keys(), random.randint(5, 10))
        itemsWithTags = set()
        for tag in randomTags:
            itemsWithTags.update(tags[tag])
        
        result = Item.getByTag(includeTags=randomTags)

        self.assertTrue(isinstance(result,
                                   types.GeneratorType))
        self.assertEqual(sorted(list(itemsWithTags)),
                         sorted(map(lambda x: x.Name, result)))

    def testUtf8(self):
        f = open('unicode_text.pickle')
        items = pickle.load(f)
        f.close()

        DB.connection.set_character_set('utf8')
        DB.execute('SET NAMES utf8')
        DB.execute('SET CHARACTER SET utf8')
        DB.execute('SET character_set_connection=utf8')

        for item in items:
            try:
                test_table3Item = test_table3(field1=item)
            except kdbom.KdbomLookupError:
                test_table3Item = test_table3(field1=item,
                                              insertOnly=True)

        self.assertEqual(test_table3.count() == len(items))


suite = unittest.TestSuite()
for cls in (TestTable, TestField, TestRecord, TestKSqlObject):
    s = unittest.TestLoader().loadTestsFromTestCase(cls)
    suite.addTest(s)

if __name__ == '__main__':
    unittest.main()
