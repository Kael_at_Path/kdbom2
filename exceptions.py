#KDBOM exceptions#
__version__="$Id: exceptions.py,v 1.4 2011/01/07 22:40:27 kael Exp $"	
class KdbomError(Exception):
    pass
                 
class DateConversionError (Exception):
    pass

class KdbomRelationshipError (KdbomError):
    pass

Relationship_Error=KdbomRelationshipError

class ArgumentError (Exception):
    pass

class KdbomDatabaseError (KdbomError):
    pass

class KdbomInsertError (KdbomDatabaseError):
    pass

class KdbomLookupError (KeyError,KdbomDatabaseError):
    pass

class KdbomUsageError (KdbomError):
    pass

class KdbomProgrammingError (KdbomError):
    pass

class KdbomOperationalError(KdbomError):
    pass
