# Kael's Database Object Model
# MySQL driver

__version__ = tuple([int(x) for x in
                     '$Revision: 1.65 $'.split()[1].split('.')])
__author__ = "Kael Fischer and Julio Carlos Menendez"


import time
import re
import types
from copy import copy
import sys
import os.path

import MySQLdb
import MySQLdb.cursors

from kdbom2.util import getIterable
from kdbom2.exceptions import *
from __init__ import Relationship, BaseField, BaseCursor
import kdbom2.query
from kdbom2.query import (BaseQuery, Expression, DummyFunctionClass,
                          BaseTable)

NAME = 'mysql'

possibleDefaultsFileLocations = (
     os.path.normpath(os.path.expanduser('~/.my.cnf')),
     os.path.normpath('C:\\my.cnf'),
     os.path.normpath('C:\\my.ini')     
     )

if 'HOME' in os.environ:
    home = '%s/.my.cnf' % os.environ['HOME']
    possibleDefaultsFileLocations += (os.path.normpath(home),)


defaultCnfFile = None
for p in possibleDefaultsFileLocations:
    if os.access(p,os.R_OK):
        defaultCnfFile = p
        break


class SSCursor(MySQLdb.cursors.SSCursor):
    def __init__(self, connection):
        """`connection` needs to be a kdbom.DB object to be able to
        clone the connection.
        """
        if not hasattr(connection, 'clone'):
            raise KdbomUsageError, ('This cursor requires a clonable '
                                    'connection.')
        
        newConn = connection.clone()
        newConn.connection = None
        newConn.connect()
        super(SSCursor, self).__init__(newConn.connection)


class Cursor(BaseCursor):
    """MySQL cursor class
    """
    def __init__(self, *args, **kwargs):
        BaseCursor.__init__(self, *args, **kwargs)
        self.generatorCompatibleCursorClass = SSCursor

    def exceptionButTryAgain(self, exception):
        if isinstance(exception, MySQLdb.OperationalError) and \
               exception[0] == 2006:
            return True
        return False

    def beforeExecuteMany(self, query, cursor, params, chunkSize,
                          partialCommits, **kwargs):
        if 'disableFKcheck' in kwargs and kwargs['disableFKcheck']:
            cursor = self._execute("SET FOREIGN_KEY_CHECKS=0",
                                    reuseCursor=cursor)[0]
        return (query, cursor, params, chunkSize, partialCommits)

    def afterExecuteMany(self, query, cursor, params, chunkSize,
                          partialCommits, **kwargs):
        if 'disableFKcheck' in kwargs and kwargs['disableFKcheck']:
            cursor = self._execute("SET FOREIGN_KEY_CHECKS=1",
                                    reuseCursor=cursor)[0]

    def _getCursor(self, needConnection=False, cursorClass=None):
        """Returns a MySQL cursor.
        """
        if cursorClass is None:
            cursorClass = self.cursorClass
        if needConnection:
            self.db.connect()
        if cursorClass is SSCursor:
            return cursorClass(self.db)
        return self.db.connection.cursor(cursorClass)


class Query(BaseQuery):
    """Represents a MySQL query.
    """
    
    def __init__(self, table, avoidTables=None):
        """Initializes a query instance.
        
        Arguments:
        - `table`: The main table of this query.
        - `avoidTables`: Table or list of tables to avoid when
        generating the required JOIN clauses.
        """
        BaseQuery.__init__(self, table, avoidTables)
        self._wildcard = '%s'

    def escape(self, value, field=None):
        """Escapes the value using the DBI escape method.
        """
        if isinstance(value, Expression):
            return str(value)
        if self._wildcard is not None and value == '%s':
            return self._wildcard
        dbConn = self._table.db.connection
        return dbConn.escape(value, dbConn.encoders)


class DB(object):
    """MySQL Database class
    """
    
    def __init__(self, db=None, host='', username=None, password=None,
                 port=3306, defaultsFile=defaultCnfFile, retryTimes=2,
                 retryInterval=30, reuseDBconnection=None, *args,
                 **kwargs):
        """Initializes a DB instance. Connects to the DB specified in
        the arguments or using the defaults file, usually ~/.my.cnf
        
        Arguments:
        - `host`: MySQL server to connect to.
        - `db`: Name of the DB to connect to.
        - `username`: username to use
        - `password`: password to use
        - `port`: Port where the MySQL server is running.
        - `defaultsFile`: File to use instead of the system's defaults
        file.
        - `retryTimes`: Number of retries when the connection fails
        before raising an exception.
        - `retryInterval`: Time to wait in miliseconds between every retry.
        """
        self._name = db
        self.tables = {}
        self.table_names = []
        self._retryTimes = retryTimes
        self._retryInterval = retryInterval
        self.connection = None
        self.useCharSet=None


        if reuseDBconnection is not None:
            self._defaultDB = reuseDBconnection
            self.connection = reuseDBconnection.connection
            self._host = reuseDBconnection._host
            self._port = reuseDBconnection._port
            self._username = reuseDBconnection._username
            self._password = reuseDBconnection._password
            self._defaultsFile = reuseDBconnection._defaultsFile
            self._kwargs = kwargs
        else:
            self._defaultDB = self
            self._host = host
            self._username = username
            self._password = password
            self._port = port
            self._defaultsFile = defaultsFile
            self._kwargs=kwargs
        
    def connect(self):
        """Connects or reconnects to the MySQL server.
        """
        if self.alive():
            return
        arguments={}#self._kwargs
        arguments.update({'host': self._host,'port':self._port})
        if self._name is not None:
            arguments['db'] = self._name
        if self._username is not None or self._password is not None:
            if self._username is not None:
                arguments['user'] = self._username
            else:
                arguments['user'] = ''
            if self._password is not None:
                arguments['passwd'] = self._password
            else:
                arguments['passwd'] = ''
        elif self._defaultsFile is not None:
            if 'user' in self._kwargs:
                del self._kwargs['user']
            if 'passwd' in self._kwargs:
                del self._kwargs['passwd']
            arguments['read_default_file'] = self._defaultsFile

        if 'autocommit' in self._kwargs:
            del self._kwargs['autocommit']
        if 'debug' in self._kwargs:
            del self._kwargs['debug']
        if 'getTables' in self._kwargs:
            del self._kwargs['getTables']
        if 'useCharSet' in self._kwargs:
            self.useCharSet=self._kwargs['useCharSet']
            del self._kwargs['useCharSet']
        
        arguments.update(self._kwargs)
        attempts = 0
        self.connection = None
        while attempts < self._retryTimes and self.connection is None:
            if self._debug:
                print 'MySQL connection attempt #%i' % attempts
            try:
                #print arguments
                self.connection = MySQLdb.Connection(**arguments)
            except (MySQLdb.OperationalError,
                    MySQLdb.DatabaseError), e:
                if isinstance(e, MySQLdb.OperationalError) and \
                   e.args[0] == 1049:
                    raise KdbomDatabaseError, e.args[1]
                attempts += 1
                if self._debug:
                    print 'MySQL connection error: \n %s' % str(e)
                time.sleep(self._retryInterval)

        if self.connection is None:
            raise Exception('MySQL driver exception: Cannot '
                            'connect for unknown reasons.')

        if self.useCharSet is not None:
            self.connection.set_character_set(self.useCharSet)
            self.execute('SET NAMES %s'%self.useCharSet)
            self.execute('SET CHARACTER SET %s'%self.useCharSet)
            self.execute('SET character_set_connection=%s'%self.useCharSet)


        if self._name is not None:
            self.__loadTables()

    _establishConnection = connect


    def changeUser(self, user=None, passwd=None):
        """Reconnects as a different user. Notice that this will close
        the current connection and try to open a new one.
        """
        self._origConnection = self.connection
        self._username = user
        self._password = passwd
        self.close()
        self.connect()

    def alive(self):
        """Checks if the connection to the server is alive.
        """
        try:
            self.connection.ping()
        except:
            return False
        return True

    def commit(self):
        """Commit the transaction.
        """
        if not self.alive():
            self.connect()
        self.connection.commit()

    def rollback(self):
        """Rollback the transaction.
        """
        if not self.alive():
            self.connect()
        self.connection.rollback()

    def close(self):
        """Closes the connection
        """
        return self.connection.close()

    def __getName(self):
        return self._name

    name = property(__getName)

    def __getHost(self):
        return self._host

    host = property(__getHost)

    def __getPort(self):
        return self._port

    port = property(__getPort)

    def __loadTables(self):
        """Loads table objects for each table in the DB.
        """
        cursor = Cursor(self)
        for row in cursor.fetchall("SHOW TABLES FROM %s" % \
                                self):
            if row[0] in self._ignoreTables:
                continue
            if not self.tables.has_key(row[0]):
                self.tables[row[0]] = self.tableClass(db=self,
                                                      name=row[0])
                self.table_names.append(row[0])

        # Table relationships are loaded after all the tables are
        # created to handle circular references
        for t in self.tables.values():
            t.loadRelationships()

    def databaseNames(self, whereLike="'%'"):
        qry = "SHOW DATABASES LIKE %s" % whereLike
        return [x[0] for x in self.fetchall(qry)]

    def databaseIterator(self):
        for dbName in self.databaseNames:
            yield kdbom.tryDBconnect(db=dbName,
                                     fatal=True,
                                     verboseFailure=True,
                                     reuseDBconnection=self
                                     )


    def clone(self, dbName=None):
        if dbName is None:
            dbName = self.name
        return self.__class__(dbName, reuseDBconnection=self)

    def createDatabase(self, name):
        """Creates a database with the `name` and returns a DB
        instance with that new DB.
        """
        qry = "CREATE DATABASE IF NOT EXISTS %s" % name
        self.execute(qry)
        return self.__class__(name, reuseDBconnection=self)

    verifyConnection = connect

    def GetTableNames(self):
        self.__loadTables()
        return self.table_names

    def escape(self,values):
        """Escape values for use in query. Values can be a list, tuple
        or a single escapable thing (e.g. string, int, long, float, None, etc.).
        """
        if type(values) not in (types.ListType, types.TupleType):
            return self.connection.escape(values, self.connection.encoders)
        else:
            return tuple(map(self.escape, values))

    def makeDBSQL(self, newDBName):
        queries = ["""CREATE DATABASE `%s`""" % newDBName]
        queries += map(lambda t: t.sqlDefinition,
                       self.tables.itervalues())
        return ';\n\n'.join(queries)

    def __str__(self):
        """Returns the name of the DB already quoted for use in a
        MySQL query
        """
        return "`%s`" % self._name

    def createTable(self, name, definitions, options=None,
                    ifNotExists=False, temporary=False):
        """Creates a new table on this database and returns the
        corresponding Table instance.

        Arguments:
        - `name`: Name of the table.
        - `definitions`: List of field definitions or a Query
        instance.
        - `options`: List of table options
        - `ifNotExists`: If True, the connection wont return an error
        if a table with the same `name` already exists.
        - `temporary`: If True the table created is a temporary table.
        """
        if temporary:
            temporaryStr = 'TEMPORARY'
        else:
            temporaryStr = ''
        
        if ifNotExists:
            ifNotExistsStr = 'IF NOT EXISTS'
        else:
            ifNotExistsStr = ''

        if options:
            optionsStr = ' '.join(options)
        else:
            optionsStr = ''

        if isinstance(definitions, BaseQuery):
            if definitions.type != 'SELECT':
                raise (KdbomProgrammingError,
                       ('Tables can only be created from SELECT '
                        'queries. Your query type is %s.' %
                        definitions.type))
            definitionStr = '%s\n%s' % (optionsStr, str(definitions))
        else:
            definitionStr = '(\n%s\n) %s' % ('\n'.join(definitions),
                                             optionsStr)
        
        sql = 'CREATE %s TABLE %s %s %s' % (temporaryStr,
                                            ifNotExistsStr,
                                            name,
                                            definitionStr)

        try:
            self.execute(sql)
        except MySQLdb.OperationalError, ex:
            raise KdbomOperationalError, ex[1]
        except Exception, ex:
            raise KdbomError, ex[1]

        self.tables[name] = self.tableClass(db=self, name=name)
        self.table_names.append(name)
        self.tables[name].loadRelationships()

        return self.tables[name]


class Table(BaseTable):
    """MySQL table class
    """
    def __str__(self):
        """Return the full and quoted name of this table. Ready to use
        in a MySQL query.
        """
        return "%s.`%s`" % (self.db, self._name)

    _dbQualName = __str__
    dbQualName = _dbQualName

    def __loadFieldsAndRelationships(self):
        """Loads field objects for each field in the table
        definition. At the same time loads the relationships with
        other tables.
        """
        cursor = Cursor(self.db)
        row = cursor.fetchone("SHOW CREATE TABLE %s" % self)
        self.sqlDefinition = row[1]

        self.fields = {}
        self.fieldNames = []
        self.relationships = {}
        for row in cursor.fetchall("EXPLAIN %s" % self):
            (fieldName, dataType, nullAllowed, indexing, default,
             extra) = row
            nullAllowed = nullAllowed == 'YES'
            self.fieldNames.append(fieldName)
            self.fields[fieldName] = self.db.fieldClass(table=self,
                                            name=fieldName,
                                            dataType=dataType,
                                            nullAllowed=nullAllowed,
                                            indexing=indexing,
                                            extra=extra,
                                            default=default)
        
        self.field_names = self.fieldNames
        
        self._autoIncField = None
        match = re.search(r'^\s*`(.+)`.* AUTO_INCREMENT[, ]',
                          self.sqlDefinition, re.MULTILINE)
        if match is not None:
            self._autoIncField = self.fields[match.group(1)]

        self._pkField = self._autoIncField
        match = re.search(r'PRIMARY KEY \s*\(\`(\w+)\`\)',
                          self.sqlDefinition, re.MULTILINE)
        if match is not None:
            self._pkField = self.fields[match.group(1)]
        self.primary_key = self._pkField
        cursor.close()

    def loadRelationships(self):
        for fieldName in self.fieldNames:
            match = re.search(('FOREIGN KEY \(`%s`\) REFERENCES '
                               '`(?P<table>\w+)` '
                               '\(`(?P<field>\w+)`\)') % fieldName,
                              self.sqlDefinition)
            if match:
                foreignTableName = match.group('table')
                foreignFieldName = match.group('field')
                foreignTable = None
                if not self.db.tables.has_key(foreignTableName):
                    foreignTable = self.db.tableClass(db=self.db,
                                         name=foreignTableName)
                    self.db.tables[foreignTableName] = foreignTable
                else:
                    foreignTable = self.db.tables[foreignTableName]
                foreignField = foreignTable.fields[foreignFieldName]
                
                rel = self.db.relationClass(parent=foreignField,
                                            child=self.fields[fieldName])
                self.relationships[foreignTable] = rel
                foreignTable.relationships[self] = rel

    def cloneTable(self, newName):
        """Creates and returns a new table with the same design.
        """
        cursor = Cursor(self.db)
        cursor.execute("CREATE TABLE %s.`%s` LIKE %s" % \
                       (self.db, newName, self))
        newTable = self.__class__(db=self.db, name=newName)
        self.db.tables[newName] = newTable
        return newTable

    def disableKeys(self):
        self.db.fetchall("ALTER TABLE %s DISABLE KEYS" % self._dbQualName())

    def enableKeys(self):
        self.db.fetchall("ALTER TABLE %s ENABLE KEYS" % self._dbQualName())


class Field(BaseField):
    """Represents a field in a MySQL table.
    """
    
    def escape(self, value):
        """Returns the value ready to be used in a query.
        
        Arguments:
        - `value`: Value to prepare.
        """
        if value is None:
            return 'NULL'
        return self.db.connection.escape(self.convertForDB(value),
                                          self.db.connection.encoders)

    escapeForDB = escape

sqlFunctions = ('ABS ACOS ADDDATE ADDTIME AES_DECRYPT AES_ENCRYPT '
                  'ASCII ASIN ATAN2 ATAN ATAN AVG BENCHMARK BIN '
                  'BIT_AND BIT_COUNT BIT_LENGTH BIT_OR BIT_XOR CAST '
                  'CEIL CEILING CHAR_LENGTH CHAR CHARACTER_LENGTH '
                  'CHARSET COALESCE COERCIBILITY COLLATION COMPRESS '
                  'CONCAT_WS CONCAT CONNECTION_ID CONV CONVERT_TZ COS '
                  'COT COUNT CRC32 CURDATE CURRENT_DATE CURRENT_TIME '
                  'CURRENT_TIMESTAMP CURRENT_USER CURTIME DATABASE '
                  'DATE_ADD DATE_FORMAT DATE_SUB DATE DATEDIFF DAY '
                  'DAYNAME DAYOFMONTH DAYOFWEEK DAYOFYEAR DECODE '
                  'DEFAULT DEGREES DES_DECRYPT DES_ENCRYPT ELT ENCODE '
                  'ENCRYPT EXP EXPORT_SET EXTRACT FIELD FIND_IN_SET '
                  'FLOOR FORMAT FOUND_ROWS FROM_DAYS FROM_UNIXTIME '
                  'GET_FORMAT GET_LOCK GREATEST GROUP_CONCAT HEX HOUR '
                  'IF IFNULL IN INET_ATON INET_NTOA INSERT INSTR '
                  'INTERVAL IS_FREE_LOCK IS_USED_LOCK ISNULL '
                  'LAST_INSERT_ID LCASE LEAST LEFT LENGTH LN LOAD_FILE '
                  'LOCALTIME LOCALTIMESTAMP LOCATE LOG10 LOG2 LOG LOWER '
                  'LPAD LTRIM MAKE_SET MAKEDATE MASTER_POS_WAIT MAX MD5 '
                  'MICROSECOND MID MIN MINUTE MOD MONTH MONTHNAME '
                  'NAME_CONST NOW NULLIF OCT OCTET_LENGTH OLD_PASSWORD '
                  'ORD PASSWORD PERIOD_ADD PERIOD_DIFF PI POSITION POW '
                  'POWER QUARTER QUOTE RADIANS RAND RELEASE_LOCK REPEAT '
                  'REPLACE REVERSE RIGHT ROUND ROW_COUNT RPAD RTRIM '
                  'SCHEMA SEC_TO_TIME SECOND SESSION_USER SHA1 SHA SIGN '
                  'SIN SLEEP SOUNDEX SPACE SQRT STD STDDEV_POP '
                  'STDDEV_SAMP STDDEV STR_TO_DATE STRCMP SUBDATE SUBSTR '
                  'SUBSTRING_INDEX SUBSTRING SUBTIME SUM SYSDATE '
                  'SYSTEM_USER TAN TIME_FORMAT TIME_TO_SEC TIME '
                  'TIMEDIFF TIMESTAMP TIMESTAMPADD TIMESTAMPDIFF '
                  'TO_DAYS TRIM TRUNCATE UCASE UNCOMPRESS '
                  'UNCOMPRESSED_LENGTH UNHEX UNIX_TIMESTAMP UPPER USER '
                  'UTC_DATE UTC_TIME UTC_TIMESTAMP UUID VALUES VAR_POP '
                  'VAR_SAMP VARIANCE VERSION WEEK WEEKDAY WEEKOFYEAR '
                  'YEAR YEARWEEK DISTINCT')

for fun in sqlFunctions.split():
    setattr(kdbom2.query, fun, DummyFunctionClass.makeClass(fun))
    kdbom2.query.__all__.append(fun)
