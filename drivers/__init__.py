#
# KDBOM - Kael's Database Object Model
# Drivers module.
#
# Base for Field and Cursor classes that need to be implemented in
# each driver.
#
__version__ = tuple([int(x) for x in
                     '$Revision: 1.21 $'.split()[1].split('.')])
__author__ = "Kael Fischer and Julio Carlos Menendez"


import types
import time
import traceback


from kdbom2.exceptions import (KdbomProgrammingError,
                               KdbomOperationalError, KdbomUsageError)
from kdbom2.util import getIterable


class Relationship(object):
    """Table relationship class
    """
    
    def __init__(self, parent, child):
        """Initializes the instance
        """
#        if type(parent) is not Field or type(child) is not Field:
#            raise TypeError
        self.parent = parent
        self.child = child
        
    def __repr__(self):
        """String representation
        """
        return '%s -> %s' % (self.parent, self.child)

    def join(self, table, joinType='LEFT'):
        """Returns the SQL JOIN clause relative to the `table` specified.
        
        Arguments:
        - `table`: Table instance or table name.
        """
        if type(table) in types.StringTypes:
            table = self.child.db[table]
        return '%s JOIN %s ON %s = %s' % (joinType.upper(), table,
                                          self.parent, self.child)

    def __str__(self):
        """Returns the SQL JOIN clause
        """
        return self.join(self.parent.table)

    def __contains__(self, table):
        """Returns True if `table` belongs to this relationship.
        """
        tableRep = str(table)
        return tableRep == str(self.parent.table) or \
               tableRep == str(self.child.table)

    def other(self, table):
        """Returns the other table object in this relationship
        
        Arguments:
        - `table`:
        """
        tableRep = str(table)
        if tableRep == str(self.parent.table):
            return self.child.table
        return self.parent.table

    def tables(self):
        """Return a tuple of the tables in the relationship.
        """
        return (self.parent.table, self.child.table)

    def __eq__(self, other):
        return (self.parent == other.parent and \
                self.child == other.child) or \
                (self.parent == other.child and \
                 self.child == other.parent)

    def __hash__(self):
        return hash(tuple(sorted([self.parent, self.child])))



class BaseField(object):
    """Base class for a field
    """

    def escape(self, value):
        """Returns a valid escaped version of `value`.
        Must be implemented by descendant classes.
        """
        raise NotImplementedError()

    def __str__(self):
        """Returns the name of the field quoted and ready to use in a
        SQL query.
        """
        return "%s.`%s`" % (self.table, self._name)


class BaseCursor(object):
    """Base class for cursors
    """

    def __init__(self, db, cursorClass=None, retryTimes=None,
                 retryInterval=None):
        """Initializes a cursor instance.
        """
        self.db = db
        self.description = None
        self.cursorClass = cursorClass
        self.lastCursor = None
        self.generatorCompatibleCursorClass = None
        self._closed = False
        if retryTimes is None:
            self._retryTimes = db._retryTimes
        else:
            self._retryTimes = retryTimes
        if retryInterval is None:
            self._retryInterval = db._retryInterval
        else:
            self._retryInterval = retryInterval

    def prepareQuery(self, query):
        """Gets the query string from the user and makes any changes
        required by the driver. This method should be overritten in
        the drivers.
        """
        return query

    def prepareParams(self, params):
        """Gets the list of params from the user and makes any changes
        required by the driver. This method should be overritten in
        the drivers.
        """
        return params

    def exceptionButTryAgain(self, exception):
        """Returns True or False depending on `exception`. Is used to
        know if the cursor should try to execute the query again or
        not. This method should be overritten in the drivers.
        """
        return False

    def _execute(self, query, params=None, reuseCursor=None,
                  methodName='execute', cursorClass=None):
        if type(query) is not str:
            query = str(query)
        
        if self._closed:
            raise KdbomUsageError, 'Cursor is already closed.'

        self.db.connect()

        query = self.prepareQuery(query)
        params = self.prepareParams(getIterable(params, False))

        if len(params) == 0:
            params = None
        
        if self.db._debug:
            print 'Executing query: %s with params: %s' % \
                  (query, str(params))

        attempts = 0
        while attempts < self._retryTimes:
            if self.db._debug:
                print 'Cursor execution attempt #%i' % attempts
            try:
                needConnection = attempts > 0
                if reuseCursor is not None and not needConnection:
                    cursor = reuseCursor
                else:
                    cursor = self._getCursor(needConnection,
                                             cursorClass=cursorClass)
                result = getattr(cursor, methodName)(query, params)
                if self.lastCursor is not cursor:
                    self.lastCursor = cursor
                if hasattr(cursor, 'description'):
                    self.description = cursor.description
                else:
                    self.description = None
                if hasattr(cursor, 'lastrowid'):
                    self.lastrowid = cursor.lastrowid
                else:
                    self.lastrowid = None
                return (cursor, result)
            except Exception, e:
                if self.exceptionButTryAgain(e):
                    attempts += 1
                    traceback.print_exc()
                    if self.db._debug:
                        print ('Cursor execution failed. Connection '
                               'with server lost.')
                else:
                    traceback.print_exc()
                    print query
                    raise e

        raise KdbomOperationalError('Couldn\'t execute the query '
                                    'after several retries.\n%s\n%s' %
                                    (query, params)) 

    def execute(self, query, params=None):
        """Executes `query`.

        - `query`: SQL query to execute.
        - `params`: Params to update in the `query`.
        """
        r = self._execute(query, params)[1]
        if self.db.autocommit:
            self.db.commit()
        return r

    def executemany(self, query, params=None, chunkSize=10000,
                    partialCommits=False, **kwargs): 
        """Executes `query` with more than one params. Uses
        executemany for faster execution.
        
        Arguments:
        - `query`: SQL query to execute.
        - `params`: List of params to pass to the query.
        - `disableFKcheck`: Disable/Enable FOREIGN_KEY_CHECKS.
        - `chunkSize`: Size of the actual param list to send.
        """
        cursor = None
        val = self.beforeExecuteMany(query, cursor, params,
                                     chunkSize, partialCommits,
                                     **kwargs)
        (query, cursor, params, chunkSize, partialCommits) = val
        if params is None:
            cursor = self._execute(query, params, reuseCursor=cursor,
                                    methodName='executemany')[0]
            if partialCommits:
                self.db.commit()
        elif type(params) in (types.ListType, types.TupleType):
            for start in range(0, len(params), chunkSize):
                end = start+chunkSize
                cursor = self._execute(query, params[start:end],
                                        reuseCursor=cursor,
                                        methodName='executemany')[0]
                if partialCommits:
                    self.db.commit()
        elif hasattr(params, 'next'):
            finished = False
            while not finished:
                try:
                    ps = [params.next() for x in xrange(chunkSize)]
                    cursor = self._execute(query, ps, reuseCursor=cursor,
                                           methodName='executemany')[0]
                    if partialCommits:
                        self.db.commit()
                except StopIteration:
                    finished = True

        self.afterExecuteMany(query, cursor, params, chunkSize,
                              partialCommits, **kwargs)

        if not partialCommits:
            self.db.commit()

    def beforeExecuteMany(self, query, cursor=None, params=None,
                          chunkSize=None, partialCommits=None,
                          **kwargs):
        """Receives all the arguments passed to the executemany method
        and returns a tuple with (query, cursor, params, chunkSize,
        partialCommits) changed to fit the needs of the drivers. Its
        executed before executing the actual query. This method should
        be overritten in the drivers.
        """
        return (query, cursor, params, chunkSize, partialCommits)

    def afterExecuteMany(self, query, cursor=None, params=None,
                         chunkSize=None, partialCommits=None,
                         **kwargs):
        """Receives all the arguments passed to the executemany
        method. Its executed right after all the execution is done. No
        need to return anything. This method should be overritten in
        the drivers.
        """
        pass

    def fetchgenerator(self, query, params=None):
        """Returns a generator with the results of executing `query`.
        NOTE: This uses mysql_use_result which ties up server
        resources. By default the mysqld process will close the
        connection after thirty seconds of idle time.
        
        Arguments:
        - `query`: SQL query to execute.
        - `params`: Params to update in the `query`.
        """
        cursorClass = self.generatorCompatibleCursorClass
        cursor = self._execute(query, params,
                                cursorClass=cursorClass)[0]
        reseted = False
        iterations = 0
        while True:
            try:
                if reseted:
                    current = 0
                    while current < iterations:
                        cursor.fetchone()
                        current += 1
                    reseted = False
                row = cursor.fetchone()
                if row == None:
                    raise StopIteration
                iterations += 1
                yield row
            except Exception, e:
                if self.exceptionButTryAgain(e):
                    cursor = self._execute(query, params,
                                            cursorClass=cursorClass)[0]
                    reseted = True
                    continue
                else:
                    raise e

    def fetchone(self, query=None, params=None):
        """Returns the first result row from executing the `query`.
        
        Arguments:
        - `query`: SQL query to execute.
        - `params`: Params to update in the `query`.
        """
        if query is None:
            if self.lastCursor is not None:
                return self.lastCursor.fetchone()
            else:
                raise KdbomUsageError, 'Empty SQL query'
        cursor = self._execute(query, params)[0]
        return cursor.fetchone()

    def fetchall(self, query, params=None, batchSize=1000,
                 batch=None):
        """Returns all the result rows from executing `query`
        
        Arguments:
        - `query`: SQL query to execute.
        - `params`: Params to update in the `query`.
        """
        if query is None:
            if self.lastCursor is not None:
                return self.lastCursor.fetchone()
            else:
                raise KdbomUsageError, 'Empty SQL query'
        if type(batch) is types.IntType and batch > 0:
            query = '%s LIMIT %d OFFSET %d' % (query, batch,
                                               batchSize*(batch-1))
        cursor = self._execute(query, params)[0]
        return cursor.fetchall()

    fetchbatch = fetchall

    def _getCursor(self, needConnection=False, cursorClass=None):
        """Returns a real driver cursor. Must be implemented in the
        driver.
        """
        raise NotImplementedError()

    def close(self):
        if self.lastCursor is not None:
            self.lastCursor.close()
        self.closed = True
