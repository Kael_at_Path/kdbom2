# Kael's Database Object Model
# SQLite driver

__version__ = tuple([int(x) for x in
                     '$Revision: 1.49 $'.split()[1].split('.')])
__author__ = "Kael Fischer and Julio Carlos Menendez"


import time
import re
import types
from copy import copy
import sys
import os

import sqlite3

from kdbom2.util import flatten
from kdbom2.exceptions import *
from __init__ import Relationship, BaseField, BaseCursor
import kdbom2.query
from kdbom2.query import BaseQuery, DummyFunctionClass, BaseTable

NAME = 'sqlite3'
DATA_PATH = ''


class Cursor(BaseCursor):
    """Sqlite cursor class
    """

    def __init__(self, *args, **kwargs):
        BaseCursor.__init__(self, *args, **kwargs)
        self.generatorCompatibleCursorClass = sqlite3.Cursor
    
    def prepareQuery(self, query):
        query = re.sub(r'TRUNCATE TABLE (.*)', r'DELETE FROM \1',
                       query)
        query = query.replace('IGNORE', '', 1)
        query = query.replace('%s', '?')
        return query

    def prepareParams(self, params):
        if params is None:
            return []
        return params

    def exceptionButTryAgain(self, exception):
        if isinstance(exception, sqlite3.OperationalError):
            return False
        return True

    def _getCursor(self, needConnection=False, cursorClass=None):
        """Returns a Sqlite cursor.
        """
        if cursorClass is None:
            cursorClass = self.cursorClass
        if needConnection:
            self.db.connect()
        return self.db.connection.cursor()

    def beforeExecuteMany(self, query, cursor=None, params=None,
                          chunkSize=None, partialCommits=None,
                          **kwargs):
        """Sets the isolation_level to its default value.
        """
        self._previousIsolationLevel = self.db.connection.isolation_level
        self.db.connection.isolation_level = ''
        return (query, cursor, params, chunkSize, partialCommits)

    def afterExecuteMany(self, query, cursor=None, params=None,
                         chunkSize=None, partialCommits=None,
                         **kwargs):
        """Returns the isolation_level to its previous value
        """
        self.db.connection.isolation_level = self._previousIsolationLevel


class Query(BaseQuery):
    """Represents a Sqlite query.
    """
    
    def __init__(self, table, avoidTables=None):
        """Initializes a query instance.
        
        Arguments:
        - `table`: The main table of this query.
        - `avoidTables`: Table or list of tables to avoid when
        generating the required JOIN clauses.
        """
        BaseQuery.__init__(self, table, avoidTables)
        self._wildcard = '?'


class DB(object):
    """MySQL Database class
    """
    
    def __init__(self, name=None, *args, **kwargs):
        """Initializes a DB instance. Opens the DB file if specified
        
        Arguments:
        - `name`: Filename of the DB.
        """
        self._filename = ''
        self._name = ''
        self.tables = {}
        self.table_names = []
        self._retryTimes = 1
        self._retryInterval = 0
        self._attachedDBs = {}
        
        if name is None and 'db' in kwargs:
            self._filename = kwargs['db']
        else:
            self._filename = name

        if 'dbname' in kwargs:
            self._name = kwargs['dbname']
            del kwargs['dbname']
        else:
            cleanFn = os.path.split(self._filename)[1]
            if cleanFn.endswith('.db'):
                self._name = cleanFn[:-3]
            else:
                self._name = cleanFn

        if 'reuseDBconnection' in kwargs and \
           kwargs['reuseDBconnection'] is not None:
            otherDB = kwargs['reuseDBconnection']
            if self._filename:
                otherDB.attach(self._filename, self._name)
                self.connection = otherDB.connection
                self.__loadTables()
        else:
            self.connection = None
        
    def connect(self):
        """Connects or reconnects to the MySQL server.
        """
        if self.alive():
            return

        if self._filename:
            fn = os.path.join(DATA_PATH, 'sqlite_holder.db')
            self.connection = sqlite3.connect(fn)
            self.connection.isolation_level = None
            self.attach(self._filename, self._name)
            self.__loadTables()

    _establishConnection = connect

    def changeUser(self, user=None, passwd=None):
        """Doesn't do anything in this driver
        """
        pass
        
    def alive(self):
        """Checks if the connection to the server is alive.
        """
        return self.connection is not None

    def commit(self):
        """Commit the transaction.
        """
        self.connection.commit()

    def rollback(self):
        """Rollback the transaction.
        """
        self.connection.rollback()

    def close(self):
        """Closes the connection
        """
        self.connection.close()
        self.connection = None

    def __getName(self):
        return self._name

    name = property(__getName)

    def __getHost(self):
        return ''

    host = property(__getHost)

    def __getPort(self):
        return ''

    port = property(__getPort)

    def __loadTables(self):
        """Loads table objects for each table in the DB.
        """
        cursor = Cursor(self)
        qry = 'SELECT tbl_name FROM %s.sqlite_master' % self._name
        for row in cursor.fetchall(qry):
            if row[0] in self._ignoreTables:
                continue
            if not self.tables.has_key(row[0]):
                if not self._lazyLoad:
                    self.tables[row[0]] = self.tableClass(db=self,
                                                          name=row[0])
                
                self.table_names.append(row[0])
        for t in self.tables.values():
            t.loadRelationships()

    def databaseNames(self, whereLike='%'):
        if self._name:
            return [self._name]
        return []

    def clone(self, dbName=None):
        if dbName is None:
            dbName = self._filename
        newDB = self.__class__(dbName)
        for aName, dbFile in self._attachedDBs.iteritems():
            if aName == self._name:
                continue
            newDB.attach(dbFile, aName)
        return newDB

    def createDatabase(self, name):
        """Creates a database with the `name` and returns a DB
        instance with that new DB.
        """
        return self.clone(name)

    verifyConnection = connect

    def GetTableNames(self):
        self.__loadTables()
        return self.table_names

    def escape(self,values):
        """Escape values for use in query. Values can be a list, tuple
        or a single escapable thing (e.g. string, int, long, float, None, etc.).
        """
        if type(values) not in (types.ListType, types.TupleType):
            if values is None:
                return 'NULL'
            v = "'%s'" % str(values).replace("'", "''")
            return v.decode('utf-8','ignore')
        else:
            return tuple(map(self.escape, values))

    def makeDBSQL(self, newDBName):
        queries = ["""CREATE DATABASE `%s`""" % newDBName]
        queries += map(lambda t: t.sqlDefinition,
                       self.tables.itervalues())
        return ';\n\n'.join(queries)

    def __str__(self):
        """Returns the name of the DB already quoted for use in a
        query
        """
        return '`%s`' % self._name

    def attach(self, dbFilename, attachedName=None,
               returnNewDb=False):
        """Attaches another DB to this one. The new DB will be
        accessible with its full qualified name DBName.TableName

        Parameters:
        - `dbFilename`: SQLite DB filename
        - `attachedName`: Name to use as DBName instead of the
        filename
        - `returnNewDb`: If True returns a new class instance with the
        attached database as main
        """
        if attachedName is None:
            fn = os.path.split(dbFilename)[1]
            attachedName = os.path.splitext(fn)[0]
        sql = "ATTACH DATABASE '%s' AS %s" % (dbFilename,
                                              attachedName)
        cursor = Cursor(self)
        cursor.execute(sql)
        self._attachedDBs[attachedName] = dbFilename

        if returnNewDb:
            return self.getAttached(attachedName)

    def detach(self, dbname):
        """Detaches a DB from this one.
        """
        if dbname in self._attachedDBs:
            sql = "DETACH DATABASE '%s'" % dbname
            cursor = Cursor(self)
            cursor.execute(sql)
            del self._attachedDBs[dbname]

    def getAttached(self, dbname):
        """Returns a DB instance with `dbname` as main database
        
        Arguments:
        - `dbname`: Database name
        """
        if dbname not in self._attachedDBs:
            raise KdbomDatabaseError, ('Database %s is not attached' %
                                       dbname)
       
        newDb = self.__class__(self._attachedDBs[dbname], dbname=dbname)
        newDb.connection = self.connection
        newDb._attachedDBs = self._attachedDBs
        newDb.__loadTables()
        return newDb
    

class Table(BaseTable):
    """Sqlite table class
    """
    def __loadFieldsAndRelationships(self):
        """Loads field objects for each field in the table
        definition. At the same time loads the relationships with
        other tables.
        """
        cursor = Cursor(self.db)
        qry = ("SELECT sql FROM %s.sqlite_master WHERE "
               "tbl_name = '%s'") % (self.db, self._name)
        row = cursor.fetchone(qry)
        sql = map(lambda x: x.strip(), row[0].split('\n'))
        self.sqlDefinition = ''.join(sql)

        self.fields = {}
        self.fieldNames = []
        self.relationships = {}
        self._autoIncField = None
        self._pkField = None

        first = pos = self.sqlDefinition.find('(') + 1
        openParentesis = 1
        while openParentesis > 0:
            pos += 1
            if self.sqlDefinition[pos] == '(':
                openParentesis += 1
            elif self.sqlDefinition[pos] == ')':
                openParentesis -= 1
            else:
                continue

        tempConn = sqlite3.connect(self.db._filename)
        tempConn.isolation_level = None
        indexed = []
        tempCursor = tempConn.cursor()
        tempCursor.execute('PRAGMA index_list(%s)' % self._name)
        rows = tempCursor.fetchall()
        for r in rows:
            tempCursor.execute('PRAGMA index_info(%s)' % r[1])
            idx_rows = tempCursor.fetchall()
            indexed.append(idx_rows[0][2])

        tempCursor.execute('PRAGMA table_info(%s)' % self._name)
        rows = tempCursor.fetchall()
        tempConn.close()
        
        rows = dict([(r[1], r[2:]) for r in rows])
        fieldDefs = self.sqlDefinition[first:pos].split(',')
        fieldDefs = map(lambda s: s.strip(), fieldDefs)

        self._fieldDefs = {}
        for fieldDef in fieldDefs:
            parts = fieldDef.split(' ', 1)
            fieldName = parts[0].strip("'")
            if fieldName not in rows:
                continue
            self._fieldDefs[fieldName] = fieldDef
            isAutoInc = False
            dataType = rows[fieldName][0]
            if dataType == '':
                dataType = 'varchar(255)'
            extra = ''
            nullAllowed = rows[fieldName][1] == 0
            indexing = ''
            match = re.search('PRIMARY KEY', fieldDef)
            isPrimaryKey = bool(match)
            match = re.search('AUTOINCREMENT', fieldDef)
            isAutoInc = bool(match)
            if isAutoInc:
                extra += ' AUTOINCREMENT'
            match = re.search('(PRIMARY|UNIQUE)', fieldDef)
            indexing = ''
            if match:
                indexing = match.group(0)[0:3]
            elif fieldName in indexed:
                indexing = 'MUL'
            default = rows[fieldName][2]
            fieldObj = self.db.fieldClass(table=self,
                                          name=fieldName,
                                          dataType=dataType,
                                          nullAllowed=nullAllowed,
                                          indexing=indexing,
                                          extra=extra,
                                          default=default)
            self.fields[fieldName] = fieldObj
            self.fieldNames.append(fieldName)
            if isPrimaryKey:
                self._pkField = fieldObj
            if isAutoInc:
                self._autoIncField = fieldObj

        self.field_names = self.fieldNames

        if self._pkField is None:
            self._pkField = self._autoIncField
        self.primary_key = self._pkField

    def loadRelationships(self):
        for name, definition in self._fieldDefs.iteritems():
            match = re.search(('REFERENCES (?P<table>\w+)\s*'
                               '\((?P<field>\w+)\)'), definition)
            if match:
                fTableName = match.group('table')
                fFieldName = match.group('field')
                if fTableName not in self.db.tables:
                    fTable = self.db.tableClass(db=self.db,
                                                name=fTableName)
                    self.db.tables[fTableName] = fTable
                else:
                    fTable = self.db.tables[fTableName]
                fField = fTable.fields[fFieldName]
                fieldObj = self.fields[name]
                rel = self.db.relationClass(parent=fField,
                                            child=fieldObj)
                self.relationships[fTable] = rel
                fTable.relationships[self] = rel


    def cloneTable(self, newName):
        """Creates and returns a new table with the same design.
        """
        cursor = Cursor(self.db)
        cursor.execute("CREATE TABLE %s.`%s` LIKE %s" % \
                       (self.db, newName, self))
        newTable = self.__class__(db=self.db, name=newName)
        self.db.tables[newName] = newTable
        return newTable

    def disableKeys(self):
        pass

    def enableKeys(self):
        pass

    def __str__(self):
        """Returns the name of the DB already quoted for use in a
        query
        """
        return '%s.`%s`' % (self.db, self._name)

    _dbQualName = __str__
    dbQualName = _dbQualName


class Field(BaseField):
    """Represents a field in a MySQL table.
    """

    def escape(self, value):
        """Returns the value ready to be used in a query.
        
        Arguments:
        - `value`: Value to prepare.
        """
        if value is None:
            return 'NULL'
        if type(value) in types.StringTypes and value != '%s':
            value = value.replace('%', '%%')
        v = "'%s'" % str(self.convertForDB(value)).replace("'", "''")
        return v.decode('utf-8','ignore')

    escapeForDB = escape


sqlFunctions = ('AVG COUNT GROUP_CONCAT MAX MIN SUM TOTAL ABS '
                'CHANGES COALESCE GLOB IFNULL HEX LENGTH LIKE LOWER '
                'LTRIM NULLIF QUOTE RANDOM RANDOMBLOB REPLACE ROUND '
                'RTRIM SOUNDEX SUBSTR TRIM TYPEOF UPPER ZEROBLOB DATE '
                'TIME DATETIME JULIANDAY STRFTIME DISTINCT')

for fun in sqlFunctions.split():
    setattr(kdbom2.query, fun, DummyFunctionClass.makeClass(fun))
