#
# KDBOM - Kael's Database Object Model
# Query module.
#
# Provides Condition classes and the base of the Query classes of the
# drivers.
#
__version__ = tuple([int(x) for x in
                     '$Revision: 1.84 $'.split()[1].split('.')])
__author__ = "Kael Fischer and Julio Carlos Menendez"
__all__ = ['Condition', 'LiteralCondition', 'CompoundCondition',
           'AndCondition', 'OrCondition', 'IsNullCondition',
           'IsNotNullCondition', 'LikeCondition', 'NotLikeCondition',
           'Expression', 'Operation', 'AddOperation',
           'SubtractOperation', 'MultiplyOperation', 'BaseCondition',
           'DivideOperation', 'PowerOperation', 'ModuloOperation',
           'Alias', 'AS', 'BaseTable', 'BetweenCondition', 'InCondition',]


import types
import copy
import re
import traceback

from kdbom2.util import getIterable, flatten
from kdbom2.exceptions import *
from kdbom2.drivers.__init__ import BaseField, Relationship


class BaseCondition(object):
    """Base condition class
    """

    def __str__(self):
        """Returns the SQL representation of this condition.
        Must be implemented in the descendant classes.
        """
        raise NotImplementedError

    def copy(self):
        """Returns a deep copy of this condition.
        """
        raise NotImplementedError()

    def __deepcopy__(self, memo):
        return self.copy()

    def __and__(self, other):
        """Implements the operation C1 & C2. Allows to join with AND a
        deep copy of this condition and `other`. Doesn't modify
        `self`.

        Arguments:
        - `other`: Other BaseCondition subclass instance.
        """
        if other is None:
            return self.copy()
        
        if not isinstance(other, (Expression, BaseCondition)):
            raise KdbomProgrammingError, 'Invalid type'
        
        _self = self.copy()
        other = other.copy()
        return AndCondition(_self, other)

    def __or__(self, other):
        """Implements the operation C1 | C2. Allows to join with OR a
        deep copy of this condition and `other`. Doesn't modify
        `self`.

        Arguments:
        - `other`: Other BaseCondition subclass instance.
        """
        if other is None:
            return self.copy()
        
        if not isinstance(other, (Expression, BaseCondition)):
            raise KdbomProgrammingError, 'Invalid type'
        
        _self = self.copy()
        other = other.copy()
        return OrCondition(_self, other)

    def NOT(self):
        """Returns a NotCondition of this one.
        """
        return NotCondition(self)


class CompoundCondition(BaseCondition, list):
    """Represents a list of conditions joined by 'AND' or 'OR'
    """

    def __init__(self, *args, **kwargs):
        """Initializes the compound condition.
        
        Arguments:
        - `joinBy`: Must be one of ('AND', 'OR')
        - `*args`: Conditions can also be passed outside the
        `conditions` argument.
        - `**kwargs`: Each pair key:value will be used to create a
        Condition like 'key = value'
        """
        if 'joinBy' in kwargs:
            self.joinBy = kwargs['joinBy']
            del kwargs['joinBy']
        else:
            self.joinBy = 'AND'

        if 'escapeFn' in kwargs:
            self.escapeFn = kwargs['escapeFn']
            del kwargs['escapeFn']
        else:
            self.escapeFn = None

        if len(args) == 0:
            for field, value in kwargs.iteritems():
                self.append(Condition(field, value,
                                      escapeFn=self.escapeFn))
        elif len(args) == 1 and len(kwargs) == 0 and \
                 type(args[0]) is types.DictType:
            for field, value in args[0].iteritems():
                self.append(Condition(field, value,
                                      escapeFn=self.escapeFn))
        else:
            for cond in args:
                if isinstance(cond, (Expression, BaseCondition)):
                    self.append(cond)
                elif type(cond) in types.StringTypes:
                    self.append(LiteralCondition(cond))
                else:
                    raise KdbomProgrammingError, ('Invalid condition '
                                                  'type %s' % type(cond))
            for field, value in kwargs.iteritems():
                self.append(Condition(field, value,
                                      escapeFn=self.escapeFn))
        
    def __unicode__(self):
        """Returns the SQL representation of this condition.
        """
        joinBy = ' %s\n' % self.joinBy
        conditions = []
        for cond in self:
            if isinstance(cond, CompoundCondition) and len(cond) > 1:
                conditions.append('(%s)' % cond)
            else:
                try:
                    strCond = str(cond)
                except (UnicodeEncodeError, UnicodeDecodeError):
                    strCond = unicode(cond)
                if len(strCond.strip()) > 0:
                    conditions.append(strCond)
        return joinBy.join(map(lambda x: unicode(x, 'utf-8'), conditions))

    def __str__(self):
        return unicode(self).encode('utf-8')

    def copy(self):
        """Returns a copy of this condition.
        """
        _self = self.__class__(*map(lambda x: x.copy(), self))
        _self.joinBy = self.joinBy
        return _self

    def fields(self):
        """Returns a set with the fields involved in this condition.
        """
        fields = set([])
        for cond in self:
            if isinstance(cond, CompoundCondition):
                fields = fields.union(cond.fields())
            elif isinstance(cond, Expression):
                fields = fields.union(cond.fields)
            elif cond.field is not None:
                fields.add(cond.field)
        return fields

    def __iand__(self, other):
        """Implements the operation C1 &= C2. Allows to join with AND a
        deep copy of this condition and `other`. Modifies `self`.

        Arguments:
        - `other`: Other BaseCondition subclass instance.
        """
        if not isinstance(other, (Expression, BaseCondition)):
            return NotImplemented
        
        if self.joinBy == 'AND':
            self.append(other)
        else:
            _self = self.copy()
            self.joinBy = 'AND'
            while len(self):
                self.pop()
            self.extend([_self, other])
        return self

    def __ior__(self, other):
        """Implements the operation C1 |= C2. Allows to join with AND a
        deep copy of this condition and `other`. Modifies `self`.

        Arguments:
        - `other`: Other BaseCondition subclass instance.
        """
        if not isinstance(other, (Expression, BaseCondition)):
            return NotImplemented
        
        if self.joinBy == 'OR':
            self.append(other)
        else:
            _self = self.copy()
            self.joinBy = 'OR'
            while len(self):
                self.pop()
            self.extend([_self, other])
        return self


class AndCondition(CompoundCondition):
    """Represents an AND compound condition
    """
    
    def __init__(self, *args, **kwargs):
        """Initializes the compound condition using 'AND' as join.
        """
        CompoundCondition.__init__(self, joinBy='AND', *args,
                                   **kwargs)


class OrCondition(CompoundCondition):
    """Represents an OR compound condition
    """
    
    def __init__(self, *args, **kwargs):
        """Initializes the compound condition using 'OR' as join.
        """
        CompoundCondition.__init__(self, joinBy='OR', *args, **kwargs)


class Condition(CompoundCondition):
    """Represents a single condition in a WHERE clause.
    Provides an interface to write conditions in a natural way
    without breaking the normal behavior of the operator in other
    contexts.
    i.e.: Field1 > 10 will be represented as a SQL condition when used
    in a Query and represented as a boolean when used in a truth value
    testing.
    """

    def __init__(self, *args, **kwargs):
        """Initializes the condition.
        """
        if len(args) > 0 and isinstance(args[0], (Alias, AliasField,
                                                  BaseField,
                                                  types.NoneType,
                                                  Expression)):
            sc = SingleCondition(*args, **kwargs)
            CompoundCondition.__init__(self, sc)
        else:
            CompoundCondition.__init__(self, *args, **kwargs)

    def __nonzero__(self):
        if len(self) == 1:
            return self[0].__nonzero__()
        else:
            return False


class SingleCondition(BaseCondition):
    def __init__(self, field, value, operator='=',
                 escapeFn=None, valueWildcard=None):
        """Initializes the condition.
        
        Arguments:
        - `field`: Field name or Field instance
        - `value`: Value to compare to
        - `operator`: Operator for comparison. Default: =
        - `escapeFn`: Function used to escape `value` before returning
        it in the str representation of the condition. It must accept
        the value and optionally the field of the condition as 
        argument and must return an string.
        - `valueWildcard`: Wildcard used to pass values later having
        the DB engine escape it. Ex: In MySQL is %s, in SQLite is ?
        """
        self.field = field
        if escapeFn is None:
            self.escapeFn = self.escape
        else:
            self.escapeFn = escapeFn
        self.value = value
        if operator == '<>':
            operator = '!='
        if operator == '==':
            operator = '='
        self.operator = operator
        self.wildcard = valueWildcard

    def escape(self, value, field=None):
        """Returns a SQL safe value. If `field` is set then its used
        to escape the value.

        Arguments:
        - `value`: The value to escape.
        - `field`: Instance of any BaseField subclass.
        """
        if value == '%s':
            return self.wildcard
        if value is None:
            return 'NULL'
        if isinstance(value, (Expression, BaseField)):
            return str(value)
        if field is not None and isinstance(field, BaseField):
            return field.escape(value)
        if type(value) not in types.StringTypes:
            _value = "%s" % value
        else:
            _value = "'%s'" % value
        _value = '%s' % _value.replace("'", "\'")
        return _value
    
    def __unicode__(self):
        """Returns the SQL representation of this condition.
        """
        if isinstance(self.field, Operation):
            fieldStr = '(%s)' % self.field
        elif isinstance(self.field, Alias):
            fieldStr = self.field.alias
        else:
            fieldStr = '%s' % self.field

        if type(self.value) is types.DictionaryType:
            _values = dict((k, self.escapeFn(v))
                           for k, v in self.value.iteritems())
            part = self.operator % _values
            try:
                returnVal = '%s %s' % (fieldStr, part)
            except UnicodeDecodeError, ex:
                returnVal = unicode('%s %s') % (fieldStr, part)
        else:
            try:
                returnVal =  '%s %s %s' % (fieldStr, self.operator,
                                           self.escapeFn(self.value,
                                                         self.field))
            except UnicodeDecodeError, ex:
                uVal = self.escapeFn(self.value, self.field)
                returnVal =  ('%s %s' % (fieldStr, self.operator) +
                              unicode(uVal, "utf-8"))
        return returnVal

    def __str__(self):
        return unicode(self).encode('utf-8')

    def copy(self):
        """Returns a copy of this condition.
        """
        _self = self.__class__(self.field, self.value, self.operator,
                               valueWildcard=self.wildcard)
        if self.escapeFn is not self.escape:
            _self.escapeFn = self.escapeFn
        return _self

    def __nonzero__(self):
        if type(self.value) is types.DictionaryType:
            return False
        c = self.field.__cmp__(self.value)
        if self.operator == '>':
            return c > 0
        if self.operator == '>=':
            return c > 0 or c == 0
        if self.operator == '=':
            return c == 0
        if self.operator == '<=':
            return c == 0 or c < 0
        if self.operator == '<':
            return c < 0
        if self.operator == '!=':
            return c != 0


class BetweenCondition(Condition):
    """Represents a BETWEEN condition.
    """

    def __init__(self, field, low, high, *args, **kwargs):
        """Initializes the condition.

        Arguments:
        - `field`: Field name or Field instance
        - `low`: Integer or Field or Expression to used as the start
        of the range.
        - `high`: Integer or Field or Expression to used as the end
        of the range.
        """
        self._low = low
        self._high = high
        self._field = field
        frmat = 'BETWEEN %(low)s AND %(high)s'
        values = dict(low=low, high=high)
        Condition.__init__(self, field, values, frmat)

    def copy(self):
        return self.__class__(self._field, self._low, self._high)


class InCondition(Condition):
    """Represents an IN condition.
    """

    def __init__(self, field, value_list, *args, **kwargs):
        """Initializes the condition.

        Arguments:
        - `field`: Field name or Field instance
        - `value_list`: List of values the field needs to be in.
        """
        self._value_list = value_list
        self._field = field
        frmat = 'IN (%s)' % ','.join(map(str, value_list))
        Condition.__init__(self, field, dict(), frmat)

    def copy(self):
        return self.__class__(self._field, self._value_list)


class LiteralCondition(Condition):
    """Represents a condition written as a string directly.
    """
    
    def __init__(self, condition):
        """Initializes the condition
        
        Arguments:
        - `condition`: A SQL condition
        """
        self._value = condition
        Condition.__init__(self, None, condition)

    def __str__(self):
        """Returns the SQL representation of this condition.
        """
        return self._value

    def copy(self):
        """Returns a copy of this condition.
        """
        return LiteralCondition(self._value)

class NotCondition(Condition):
    """Represents a condition preceded by NOT.
    """

    def __init__(self, condition):
        self._value = condition
        Condition.__init__(self, None, condition)

    def __str__(self):
        return 'NOT %s' % self._value

    __repr__ = __str__

    def copy(self):
        return self.__class__(self._value)


class IsNullCondition(Condition):
    """Represents an field IS NULL condition
    """
    
    def __init__(self, field, *args, **kwargs):
        """Initializes the condition. *args and **kwargs are ignored,
        only there for compatibility with Condition.copy.
        
        Arguments:
        - `field`: Field name or Field instance
        - `*args`: Ignored
        - `**kwargs`: Ignored
        """
        self._field = field
        Condition.__init__(self, field, None, 'IS')

    def __nonzero__(self):
        return self._field.__cmp__(None)

    def copy(self):
        return self.__class__(self._field)


class IsNotNullCondition(Condition):
    """Represents an field IS NOT NULL condition
    """

    def __init__(self, field, *args, **kwargs):
        """Initializes the condition. *args and **kwargs are ignored,
        only there for compatibility with Condition.copy.
        
        Arguments:
        - `field`: Field name or Field instance
        - `*args`: Ignored
        - `**kwargs`: Ignored
        """
        self._field = field
        Condition.__init__(self, field, None, 'IS NOT')

    def __nonzero__(self):
        return self._field.__cmp__(None)

    def copy(self):
        return self.__class__(self._field)


class _FieldKeywordValueCondition(Condition):
    """Represents a field KEYWORD 'value' condition
    """
    
    def __init__(self, keyword, field, value, *args, **kwargs):
        """Initializes the condition. *args and **kwargs are ignored,
        only there for compatibility with Condition.copy.
        
        Arguments:
        - `keyword`: Keyword for the condition
        - `field`: Field name or Field instance
        - `value`: Value to compare to.
        - `*args`: Ignored
        - `**kwargs`: Ignored
        """
        self._field = field
        self._value = value
        Condition.__init__(self, field, value, keyword)

    def escape(self, value, field=None):
        """Returns a SQL safe value. If `field` is set then its used
        to escape the value.

        Arguments:
        - `value`: The value to escape.
        - `field`: Instance of any BaseField subclass.
        """
        if value == '%s':
            return self.wildcard
        if value is None:
            return 'NULL'
        if field is not None:
            if isinstance(value, Expression):
                return str(value)
        if type(value) not in types.StringTypes:
            _value = "%s" % value
        else:
            _value = "'%s'" % value
        _value = '%s' % _value.replace("'", "\'")
        return _value

    def copy(self):
        return self.__class__(self._field, self._value)

    def __nonzero__(self):
        return self._field.__cmp__(self._value)


class LikeCondition(_FieldKeywordValueCondition):
    """Represents a field LIKE 'value' condition
    """
    
    def __init__(self, field, value, *args, **kwargs):
        """Initializes the condition. *args and **kwargs are ignored,
        only there for compatibility with Condition.copy.
        """
        _FieldKeywordValueCondition.__init__(self, 'LIKE', field,
                                              value) 

        
class NotLikeCondition(_FieldKeywordValueCondition):
    """Represents a field NOT LIKE 'value' condition
    """
    
    def __init__(self, field, value, *args, **kwargs):
        """Initializes the condition. *args and **kwargs are ignored,
        only there for compatibility with Condition.copy.
        """
        _FieldKeywordValueCondition.__init__(self, 'NOT LIKE', field,
                                              value) 


class RegexpCondition(_FieldKeywordValueCondition):
    """Represents a field REGEXP 'value' condition
    """

    def __init__(self, field, value, *args, **kwargs):
        """Initializes the condition. *args and **kwargs are ignored,
        only there for compatibility with Condition.copy.
        """
        _FieldKeywordValueCondition.__init__(self, 'REGEXP', field,
                                              value) 


class NotRegexpCondition(_FieldKeywordValueCondition):
    """Represents a field NOT REGEXP 'value' condition
    """

    def __init__(self, field, value, *args, **kwargs):
        """Initializes the condition. *args and **kwargs are ignored,
        only there for compatibility with Condition.copy.
        """
        _FieldKeywordValueCondition.__init__(self, 'NOT REGEXP', field,
                                              value)
        

class BaseQuery(object):
    """Base class for each driver's Query class.
    """
    
    def __init__(self, table, dontVisitTables=None, **kwargs):
        """Initializes the query
        
        Arguments:
        - `table`: Main table of this query. Used in the FROM clause.
        - `dontVisitTables`: Table or list of tables to avoid visiting
        when generating the required JOIN clauses.
        """
        self._table = table
        self._avoidTables = set(getIterable(dontVisitTables))
        if 'avoidTables' in kwargs:
            self._avoidTables = set(getIterable(kwargs['avoidTables']))
        self._db = table.db
        self._wildcard = None
        self.reset()

    def __setType(self, typeClause):
        """Sets the type of this query: SELECT, UPDATE, DELETE or
        INSERT.
        
        Arguments:
        - `typeClause`:
        """
        if self._queryType is not None:
            raise KdbomProgrammingError, ('This query already have a '
                                          'type assigned. The current '
                                          'type is %s' %
                                          self._queryType)
        self._queryType = typeClause

    def __getType(self):
        """Returns the type of this query
        """
        return self._queryType

    type = property(__getType, __setType)

    def avoidTables(self,tables):
        """Set which tables the joining engine should avoid,
        because e.g. circular relationship paths in DB structure. 
        
        This resets the currently avoided tables, perhpse set
        with Query.__init__.

        Returns the Query
        """
        self._avoidTables = set(tables)
        return self

    def reset(self):
        """Clears the query.
        """
        self._queryType = None
        self._attributes = {}
        self._tablesNeeded = set([])
        self._joinsNeeded = []
        self._joinTypes = {}
        self._forceJoinTables = {}
        self._forceJoinTablesOrder = []
        self._aliases = {}
        self._lastAlias = -1
        self._selfJoins = []
        self._needDBiEscape = {}
        self.resetWhere()
        self.resetHaving()

    def resetSelect(self):
        """Resets the fields of the SELECT clause.
        """
        self._queryType = None
        self._attributes['modifiers'] = set()
        try:
            del self._attributes['fields']
        except KeyError:
            pass
        
    def resetWhere(self):
        """Resets the conditions of the WHERE clause.
        """
        self._attributes['condition'] = AndCondition()
        self._attributes['modifiers'] = set()

    def resetHaving(self):
        """Resets the conditions of the HAVING clause.
        """
        self._attributes['having'] = AndCondition()

    def resetUpdate(self):
        """Resets the pairs of fiel:value of the UPDATE clause.
        """
        self._queryType = None
        self._attributes['modifiers'] = set()
        try:
            del self._attributes['updatevalues']
        except KeyError:
            pass

    def resetInsert(self):
        """Resets the values of the INSERT clause.
        """
        self._queryType = None
        self._attributes['modifiers'] = set()
        for k in ['insertvalues', 'insertfrom', 'insertfields']:
            try:
                del self._attributes[k]
            except KeyError:
                pass

    def resetOrderBy(self):
        """Resets the ORDER BY clause.
        """
        try:
            del self._attributes['orderby']
        except KeyError:
            pass

    def resetGroupBy(self):
        """Resets the GROUP BY clause.
        """
        try:
            self._attributes['groupby'] = set([])
        except KeyError:
            pass

    def resetLimit(self):
        """Resets the LIMIT clause.
        """
        try:
            del self._attributes['limit']
        except KeyError:
            pass   

    def select(self, fields=None, funcs=None):
        """Adds `fields` to the SELECT clause.
        
        Arguments:
        - `fields`: One or a list of fields. Can be the name of the
        field or the actual Field object.
        - `func`: One or a list of valid SQL aggregators or
        functions to apply to all `fields`. 
        """
        if self.type != 'SELECT':
            self.type = 'SELECT'
        fields = getIterable(fields)
        funcs = getIterable(funcs)
        swap = False
        if 'LITERAL' in fields:
            swap = True
        
        if len(funcs) > 0 and 'LITERAL' not in funcs:
            if all([x in types.StringTypes for x in funcs]):
                baseFields = self._table.fields.keys()
                isField = lambda x: bool(x in baseFields or '.' in x)
                if all([isField(x) for x in funcs]):
                    swap = True
                    if fields is None:
                        raise KdbomProgrammingError, 'Invalid arguments'
            else:
                swap = True
        if swap:
            funcs, fields = fields, funcs
        if not self._attributes.has_key('fields'):
            self._attributes['fields'] = []
        if funcs is not None:
            fields = map(lambda x: (x, funcs), fields)
        self._attributes['fields'].extend(fields)

    def selectCount(self, fields=None, funcs=None):
        """Adds COUNT() with the fields and funcs specified
        """
        funcs = list(getIterable(funcs))
        fields = getIterable(fields)
        funcs= ['COUNT'] + funcs
        if len(fields) == 0:
            fields = '*'
            funcs.append('LITERAL')
        self.select(funcs,fields)

    def selectCountDistinct(self,fields):
        """Adds COUNT(DISTINCT (fields))) to select clause.
        """
        self.selectCount(fields,'DISTINCT')

    def selectDistinct(self,fields=None, funcs=None):
        """Adds DISTINCT(fields) to select clause
        """
        funcs = list(getIterable(funcs))
        fields = getIterable(fields)
        funcs= ['DISTINCT'] + funcs
        self.select(funcs,fields)
        
    def selectGroupBy(self, *field):
        """Adds `*fields` to the SELECT clause and to the GROUP BY
        clause.
        """
        self.select(list(field))
        self.groupBy(*field)

    def selectID(self):
        """Adds the primary key of the main table to the select
        clause.
        """
        self.select(self._table.primary_key)

    def countBy(self,fields):
        """Adds field (or fields) to GROUP BY and SELECT
        field lists.
        """
        self.select(fields)
        self.selectCount()
        self.groupBy(fields)

    def update(self, pairs=None, **more):
        """Adds pairs Field=value to the UPDATE clause
        
        Arguments:
        - `pairs`:
        - `**more`:
        """
        if pairs is None:
            pairs = {}
        
        self.type = 'UPDATE'
        pairs.update(more)
        if not self._attributes.has_key('updatevalues'):
            self._attributes['updatevalues'] = {}
        self._attributes['updatevalues'].update(pairs)

    def delete(self):
        """Sets the query to use the DELETE clause.
        """
        self.type = 'DELETE'

    def insert(self, pairs=None, ignore=False, **more):
        """Adds the fields and values for the INSERT clause
        """
        if pairs is None:
            pairs = {}
        
        self.type = 'INSERT'
        if isinstance(pairs, BaseQuery):
            if len(pairs.fields) > 0:
                names = []
                for f in pairs.fields:
                    if isinstance(f, (Alias, AliasField, BaseField)):
                        names.append(f.name)
                    else:
                        names.append(str(f))
                curnames = []
                for f in self._table.fields:
                    if isinstance(f, (AliasField, BaseField)):
                        curnames.append(f.name)
                    else:
                        curnames.append(str(f))
                if len(set(names).difference(curnames)) > 0:
                    msg = ('Not all the fields present on the SELECT '
                           'query matches a field on the destination '
                           'table.\n'
                           'SELECT fields: %s\n'
                           'table fields: %s') % (names, curnames)
                    raise KdbomProgrammingError, msg
                self._attributes['insertfields'] = names
            self._attributes['insertfrom'] = pairs
        else:
            pairs.update(more)
            if not self._attributes.has_key('insertvalues'):
                self._attributes['insertvalues'] = {}
            self._attributes['insertvalues'].update(pairs)
        if ignore:
            self.modifier('IGNORE')

    def where(self, criteria=None, literalCriteria=None, operator='=', 
              requiredTables=None, *conditions, **values):
        """Adds conditions to the query.
        """
        if isinstance(criteria, (Expression, BaseCondition)):
            self._attributes['condition'].append(criteria)
            _criteria = {}
        elif criteria is None:
            _criteria = {}
        elif type(criteria) is types.DictType:
            _criteria = criteria

        if isinstance(literalCriteria, (Expression, BaseCondition)):
            self._attributes['condition'].append(literalCriteria)
            literalCriteria = None

        if isinstance(operator, (Expression, BaseCondition)):
            self._attributes['condition'].append(operator)
            operator = '='

        if isinstance(requiredTables, (Expression, BaseCondition)):
            self._attributes['condition'].append(requiredTables)
            requiredTables = None

        for cond in conditions:
            if not isinstance(cond, (Expression, BaseCondition)):
                raise KdbomProgrammingError, ('%s is not a condition'
                                              % cond)
            self._attributes['condition'].append(cond)

        operator = operator.upper()
        if type(_criteria) is types.DictType:
            _criteria.update(values)
            _conditions = []
            for field, value in _criteria.iteritems():
                fObj, fSql = self.__getFieldObj(field)
                if fObj is not None:
                    field = fObj
                if value is None:
                    if operator == 'IS NOT':
                        c = IsNotNullCondition(field)
                    else:
                        c = IsNullCondition(field)
                else:
                    c = Condition(field, value, operator)
                _conditions.append(c)
            self._attributes['condition'].extend(_conditions)

        literalCriteria = getIterable(literalCriteria)
        for c in literalCriteria:
            self._attributes['condition'].append(LiteralCondition(c))

        tables = getIterable(requiredTables)
        for table in tables:
            if type(table) is not Table:
                table = self._table.db.tables[table]
            self._tablesNeeded.add(table)
        return self._attributes['condition']
 
    def whereLike(self, field, value):
        """Adds a `field LIKE value` condition to the WHERE clause of
        this query.

        Arguments:
        - `field`: BaseField instance or field name.
        - `value`: Value for the condition.
        """
        condition = LikeCondition(field, value)
        self._attributes['condition'].append(condition)

    def whereNotLike(self, field, value):
        """Adds a `field NOT LIKE value` condition to the WHERE clause
        of this query.

        Arguments:
        - `field`: BaseField instance or field name.
        - `value`: Value for the condition.
        """
        condition = NotLikeCondition(field, value)
        self._attributes['condition'].append(condition)

    def whereIsNull(self, field):
        """Adds a `field IS NULL` condition to the WHERE clause of
        this query.

        Arguments:
        - `field`: BaseField instance or field name.
        """
        condition = IsNullCondition(field)
        self._attributes['condition'].append(condition)

    def whereIsNotNull(self, field):
        """Adds a `field IS NOT NULL` condition to the WHERE clause of
        this query.

        Arguments:
        - `field`: BaseField instance or field name.
        """
        condition = IsNotNullCondition(field)
        self._attributes['condition'].append(condition)

    def whereRegexp(self, field, value):
        """Adds a `field REGEXP 'value'` condition to the WHERE clause
        of this query.

        Arguments:
        - `field`: BaseField instance or field name.
        - `value`: Regular expression.
        """
        condition = RegexpCondition(field, value)
        self._attributes['condition'].append(condition)

    def whereNotRegexp(self, field, value):
        """Adds a `field NOT REGEXP 'value'` condition to the WHERE clause
        of this query.

        Arguments:
        - `field`: BaseField instance or field name.
        - `value`: Regular expression.
        """
        condition = NotRegexpCondition(field, value)
        self._attributes['condition'].append(condition)


    whereNull = whereIsNull
    whereNotNull = whereIsNotNull

    def groupBy(self, *field):
        """Adds `field` to the GROUP BY clause.

        Arguments:
        - `field`: One or a list of Field objects or field names.
        """
        fields = getIterable(field)
        if len(fields) == 0:
            return
        if not self._attributes.has_key('groupby'):
            self._attributes['groupby'] = set([])
        self._attributes['groupby'].update(fields)

    def orderBy(self, field, desc=False, literal=False):
        """Adds `field` to the ORDER BY clause.

        Arguments:
        - `field`: One or a list of Field objects or field
        names. Notice that if is a list then all its elements will be
        ordered using the same order.
        - `desc`: If True order will be DESC. ASC if False.
        """
        fields = getIterable(field)
        if len(fields) == 0:
            return
        if not self._attributes.has_key('orderby'):
            self._attributes['orderby'] = []

        if isinstance(desc, (BaseField, Expression)):
            raise KdbomProgrammingError, ('Argument `desc` cannot be '
                                          'a Field nor Expression')
        
        if (desc is True) | (desc is 1):
            order = 'DESC'
        else:
            order = 'ASC'
        self._attributes['orderby'].extend(map(lambda x: (x, order,
                                                          literal),
                                               fields))

    def limit(self, numRows, offset=0):
        """Adds the LIMIT clause. If it was already added will update
        it instead.
        
        Arguments:
        - `numRows`: The number of rows to return.
        - `offset`: The offset of the results.
        """
        self._attributes['limit'] = (numRows, offset)

    def having(self, criteria=None, literalCriteria=None,
               operator='=', requiredTables=None, *conditions,
               **values):
        """Adds HAVING conditions.
        """
        if isinstance(criteria, (Expression, BaseCondition)):
            self._attributes['having'].append(criteria)
            _criteria = {}
        elif criteria is None:
            _criteria = {}
        elif type(criteria) is types.DictType:
            _criteria = criteria

        if isinstance(literalCriteria, (Expression, BaseCondition)):
            self._attributes['having'].append(literalCriteria)
            literalCriteria = None

        if isinstance(operator, (Expression, BaseCondition)):
            self._attributes['having'].append(operator)
            operator = '='

        if isinstance(requiredTables, (Expression, BaseCondition)):
            self._attributes['having'].append(requiredTables)
            requiredTables = None

        for cond in conditions:
            if not isinstance(cond, (Expression, BaseCondition)):
                raise KdbomProgrammingError, ('%s is not a condition'
                                              % cond)
            self._attributes['having'].append(cond)

        operator = operator.upper()
        if type(_criteria) is types.DictType:
            _criteria.update(values)
            _conditions = []
            for field, value in _criteria.iteritems():
                fObj, fSql = self.__getFieldObj(field)
                if fObj is not None:
                    field = fObj
                if value is None:
                    if operator == 'IS NOT':
                        c = IsNotNullCondition(field)
                    else:
                        c = IsNullCondition(field)
                else:
                    c = Condition(field, value, operator)
                _conditions.append(c)
            self._attributes['having'].extend(_conditions)

        literalCriteria = getIterable(literalCriteria)
        for c in literalCriteria:
            self._attributes['having'].append(LiteralCondition(c))

        tables = getIterable(requiredTables)
        for table in tables:
            if type(table) is not Table:
                table = self._table.db.tables[table]
            self._tablesNeeded.add(table)
        return self._attributes['having']

    def __getFieldObj(self, fieldName):
        """Returns the actual field object if possible.
        """
        if isinstance(fieldName, BaseField):
            self._tablesNeeded.add(fieldName.table)
            return (fieldName, str(fieldName))

        if isinstance(fieldName, AliasField):
            return (fieldName, str(fieldName))

        if isinstance(fieldName, (CompoundCondition, Expression)):
            for f in fieldName.fields:
                if not isinstance(f, (Expression, AliasField)):
                    self._tablesNeeded.add(f.table)
            return (fieldName, str(fieldName))

        if isinstance(fieldName, SingleCondition):
            if not isinstance(fieldName.field, (Expression, AliasField)):
                self._tablesNeeded.add(fieldName.field.table)
            return (fieldName, str(fieldName))

        tn = set([self._table])
        tn.update(self._tablesNeeded)
        fieldObj = None
        collisions = -1
        for tbl in tn:
            if fieldName in tbl.fieldNames:
                fieldObj = tbl.fields[fieldName]
                collisions += 1
        if collisions > 0:
            raise KdbomProgrammingError, (
                'Query exception: Field `%s` appears in '
                'more than one table in this '
                'query.') % fieldName
        if fieldObj is None:
            parts = map(lambda x: x.strip('`'), fieldName.split('.'))
            table, field = None, None
            if len(parts) == 3:
                table = parts[1]
                field = parts[2]
            elif len(parts) == 2:
                table = parts[0]
                field = parts[1]
            if table is not None and field is not None:
                if table in self._db.tables:
                    tableObj = self._db.tables[table]
                elif table in self._aliases:
                    tableObj = self._aliases[table]
                else:
                    return (None, fieldName)
                fieldObj = tableObj.fields[field]
        if fieldObj is not None:
            tn.add(fieldObj.table)
        tn.remove(self._table)
        self._tablesNeeded.update(tn)
        if fieldObj is None:
            return (fieldObj, fieldName)
        return (fieldObj, str(fieldObj))

    def __generateJoin(self, needed):
        """Generates the relations and the SQL of the JOIN clause
        from those relations.
        """
        forceJoinDelayed = []
        joinsSql = ''

        # Adds joins inserted by the user but leave for later those that
        # have fields in tables that need join discovery.
        for table in self._forceJoinTablesOrder:
            joint = self._forceJoinTables[table]

            fieldsNeeded = filter(lambda x: x.table in needed,
                                  joint.condition.fields())
            if len(fieldsNeeded) > 0:
                forceJoinDelayed.append(joint)
            else:
                joinsSql += '\n' + str(joint)
                try:
                    needed.remove(table)
                except KeyError:
                    pass

        relations = self.__generateRelations(needed)
        if relations == '':
            relations = []
        
        if self._table in needed:
            needed.remove(self._table)
        used = set([self._table])
        for rel in relations:
            joinTable = None
            ts = filter(lambda x: x in needed, rel.tables())
            if len(ts) == 1:
                joinTable = ts[0]
                needed.remove(joinTable)
            else:
                ts = filter(lambda x: x not in used, rel.tables())
                if len(ts) == 1:
                    joinTable = ts[0]
            
            if joinTable:
                used.add(joinTable)
                joinType = self._joinTypes.get(rel, 'LEFT')
                joinsSql += ' \n' + rel.join(joinTable, joinType)

        # Adds the self joins
        for sj in self._selfJoins:
            field1, field2Obj, alias = sj
            joinsSql += ' \nLEFT JOIN %s %s ON %s = %s.`%s`' % \
                        (field2Obj.table, alias, field1, alias,
                         field2Obj.name)
            
        # Adds delayed joins inserted by the user
        if len(forceJoinDelayed) > 0:
            joinsSql += '\n' + '\n'.join(map(str, forceJoinDelayed))
        
        return joinsSql

    def __generateRelations(self, needed):
        """Returns the needed relationship instances.
        """
        if len(needed) == 1:
            t = list(needed)[0]
            if t in self._table.relationships:
                return [self._table.relationships[t],]

        needed.add(self._table)
        avoid = self._avoidTables.difference(needed)
        if len(needed) <= 1:
            return ''

        # Don't calculate a path for those fields with tables already
        # joined by other methods
        needed.difference_update(self._forceJoinTables.keys())

        # Get the path from the main table to each of the needed
        # tables.
        paths = map(lambda x: self._table.pathTo(x, avoid),
                    needed)
        paths = filter(lambda x: x is not None, paths)
        paths.sort(cmp=lambda x,y: cmp(len(x), len(y)), reverse=True)
        
        # Determine the best path; the path with more tables needed in
        # it.
        betterPath = None
        lowerDiff = None
        idx = None

        for i, p in enumerate(paths):
            tblInPath = flatten(map(lambda x: x.tables(), p))
            tblInPath = set(tblInPath)
            diff = len(needed.difference(tblInPath))
            if betterPath is None:
                betterPath = p
                lowerDiff = diff
                idx = i
            elif diff < lowerDiff:
                betterPath = p
                lowerDiff = diff
                idx = i

        if idx is not None:
            del paths[idx]

        if betterPath is not None:
            relations = betterPath
        else:
            relations = []
        for rel in filter(lambda x: x not in relations,
                          self._joinsNeeded):
            relations.append(rel)
        
        tablesInRels = set(flatten(map(lambda x: x.tables(),
                                       relations)))
        needed.difference_update(tablesInRels)

        if self._table in needed:
            needed.remove(self._table)

        # Try to resolve the missing tables.
        if len(needed) > 0:
            for t in needed:
                paths = map(lambda x: x.pathTo(t, avoid),
                            tablesInRels)
                selPath, idx = None, None
                minLength = None
                for i, p in enumerate(paths):
                    if p is None:
                        continue
                    curLength = len(p)
                    if minLength is None:
                        minLength = curLength
                        selPath = p
                        idx = i
                    elif minLength > curLength:
                        minLength = curLength
                        selPath = p
                        idx = i
                if selPath is None:
                    continue
                for rel in selPath:
                    relations.append(rel)

            tablesInRels = set(flatten(map(lambda x: x.tables(),
                                       relations)))

            needed.difference_update(tablesInRels)
            if len(needed) > 0:
                raise KdbomRelationshipError, (
                    'Error generating the JOINs, some '
                    'tables unable to reach: %s' %
                    ', '.join([str(t) for t in needed]))
        
        return relations

    def __generateWhere(self):
        """Generates the WHERE clause.
        """
        condition = str(self.condition)
        if len(condition) > 0:
            return '\nWHERE %s' % condition
        return ''

    def __generateHaving(self):
        """Generates the HAVING clause.
        """
        condition = str(self.havingCondition)
        if len(condition) > 0:
            return '\nHAVING %s' % condition
        return ''


    def __generateLimit(self):
        """Generates the LIMIT clause.
        """
        if self._attributes.has_key('limit'):
            return '\nLIMIT %d OFFSET %d' % \
                       self._attributes['limit']
        return ''

    def __generateOrderBy(self):
        """Generates the ORDER BY clause.
        """
        if self._attributes.has_key('orderby'):
            ordersSql = []
            for f, o, lit in self._attributes['orderby']:
                if not lit:
                    fieldObj, fSql = self.__getFieldObj(f)
                else:
                    fSql = f
                ordersSql.append('%s %s' % (fSql, o))
            return '\nORDER BY %s' % ','.join(ordersSql)
        return ''

    def __generateGroupBy(self):
        """Generates the GROUP BY clause.
        """
        if self._attributes.has_key('groupby'):
            groupBysSql = set([])
            for f in self._attributes['groupby']:
                fieldObj, fSql = self.__getFieldObj(f)
                groupBysSql.add(fSql)
            return '\nGROUP BY %s' % ','.join(groupBysSql)
        return ''

    def __generateSelect(self):
        """Generates the SELECT clause.
        """
        fieldsSql = []
        fields = self._attributes['fields']
        if len(fields) == 0:
            fieldsSql.append('*')
        else:
            for f in fields:
                if type(f) is types.TupleType:
                    f, funcs = f
                    funcs = list(funcs)
                    fSql = ''
                    if 'LITERAL' not in funcs:
                        fieldObj, fSql = self.__getFieldObj(f)
                    else:
                        fSql = str(f)
                        funcs.remove('LITERAL')
                    for func in reversed(funcs):
                        fSql = '%s(%s)' % (func, fSql)
                    fieldsSql.append(fSql)
                else:
                    fieldObj, fSql = self.__getFieldObj(f)
                    fieldsSql.append(fSql)
        return ('SELECT %%(mods)s %s \nFROM %%(table)s %%(join)s '
                '%%(where)s %%(group)s %%(having)s %%(order)s '
                '%%(limit)s') % \
                ','.join(map(lambda x: x.replace('%', '%%'),
                         fieldsSql))

    def __generateUpdate(self):
        """Generates the UPDATE clause.
        """
        pairs = []
        valCount = 0
        for f, v in self._attributes['updatevalues'].iteritems():
            fieldObj, fSql = self.__getFieldObj(f)
            if isinstance(fieldObj, BaseField):
                fSql = fieldObj.name
            fSql = '`%s`' % fSql
            replName = 'uv' + str(valCount)
            valCount += 1
            self._needDBiEscape[replName] = v
            pairs.append('%s=%s' % (fSql, '%%(' + replName + ')s'))
        return ('UPDATE %%(mods)s %%(table)s \nSET %s %%(where)s '
                '%%(order)s %%(limit)s') % ','.join(pairs)

    def __generateDelete(self):
        """Generates the DELETE clause.
        """
        return ('DELETE %(mods)s \nFROM %(table)s %(where)s %(order)s '
                '%(limit)s')

    def __generateInsert(self):
        """Generates the INSERT clause.
        """
        if 'insertfrom' in self._attributes:
            s = 'INSERT %(mods)s INTO %(table)s'
            if 'insertfields' in self._attributes:
                fields = ','.join(self._attributes['insertfields'])
                s += '(%s)' % fields
            s += '\n%s' % self._attributes['insertfrom']
            return s

        cols = []
        needOrder = False
        pos = -1
        replNames = []
        for f, v in self._attributes['insertvalues'].iteritems():
            fieldObj, fSql = self.__getFieldObj(f)
            if isinstance(fieldObj, BaseField):
                fSql = fieldObj.name
            if type(v) is types.TupleType:
                v, pos = v
                needOrder = True
            else:
                pos += 1
            replName = 'iv' + str(pos)
            self._needDBiEscape[replName] = v
            replNames.append('%%(' + replName + ')s')
            fSql = '`%s`' % fSql
            cols.append((fSql, pos))
        if needOrder:
            colsOrdered = ['']*len(cols)
            for c, pos in cols:
                colsOrdered[pos] = c
            cols = colsOrdered
        else:
            cols = map(lambda x: x[0], cols)
        return ('INSERT %%(mods)s \nINTO %%(table)s (%s) \nVALUES(%s)' %
                (','.join(cols), ','.join(replNames)))

    def __wrapSQLline(self,line,colLimit=70):
        """wrap at commas for readbility.
        """
        line=line.strip()
        if len(line) <= colLimit:
            return line

        outLines=[[]]
        lParts=line.split(',')
        if len(lParts) == 1:
            return line

        while len(lParts)>0:
            thisPart=lParts.pop(0)
            if (sum([len(x) for x in outLines[-1]])
                + len(outLines[-1])+len(thisPart)) > colLimit:
                outLines.append([])
            outLines[-1].append(thisPart)
        return ',\n'.join([','.join(row) for row in outLines])
        

    def __unicode__(self):
        """Return the generated SQL query.
        """
        query = ''
        if self.type == 'SELECT':
            query = self.__generateSelect()
        elif self.type == 'UPDATE':
            query = self.__generateUpdate()
        elif self.type == 'DELETE':
            query = self.__generateDelete()
        elif self.type == 'INSERT':
            query = self.__generateInsert()

        whereSql = self.__generateWhere()
        limitSql = self.__generateLimit()
        orderBySql = self.__generateOrderBy()
        groupBySql = self.__generateGroupBy()
        havingSql = self.__generateHaving()

        for table in self.condition.fields():
            self.__getFieldObj(table)


        for table in self.havingCondition.fields():
            self.__getFieldObj(table)

        joinSql = self.__generateJoin(set(self._tablesNeeded))

        mods = ''
        if 'modifiers' in self._attributes:
            mods = ' '.join(self._attributes['modifiers'])

        qryStr = ''
        try:
            qryStr = query % {'table': self._table,
                              'where': unicode(whereSql, 'utf-8'),
                              'limit': limitSql,
                              'order': orderBySql,
                              'group': groupBySql,
                              'having': unicode(havingSql),
                              'join': joinSql,
                              'mods': mods,}
            qryStr = '\n'.join(
                [self.__wrapSQLline(l) for l in qryStr.split('\n')])

            
            if len(self._needDBiEscape) > 0:
                escaped = dict([(k, self.escape(v))
                                for k,v in self._needDBiEscape.iteritems()])
                doNotSubs = '|'.join(['\(%s\)s' % k
                                      for k in escaped.keys()])
                expr = u'%%(?!%s)' % doNotSubs
                qryStr = re.sub(expr, '%%', qryStr)
                return qryStr % escaped
            return qryStr
        except Exception, e:
            d = {'table': self._table,
                 'where': whereSql,
                 'limit': limitSql,
                 'order': orderBySql,
                 'group': groupBySql,
                 'having': havingSql,
                 'join': joinSql,
                 'mods': mods,}
            msg = ('Error compiling the SQL: %s\nPieces:%s\n%s\n\n'
                   'Traceback:\n%s' %
                   (e, qryStr, d, traceback.format_exc()))
            raise KdbomProgrammingError(msg)

    def __str__(self):
        return unicode(self).encode('utf-8')

    def escape(self, value, field=None):
        """Returns a SQL safe value. If `field` is set then its used
        to escape the value.

        Arguments:
        - `value`: The value to escape.
        - `field`: Instance of any BaseField subclass.
        """
        if isinstance(value, Expression):
            return str(value)
        if self._wildcard is not None and value == '%s':
            return self._wildcard
        if field is not None and isinstance(field, BaseField):
            return field.escape(value)
        if value is None:
            return 'NULL'
        return "'%s'" % str(value)

    def joinWith(self, tables):
        """Returns the JOIN clauses to join the main table with all
        the tables in `tables`.

        Arguments:
        - `tables`: List of tables to join.
        """
        if type(tables) is not set:
            tables = set([tables,])
        return self.__generateJoin(tables)

    def join(self, table, field1, field2=None, joinType='LEFT'):
        """Adds a required JOIN clause
        """
        if isinstance(table, BaseField) and field2 is None:
            field2 = field1
            field1 = table

        if isinstance(field1, BaseCondition):
            join = Join(joinType, table, field1)
            self._forceJoinTables[table] = join
            self._forceJoinTablesOrder.append(table)
            for field in field1.fields():
                if type(field) != AliasField and \
                       field.table != self._table and \
                       field.table not in self._forceJoinTablesOrder:
                    self._tablesNeeded.add(field.table)
        else:
            if isinstance(field1, BaseField):
                self._tablesNeeded.add(field1.table)
            if isinstance(field2, BaseField):
                self._tablesNeeded.add(field2.table)

            rel = Relationship(field1, field2)
            self._joinsNeeded.append(rel)
            self._joinTypes[rel] = joinType

    leftJoin = join

    def innerJoin(self, field1, field2):
        """Adds a required INNER JOIN clause
        """
        self.join(field1, field2, 'INNER')

    def crossJoin(self, field1, field2):
        """Adds a required CROSS JOIN clause
        """
        self.join(field1, field2, 'CROSS')

    def rightJoin(self, field1, field2):
        """Adds a required RIGHT JOIN clause
        """
        self.join(field1, field2, 'RIGHT')

    def leftOuterJoin(self, field1, field2):
        """Adds a required LEFT OUTER JOIN clause
        """
        self.join(field1, field2, 'LEFT OUTER')

    def rightOuterJoin(self, field1, field2):
        """Adds a required RIGHT OUTER JOIN clause
        """
        self.join(field1, field2, 'RIGHT OUTER')

    def selfJoin(self, field1, field2):
        """Adds a JOIN between the same table
        """
        self._lastAlias += 1
        alias = 'T%d' % self._lastAlias
        field2Obj = self.__getFieldObj(field2)[0]
        if field2Obj is None:
            raise KdbomRelationshipError, (
                "Join path not found using: '%s' and '%s'" % (field1, field2))
        self._aliases[alias] = field2Obj.table
        self._selfJoins.append((field1, field2Obj, alias))
        return alias

    def __and__(self, other):
        """Implements the operation Q1 & C1. Allows to join with AND a
        deep copy of this condition and `other` and sets the resulting
        CompoundCondition as the condition of a copy of this
        Query. Doesn't modify this instance of the Query, returns a
        copy with the new condition.

        Arguments:
        - `other`: A BaseCondition subclass instance.
        """
        if other is None:
            return self.copy()
        
        if not isinstance(other, (Expression, BaseCondition)):
            return NotImplemented

        _self = self.copy()
        _self._attributes['condition'] = _self.condition & other
        return _self

    def __or__(self, other):
        """Implements the operation Q1 | C1. Allows to join with OR a
        deep copy of this condition and `other` and sets the resulting
        CompoundCondition as the condition of a copy of this
        Query. Doesn't modify this instance of the Query, returns a
        copy with the new condition.

        Arguments:
        - `other`: A BaseCondition subclass instance.
        """
        if other is None:
            return self.copy()
        
        if not isinstance(other, (Expression, BaseCondition)):
            return NotImplemented

        _self = self.copy()
        _self._attributes['condition'] = _self.condition | other
        return _self

    def __iand__(self, other):
        """Implements the operation Q1 &= C1. Allows to join with AND a
        deep copy of this condition and `other` and sets the resulting
        CompoundCondition as the condition of this Query. This does
        modify this instance of the Query, returns a `self`.

        Arguments:
        - `other`: A BaseCondition subclass instance.
        """
        if not isinstance(other, (Expression, BaseCondition)):
            return NotImplemented

        self._attributes['condition'] = self.condition & other
        return self

    def __ior__(self, other):
        """Implements the operation Q1 |= C1. Allows to join with OR a
        deep copy of this condition and `other` and sets the resulting
        CompoundCondition as the condition of this Query. This does
        modify this instance of the Query, returns a `self`.

        Arguments:
        - `other`: A BaseCondition subclass instance.
        """
        if not isinstance(other, (Expression, BaseCondition)):
            return NotImplemented

        self._attributes['condition'] = self.condition | other
        return self

    def copy(self):
        """Returns a deep copy of this query.
        """
        _copy = self.__class__(self._table, self._avoidTables)
        _copy._queryType = self._queryType
        _copy._tablesNeeded = self._tablesNeeded
        _copy._joinsNeeded = self._joinsNeeded
        _copy._aliases = self._aliases
        _copy._lastAlias = self._lastAlias
        _copy._selfJoins = self._selfJoins
        _copy._needDBiEscape = self._needDBiEscape

        _copy._attributes = self._attributes.copy()
        return _copy

    def __deepcopy__(self, memo):
        """Returns a deep copy of this query.
        """
        return self.copy()

    def getCondition(self):
        """Returns the Condition instance of this query.
        """
        return self._attributes['condition']

    condition = property(getCondition)

    def getHavingCondition(self):
        """Returns the Condition for HAVING clause of this query.
        """
        return self._attributes['having']

    havingCondition = property(getHavingCondition)
        
    #
    # Access to kdbom2 db methods 
    #
    def execute(self, *args, **kwds):
        """Executes the query.
        Calls the corresponding method of the underlying database.
        """
        return self._db.execute(self, *args, **kwds)
        
    def executemany(self, *args,**kwds):
        """Execute a query with more than one params.
        Calls the corresponding method of the underlying database.
        """
        return self._db.executemany(self, *args, **kwds)

    def fetchgenerator(self,*args,**kwds):
        """Calls the corresponding method of the underlying database.
        """
        return self._db.fetchgenerator(self, *args, **kwds)

    def fetchall(self,*args,**kwds): 
        """Calls the corresponding method of the underlying database.
        """
        return self._db.fetchall(self,*args,**kwds)

    def fetchbatch(self, *args,**kwds):
        """Calls the corresponding method of the underlying database.
        """
        return self._db.fetchbatch(self,*args,**kwds)

    def fieldsAndRows(self,*args,**kwds):
        """Return a 2-tuple containing the fields descriptions (see
        PEP-0249) and the fetchall tuple of tuples.
        Calls the corresponding method of the underlying database.
        """
        return self._db.fieldsAndRows(self,*args,**kwds)

    def fieldNamesRows(self,*args,**kwds):
        """Return a 2-tuple containing the field names and
        the fetchall tuple of tuples. 
        Calls the corresponding method of the underlying database.
        """
        return self._db.fieldNamesRows(self,*args,**kwds)

    def fieldValueDict(self,*args,**kwds):
        """Return a list of dictionaries of the form:
        fieldName:fieldValue
        Calls the corresponding method of the underlying database.
        """
        return self._db.fieldValueDict(self,*args,**kwds)

    def modifier(self, modifier):
        """Adds a modifier for the query type. Driver subclasses
        should implement the method isValidModifier to check if
        valid modifier being requested.

        Reseting the query clears the set of active modifers.

        Arguments:
        - `modifier`: String - the modifier to add.
                      or
                      Other Sequence - the modifers to add.
        """
        if 'modifiers' not in self._attributes:
            self._attributes['modifiers'] = set([])
        if type(modifier) not in (types.ListType, types.GeneratorType,
                                  types.TupleType):
            modifier = [modifier,]
        for m in modifier:
            if self.isValidModifier(m):
                self._attributes['modifiers'].add(m)
            else:
                raise KdbomProgrammingError, ('Invalid query '
                                              'modifier: %s' % m)

    def isValidModifier(self, modifier):
        """Returns True if `modifier` is valid. This should be
        implemented on the drivers for compatibility issues.

        Arguments:
        - `modifiers`: String as a modifier to validate.
        """
        return True

    def columnNames(self):
        """Returns a list of the column names as they will look like
        after the SQL engine returns the result.
        """
        if 'fields' not in self._attributes:
            raise KdbomProgrammingError, 'Not a SELECT query.'

        if len(self._attributes['fields']) == 0:
            fields = self._table.fieldNames
            for t in self._tablesNeeded:
                fields.extend(t.fieldNames)
        else:
            fields = []
            for f, x in self._attributes['fields']:
                if isinstance(f, BaseField):
                    fields.append(f.name)
                else:
                    fields.append(str(f))
        return fields

    def getFields(self):
        """Returns the list of fields as they were set using any of
        the 'select' methods.
        """
        if 'fields' not in self._attributes:
            raise KdbomProgrammingError, 'Not a SELECT query.'

        return [x[0] for x in self._attributes['fields']]

    def setFields(self, fields):
        """Resets the fields and sets it to `fields`.

        Arguments:
        - `fields`: List of fields that replaces the current fields.
        """
        if fields:
            self.resetSelect()
            self.select(fields)

    fields = property(getFields, setFields)


class Expression(object):
    """Represents a SQL expression i.e.: NOW(), COUNT(`x`). Its
    string representation will be used unquoted on a Query object.
    """
    def __init__(self, expression, *args):
        """Initializes the object.

        Arguments:
        - `expression`: string containing the SQL expression.
        - `*args`: Field or list of fields appearing on the
        `expression`.
        """
        self.expression = expression
        self._args = list(args)

    def _getFields(self):
        fields = []
        for arg in self._args:
            if isinstance(arg, (BaseField, AliasField)):
                fields.append(arg)
            elif isinstance(arg, Expression):
                fields.extend(arg.fields)
        return fields

    fields = property(_getFields)

    def __str__(self):
        return str(self.expression)

    def __lt__(self, other):
        """Overrides the behavior of < operator.
        """
        return Condition(self, other, '<')

    def __le__(self, other):
        """Overrides the behavior of <= operator.
        """
        return Condition(self, other, '<=')

    def __eq__(self, other):
        """Overrides the behavior of == operator.
        """
        return Condition(self, other, '==')

    def __ne__(self, other):
        """Overrides the behavior of != and <> operators.
        """
        return Condition(self, other, '!=')

    def __gt__(self, other):
        """Overrides the behavior of > operator.
        """
        return Condition(self, other, '>')

    def __ge__(self, other):
        """Overrides the behavior of >= operator.
        """
        return Condition(self, other, '>=')
    
    def __add__(self, other):
        """Returns an object representation of self + other
        """
        return AddOperation(self, other)

    def __sub__(self, other):
        """Returns an object representation of self - other
        """
        return SubtractOperation(self, other)

    def __mul__(self, other):
        """Returns an object representation of self * other
        """
        return MultiplyOperation(self, other)

    def __div__(self, other):
        """Returns an object representation of self / other
        """
        return DivideOperation(self, other)

    __truediv__ = __div__

    def __radd__(self, other):
        """Returns an object representation of other + self
        """
        return AddOperation(other, self)

    def __rsub__(self, other):
        """Returns an object representation of other - self
        """
        return SubtractOperation(other, self)

    def __rmul__(self, other):
        """Returns an object representation of other * self
        """
        return MultiplyOperation(other, self)

    def __rdiv__(self, other):
        """Returns an object representation of other / self
        """
        return DivideOperation(other, self)

    __rtruediv__ = __rdiv__

    def __mod__(self, other):
        """Returns an object representation of self % other
        """
        return ModuloOperation(self, other)

    def __rmod__(self, other):
        """Returns an object representation of other % self
        """
        return ModuloOperation(other, self)

    def __pow__(self, other):
        """Returns an object representation of self**other
        """
        return PowerOperation(self, other)

    def __rpow__(self, other):
        """Returns an object representation of other**self
        """
        return PowerOperation(other, self)

    def __cmp__(self, other):
        return cmp(hash(str(self)), hash(other))

    def __and__(self, other):
        if other is None:
            return self.copy()

        if not isinstance(other, (Expression, BaseCondition)):
            raise KdbomProgrammingError, 'Invalid type'
        
        _self = self.copy()
        other = other.copy()
        return AndCondition(_self, other)

    def __or__(self, other):
        """Implements the operation C1 | C2. Allows to join with OR a
        deep copy of this condition and `other`. Doesn't modify
        `self`.

        Arguments:
        - `other`: Other BaseCondition subclass instance.
        """
        if other is None:
            return self.copy()

        if isinstance(other, _OperatorHelper):
            return NotImplemented
        
        if not isinstance(other, (Expression, BaseCondition)):
            raise KdbomProgrammingError, 'Invalid type'
        
        _self = self.copy()
        other = other.copy()
        return OrCondition(_self, other)

    def copy(self):
        if self.expression != '':
            args = [self.expression,] + self._args
        else:
            args = self._args
        _self = self.__class__(*args)
        return _self

    def NOT(self):
        """Returns a NotCondition of this one.
        """
        return NotCondition(self)

    def __nonzero__(self):
        return True

class Operation(Expression):
    """Represents a Field operation i.e. Field + Field, Field + number
    """
    def __init__(self, operator, operand1, operand2):
        """Initializes the object.

        Arguments:
        - `operator`: string to be used as operator, i.e. +, -, /, *
        - `*args`: list of Field and numbers
        """
        self.operand1 = operand1
        self.operand2 = operand2
        self.operator = operator
        Expression.__init__(self, '', operand1, operand2)

    def __str__(self):
        if isinstance(self.operand1, Operation):
            op1 = '(%s)' % self.operand1
        else:
            op1 = str(self.operand1)

        if isinstance(self.operand2, Operation):
            op2 = '(%s)' % self.operand2
        else:
            op2 = str(self.operand2)
        return '%s %s %s' % (op1, self.operator, op2)


class AddOperation(Operation):
    def __init__(self, operand1, operand2):
        timeField = None
        interval = None
        if isinstance(operand1, BaseField) and operand1.isTime:
            timeField = operand1
            if type(operand2) is str:
                interval = operand2
        elif isinstance(operand2, BaseField) and operand2.isTime:
            timeField = operand2
            if type(operand1) is str:
                interval = operand1

        if timeField and not interval:
            helpUrl = 'http://dev.mysql.com/doc/refman/5.0/en/date-and-time-functions.html#function_date-add'
            raise KdbomProgrammingError, ('Invalid value added to a '
                                          'date/datetime/timestamp '
                                          'type. Refer to %s' %
                                          helpUrl)
        
        self.isTime = timeField is not None
        if self.isTime:
            self.timeField = timeField
            self.interval = interval.strip().upper()
            if not self.interval.startswith('INTERVAL'):
                self.interval = 'INTERVAL %s' % self.interval
        
        Operation.__init__(self, '+', operand1, operand2)

    def __str__(self):
        if self.isTime:
            return "DATE_ADD(%s, %s)" % (self.timeField,
                                           self.interval)
        return Operation.__str__(self)


class SubtractOperation(Operation):
    def __init__(self, operand1, operand2):
        timeField = None
        interval = None
        if isinstance(operand1, BaseField) and operand1.isTime:
            timeField = operand1
            if type(operand2) is str:
                interval = operand2
        elif isinstance(operand2, BaseField) and operand2.isTime:
            timeField = operand2
            if type(operand1) is str:
                interval = operand1

        if timeField and not interval:
            helpUrl = 'http://dev.mysql.com/doc/refman/5.0/en/date-and-time-functions.html#function_date-add'
            raise KdbomProgrammingError, ('Invalid value subtracted to a '
                                          'date/datetime/timestamp '
                                          'type. Refer to %s' %
                                          helpUrl)
        
        self.isTime = timeField is not None
        if self.isTime:
            self.timeField = timeField
            self.interval = interval.strip().upper()
            if not self.interval.startswith('INTERVAL'):
                self.interval = 'INTERVAL %s' % self.interval
        
        Operation.__init__(self, '-', operand1, operand2)

    def __str__(self):
        if self.isTime:
            return "DATE_SUB(%s, %s)" % (self.timeField,
                                           self.interval)
        return Operation.__str__(self)   


class DivideOperation(Operation):
    def __init__(self, *args):
        Operation.__init__(self, '/', *args)


class MultiplyOperation(Operation):
    def __init__(self, *args):
        Operation.__init__(self, '*', *args)


class ModuloOperation(Operation):
    def __init__(self, *args):
        Operation.__init__(self, '%', *args)


class PowerOperation(Operation):
    def __init__(self, *args):
        Operation.__init__(self, '**', *args)

    def __str__(self):
        return 'POW(%s, %s)' % (self.operand1, self.operand2)


class DummyFunctionClass(Expression):
    _functionName = None
    def __init__(self, *args):
        Expression.__init__(self, '', *args)

    def __str__(self):
        fields = ','.join(map(str, self._args))
        return '%s(%s)' % (self._functionName, fields)

    @classmethod
    def makeClass(cls, name):
        return type(name, (DummyFunctionClass, object,),
                    {'_functionName': name})


class Alias(Expression):
    aliases = {}
    
    def __init__(self, expression, alias=None):
        if alias is None:
            alias = 'T%d' % len(Alias.aliases)
        
        self.expression = expression
        self.alias = alias
        Alias.aliases[alias] = expression

    def __str__(self):
        if isinstance(self.expression, BaseQuery):
            expression = '(%s)' % str(self.expression).strip()
        else:
            expression = self.expression
        return '%s AS %s' % (expression, self.alias)

    def __getFields(self):
        if isinstance(self.expression, BaseQuery):
            if len(self.expression._attributes['fields']) == 0:
                fields = self.expression._table.fields.values()
            else:
                fields = [x for x,y in \
                          self.expression._attributes['fields']]
            for field in fields:
                yield AliasField(field, self)
        elif isinstance(self.expression, BaseTable):
            for field in self.expression.fields:
                yield AliasField(getattr(self.expression, field),
                                 self)
        else:
            yield self.expression

    fields = property(__getFields)

    def __getattr__(self, name):
        if isinstance(self.expression, BaseQuery):
            if len(self.expression._attributes['fields']) == 0:
                fields = self.expression._table.fields.values()
            else:
                fields = [x for x,y in \
                          self.expression._attributes['fields']]
            for field in fields:
                if field.name == name:
                    return AliasField(field, self)
            raise AttributeError, name

        if name.startswith('__'):
            raise AttributeError, name
        
        field = getattr(self.expression, name)
        return AliasField(field, self)

    def getName(self):
        return self.alias

    name = property(getName)


class AliasField(Expression):
    def __init__(self, field, alias):
        self.field = field
        if isinstance(alias, Alias):
            self.alias = alias.alias
        else:
            self.alias = alias

    def __getattr__(self, name):
        return getattr(self.field, name)

    def __setattr__(self, name, value):
        if name in ('field', 'alias'):
            object.__setattr__(self, name, value)
        else:
            setattr(self.field, name, value)

    def __str__(self):
        return '%s.`%s`' % (self.alias, self.field.name)

    def getName(self):
        return self.alias

    name = property(getName)


class _OperatorHelper(object):
    """Allows to define new operators
    """
    
    def __init__(self):
        """Initializes the operator.
        """
        self.leftOperand = None
        
    def __ror__(self, other):
        self.leftOperand = other
        return self

    def __or__(self, other):
#        if isinstance(self.leftOperand, BaseField):
#            return AliasField(self.leftOperand, other)
        return Alias(self.leftOperand, other)

# Defines |AS| operator. Use it like this table1 |AS| 'T1' to generate
# this SQL `table1` AS T1 in your queries.
AS = _OperatorHelper()


class BaseTable(object):
    """Base class for Table class.
    """
    pass


class Join(object):
    """Represents a JOIN clause
    """
    def __init__(self, joinType='LEFT', joinTable=None, condition=None):
        """Initializes the instance

        Arguments:
        - `joinTable`: instance of Table, Alias or Expression suitable
        for a table reference.
        - `condition`: instance of BaseCondition
        """
        self.joinType = joinType
        self.table = joinTable
        self.condition = condition

    def __str__(self):
        return '%s JOIN %s ON %s' % (self.joinType, self.table,
                                     self.condition)
