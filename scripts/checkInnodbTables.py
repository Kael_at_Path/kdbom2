#!/usr/bin/env python
#
# Checks for errors in InnoDB tables.
#
__version__ = tuple([int(x) for x in
                     '$Revision: 1.2 $'.split()[1].split('.')])
__author__ = "Julio Menendez"

import sys
from optparse import OptionParser
import re

from kdbom2 import kdbom

INNODB_RE = re.compile('\s+ENGINE\s*=\s*InnoDB\s+')
AUTOINC_RE = re.compile('\s+AUTO_INCREMENT\s*=\s*(\d+)\s+')

def checkInnoDbTables(db):
    for tableName, table in db.tables.iteritems():
        if INNODB_RE.search(table.sqlDefinition) is None:
            print str(table), 'is not InnoDB... skipping'
            continue
        autoInc = AUTOINC_RE.search(table.sqlDefinition)
        if autoInc is not None:
            autoIncVal = int(autoInc.groups()[0])
            sql = ('ALTER TABLE `%s` AUTO_INCREMENT '
                   '= %d' % (tableName, autoIncVal + 1))
            try:
                db.execute(sql)
                print str(table), 'is ok'
            except Exception, ex:
                print str(table), 'failed with exception:\n%s' % ex
        else:
            print str(table), ('doesn\'t have a AUTO_INCREMENT value.'
                              'The script needs that info to check '
                              'the table... skipping')

def main(args):
    parser = OptionParser('usage: %prog [options] <dbname>')
    parser.add_option('-s', '--host', dest='host', help='MySQL host') 
    parser.add_option('-u', '--username', dest='username',
                      help='Username')
    parser.add_option('-p', '--password', dest='password',
                      help='Password')
    parser.add_option('-P', '--port', dest='port',
                      help='Port number for the connection')
    parser.add_option('-a', '--all-databases', dest='alldatabases',
                      help='Checks all the databases on the server',
                      action='store_true', default=False)
    (options, args) = parser.parse_args(args)

    if len(args) == 0 and not options.alldatabases:
        print >> sys.stderr, ('Usage error: a database name is '
                              'required or use -a to check all.')
        print >> sys.stderr, parser.format_help()
        sys.exit(1)

    connectKwargs = {}
    if options.host:
        if options.port:
            port = options.port
        else:
            port = 3306
        connectKwargs['serversPorts'] = ((options.host, port),)
    if options.username:
        connectKwargs['user'] = options.username
    if options.password:
        connectKwargs['password'] = options.password

    if options.alldatabases:
        mysqldb = kdbom.tryDBconnect(db='mysql', **connectKwargs)
        dbnames = [x[0] for x in mysqldb.fetchall('SHOW DATABASES')
                   if x[0] not in ('mysql', 'information_schema')]
    else:
        dbnames = args

    for dbname in dbnames:
        db = kdbom.tryDBconnect(db=dbname, **connectKwargs)
        checkInnoDbTables(db)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
