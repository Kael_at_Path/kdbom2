import re
import sys

import kdbom2.kdbom


def toMysql(obj, output=sys.stdout, includeData=False, exclude=None,
            onlyData=False, dataConditions=None):
    """Gets the SQL to create `obj` in MySQL

    Arguments:
    - `obj`: A KDBOM DB or Table object.
    - `includeData`: If True the SQL will include instructions to
    insert the data.
    - `exclude`: List of Field and/or Table objects to NOT include in
    the SQL
    """
    if exclude is None:
        exclude = []
    if dataConditions is None:
        dataConditions = dict()
    if isinstance(obj, kdbom2.kdbom.DB):
        print >> output, ('CREATE DATABASE IF NOT EXISTS '
                          '`%s`;' % obj.name)
        print >> output, 'USE `%s`;' % obj.name
        for table in obj.tables.itervalues():
            toMysql(table, output, includeData)
    elif isinstance(obj, kdbom2.kdbom.Table):
        if not onlyData:
            print >> output, 'CREATE TABLE IF NOT EXISTS `%s` (' % obj.name
            fieldsSql = []
            fields = []
            needsIndex = set()
            for fieldName in obj.fieldNames:
                field = obj.fields[fieldName]
                allowsNull = ''
                indexing = ''
                extra = ''
                if not field.nullAllowed:
                    allowsNull = 'NOT NULL'
                if field._indexing.find('PRI') > -1:
                    indexing = 'PRIMARY KEY'
                if field._indexing.find('MUL') > -1:
                    needsIndex.add(field)
                if field._indexing.find('UNI') > -1:
                    indexing = 'UNIQUE KEY'
                match = re.search('increment', field._extra, re.I)
                if match:
                    extra = 'AUTO_INCREMENT'
                default = ''
                if field.default is not None:
                    default = 'DEFAULT %s' % field.default
                fieldsSql.append(('`%(name)s` %(type)s %(null)s %(def)s '
                                  '%(extra)s %(idx)s'
                                  % {'name': field.name,
                                     'type': field._dataType,
                                     'idx': indexing,
                                     'extra': extra,
                                     'null': allowsNull,
                                     'def': default
                                     }))
                fields.append(field)
            print >> output, ', \n'.join(fieldsSql),
            if len(needsIndex) > 0:
                print >> output, ','
            else:
                print >> output, ''
            indexes = []
            for i, field in enumerate(needsIndex):
                indexes.append(('INDEX %s_idx%d (`%s`)' %
                                (obj.name, i, field.name)))
            if len(indexes) > 0:
                print >> output, ', \n'.join(indexes),
            relsSql = []
            for rel in obj.relationships.itervalues():
                if rel.child.table == obj:
                    relsSql.append(('CONSTRAINT `fk_%d` FOREIGN KEY (`%s`) '
                                    'REFERENCES `%s` (`%s`)' %
                                    (len(relsSql) + 1, rel.child.name,
                                     rel.parent.table.name,
                                     rel.parent.name)))
            if len(relsSql) > 0:
                print >> output, ','
                print >> output, ', \n'.join(relsSql)
            else:
                print >> output, ''
            print >> output, ');'

        if includeData or onlyData:
            escape = lambda x: [obj.fields[obj.fieldNames[i]].escape(v) \
                                for i, v in enumerate(x)]
            hasRows = False
            for i, row in enumerate(obj(selectExpr='*',
                                        iterator=True,
                                        **dataConditions)):
                if not hasRows:
                    hasRows = True
                if i%1000==0:
                    if i!=0:
                        print >> output, ';'
                    print >> output, ('INSERT INTO %s VALUES(%s)' %
                                      (obj, ','.join(escape(row)))),
                else:
                    print >> output, ',(%s)' % ','.join(escape(row)),
            if obj.count() > 0:
                print >> output, ';'
    else:
        raise Exception, ('Adapter only works with databases '
                          'and tables instances.')

def toSqlite(obj, output=sys.stdout, includeData=False, exclude=None,
             onlyData=False, dataConditions=None):
    """Gets the SQL to create `obj` in Sqlite

    Arguments:
    - `obj`: A KDBOM DB or Table object.
    - `includeData`: If True the SQL will include instructions to
    insert the data.
    - `exclude`: List of Field and/or Table objects to NOT include in
    the SQL
    """
    if exclude is None:
        exclude = []
    if dataConditions is None:
        dataConditions = dict()
    if isinstance(obj, kdbom2.kdbom.DB):
        for table in obj.tables.itervalues():
            if table in exclude:
                continue
            toSqlite(table, output, includeData, exclude, onlyData)
    elif isinstance(obj, kdbom2.kdbom.Table):
        if not onlyData:
            print >> output, ('CREATE TABLE IF NOT EXISTS '
                              '%s (' % obj.name)
            fieldsSql = []
            needsIndex = set()
            fields = []
            for fieldName in obj.fieldNames:
                field = obj.fields[fieldName]
                if field in exclude:
                    continue
                relSql = ''
                for rel in obj.relationships.itervalues():
                    if rel.child == field:
                        relSql = ('REFERENCES %s (%s)' % \
                                  (rel.parent.table.name,
                                   rel.parent.name))

                dataType = field._dataType
                if dataType.find('int') > -1:
                    dataType = 'INTEGER'
                if dataType.find('float') > -1:
                    dataType = 'float'
                if dataType.find('enum') > -1:
                    dataType = 'varchar(255)'
                allowsNull = ''
                if not field.nullAllowed:
                    allowsNull = 'NOT NULL'
                indexing = ''
                if field._indexing.find('PRI') > -1 and \
                   obj.primary_key == field:
                    indexing = 'PRIMARY KEY'
                elif field._indexing.find('UNI') > -1:
                    indexing = 'UNIQUE'
                elif field._indexing.find('MUL') > -1:
                    needsIndex.add(field.name)
                extra = ''
                match = re.search('increment', field._extra, re.I)
                if match:
                    extra = 'AUTOINCREMENT'
                default = ''
                if dataType.find('imestamp') > -1:
                    default = 'DEFAULT CURRENT_TIMESTAMP'
                elif field.default is not None:
                    default = 'DEFAULT %s' % field.default
                nameClean = field.name.replace('`', '')
                nameClean = nameClean.replace('(', '').replace(')', '')
                fieldsSql.append(('\'%(name)s\' %(type)s %(idx)s %(extra)s '
                                  '%(null)s %(def)s %(rels)s'
                                  % {'name': nameClean,
                                     'type': dataType,
                                     'idx': indexing,
                                     'extra': extra,
                                     'null': allowsNull,
                                     'rels': relSql,
                                     'def': default
                                     }))
                fields.append(field)
            print >> output, ', \n'.join(fieldsSql)
            print >> output, ');'
            for i, fieldName in enumerate(needsIndex):
                print >> output, ('CREATE INDEX IF NOT EXISTS %s '
                                  'ON %s(%s);' %
                                  ('%s_idx%d' % (obj.name, i), obj.name,
                                   fieldName))
        if includeData or onlyData:
            print >> output, 'BEGIN TRANSACTION;'
            def escape(values, fields):
                results = []
                for i, v in enumerate(values):
                    v = fields[i].escape(v)
                    if type(v) is str:
                        v = v.replace("\\'", "''")
                    results.append(v)
                return results
            f = filter(lambda x: x not in exclude,
                       obj.fields.itervalues())
            fns = ','.join(["`%s`" % x.name for x in f])
            
            for row in obj(selectExpr=fns, iterator=True,
                           **dataConditions):
                print >> output, ('INSERT INTO %s(%s) VALUES(%s);' % \
                                  (obj.name, fns,
                                   ','.join(escape(row, f))))
            print >> output, 'COMMIT TRANSACTION;'
    else:
        raise Exception, ('Adapter only works with databases '
                          'and tables instances.')
