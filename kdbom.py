# Kael's Database Object Model
# Classes and tools for MySQL databases, and objects that
# use a database for persistence.
# The database to be used is determined by which driver is set. By
# default MySQL driver is in use. To change it use the
# kdbom2.setDriver method. A driver contains all the necessary methods
# to interact with the database system they represent.
#
__version__ = tuple([int(x) for x in
                     '$Revision: 1.95 $'.split()[1].split('.')])
__author__ = "Kael Fischer and Julio Carlos Menendez"

import functools
import inspect
import logging
import random
import re
import types

import kdbom2
from exceptions import *
from kdbom2.util import getIterable
from kdbom2.drivers import Relationship
from kdbom2.query import *

if kdbom2.driver is None:
    raise Exception, ("Before importing this module you need to "
                      "define the driver to use. For example:\n\r"
                      "import kdbom2\n\r"
                      "import kdbom2.drivers.mysql\n\r"
                      "kdbom2.driver = kdbom2.drivers.mysql")

DEFAULTLOG='/dev/null'

Query = kdbom2.driver.Query
Cursor = kdbom2.driver.Cursor

IgnoreTables = []


class DB(kdbom2.driver.DB):
    """DB subclass that implements the MySQLdb connection to a
    database.
    """
    
    def __init__(self, *args, **kwargs):
        if 'debug' in kwargs and not (kwargs['debug']==False):
            if type(kwargs['debug']) in types.StringTypes:
                self.activateDebugging(kwargs['debug'])
            else:
                self.activateDebugging(DEFAULTLOG)
        else:
            self._debug = False
            
        if 'ignoreTables' in kwargs:
            self._ignoreTables = kwargs['ignoreTables']
        else:
            self._ignoreTables = IgnoreTables

        if 'getTables' in kwargs:
            self._lazyLoad = kwargs['getTables'] is False
        else:
            self._lazyLoad = False

        if 'autocommit' in kwargs:
            self.autocommit = kwargs['autocommit']
        else:
            self.autocommit = True

        self.tableClass = Table
        self.fieldClass = Field
        self.recordClass = Record
        self.relationClass = Relationship

        kdbom2.driver.DB.__init__(self, *args, **kwargs)
        self.connect()
        if 'reuseDBconnection' in kwargs:
            self.__loadTables()

        self.joinAvoidTables = []

    def activateDebugging(self,logfile,trim=500):
        """Log all SQL queries and execute params.
        Each is trimmed at 'trim' characters.
        """

        self._debug = logfile

        hdlr = logging.FileHandler(self._debug)
        formatter = logging.Formatter(
            '%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)

        self._logger = logging.getLogger('kdbom')
        self._logger.addHandler(hdlr)
        self._logger.setLevel(logging.DEBUG) 

    def deactivateDebugging(self):
        """turn off debug log.
        """
        self._debug=False

    def refresh(self):
        """Reloads the tables.
        """
        self.__loadTables()

    def cursor(self):
        """return Cursor object linked to this DB
        """
        return Cursor(self)

    def execute(self, sql, parameters=None):
        """Executes the query
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))

        cursor = Cursor(self)
        return cursor.execute(sql, parameters)
        
    def executemany(self, query, params=None, disableFKcheck=False,
                    chunkSize=1000):
        """Execute a query with more than one params.
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))

        cursor = Cursor(self)
        return cursor.executemany(query, params, chunkSize,
                                  disableFKcheck=disableFKcheck)


    def fetchgenerator(self, sql, parameters=None):
        """Returns a generator with the results of executing `sql`.
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))

        cursor = Cursor(self)
        for r in cursor.fetchgenerator(sql, parameters):
            yield r

    def fetchall(self, sql, parameters=None):
        """Returns all the results of executing `sql`.
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))
        cursor = Cursor(self)
        return cursor.fetchall(sql, parameters)

    def fetchbatch(self, sql, parameters=None, batchSize=1000,
                   batch=None):
        """Returns the results in the specified batch start and size.
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))
        cursor = Cursor(self)
        return cursor.fetchbatch(sql, parameters, batchSize, batch)

    def fieldsAndRows(self, sql, parameters=None):
        """Return a 2-tuple containing the fields descriptions (see
        PEP-0249) and the fetchall tuple of tuples.
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))
        cursor = Cursor(self)
        rows = cursor.fetchall(sql, parameters)
        return (cursor.description, rows)

    def fieldNamesRows(self, sql, parameters=None):
        """Return a 2-tuple containing the field names and
        the fetchall tuple of tuples. 
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))
        desc, rows = self.fieldsAndRows(sql, parameters)
        return (map(lambda x: x[0], desc), rows)

    def fieldValueDict(self, sql, parameters=None):
        """Return a list of dictionaries of the form:
        fieldName:fieldValue
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))
        names, rows = self.fieldNamesRows(sql, parameters)
        names = [names] * len(rows)
        return map(dict, map(zip, names, rows))

    def uniqueKeyFieldDict(self, keyFieldName, sql, parameters=None):
        """Return a dictionary of the matching rows (each row is
        represented as a fieldName:fieldValue dictionary.  The keys
        of the row dictionary are the values of the keyField, specified
        by name as the 1st parameter.
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))
        getKeyValue = lambda x: x[keyFieldName]
        listOfDicts = self.fieldValueDict(sql, parameters)
        return dict(zip(map(getKeyValue, listOfDicts), listOfDicts))

    def generalKeyFieldDict(self, keyFieldName, sql, parameters=None):
        """Return a dictionary of _lists_ of matching rows, expressed as
        dictionaries, keys to the KeyField value.  Keys need not be unique
        since lists of matching records are the dictionary's values. 
        """
        if self._debug:
            self._logger.debug(','.join(
                [str(x)[:500] for x in (sql,parameters)]))
        rv = {}

        getKeyValue = lambda x: x[keyFieldName]
        listOfDicts = self.fieldValueDict(sql,parameters)

        for row in listOfDicts:
            k = getKeyValue(row)
            if k not in rv:
                rv[k] = [row]
            else:
                rv[k].append(row)

        return rv
    
    def __getitem__(self, table):
        """Allows to use the DB as a dictionary of Tables objects.
        
        Arguments:
        - `table`: The name of the table.
        """
        try:
            return self.tables[table]
        except KeyError:
            try:
                self.tables[table] = Table(db=self, name=table)
                return self.tables[table]
            except:
                raise KeyError

    def __getattr__(self, table):
        """Allows to get a table as an attribute of the DB.
        
        Arguments:
        - `table`: The name of the table.
        """
        if self.tables.has_key(table):
            return self[table]
        elif self._lazyLoad:
            try:
                return self[table]
            except KeyError:
                raise AttributeError
        raise AttributeError

    def __repr__(self):
        return "Tables in %s:\n%s" % (self._name,
                                      ', '.join(self.tables.keys()))

    def cursor(self):
        """Returns a kdbom2.Cursor instance.
        """
        return Cursor(self)

    def __hash__(self):
        host = hash(self.host) >> 2
        port = hash(self.port) >> 1
        db = hash(self.name)
        return db^port^host
        

Db = DB

class Table(kdbom2.driver.Table):
    """Table subclass representing a Table in a MySQL database.
    """
    
    def __init__(self, db, name):
        """Initializes a table instance
        
        Arguments:
        - `db`: The DB instance where this table is stored.
        - `name`: The name of the table.
        """
        self.db = db
        self._name = name
        self.relationships = {}
        
        self.__loadFieldsAndRelationships()

    def __getName(self):
        """Returns the name of the table.
        """
        return self._name
        
    name = property(__getName)

    def __getattr__(self, field):
        """Allows to access the fields of the table as attributes.
        
        Arguments:
        - `field`: The name of the field
        """
        try:
            return self.fields[field]
        except KeyError:
            raise AttributeError

    def __setattr__(self, name, value):
        if 'fields' in self.__dict__ and name in self.fields:
            raise KdbomProgrammingError, ('Cannot assign values to a '
                                          'Field object')
        object.__setattr__(self, name, value)

    def __getitem__(self, pkValue):
        """Returns the record with primary key `pkValue`.
        Eg. rec = Table1[1]
        
        Arguments:
        - `pkValue`: primary key
        """
        return Record(table=self, PKvalue=pkValue)

    def pathTo(self, table, dontVisit=None):
        """Returns the path to `table`.
        
        Arguments:
        - `table`:
        """
        if table == self:
            return None

        if table in self.relationships.keys():
            return [self.relationships[table]]
        
        dontVisit = set(getIterable(dontVisit))
        dontVisit.add(self)
        relatives = set(self.relationships.keys())
        relatives.difference_update(dontVisit)

        path = None
        for rel in relatives:
            if rel == table:
                return [self.relationships[table]]
            else:
                relPath = rel.pathTo(table, dontVisit)
                if relPath is None:
                    continue
                if path is not None:
                    path = min(path, relPath)
                else:
                    path = [self.relationships[rel]] + relPath
        return path

    def insert(self, data=None, ignore=False, **values):
        """Inserts a record into the table
        returns the id for the row.
        """
        if data is None:
            data = values
        else:
            data.update(values)

        qry = Query(self)    
        pairs = {}
        values = []
        for k,v in data.iteritems():
            if type(k) is str:
                k = self.fields[k]
            values.append(v)
            pairs[k] = ('%s', len(values)-1)
        qry.insert(pairs, ignore)
        cursor = Cursor(self.db)
        cursor.execute(str(qry), values)
        return cursor.lastrowid

    def insertMany(self, fieldNames, rows, callback=lambda x: x,
                   disableFKcheck=False, chunkSize=10000,
                   ignore=False, partialCommits=False):
        """Inserts many rows into the table. `callback` will be called
        on rows before insertion.
        """
        qry = Query(self)
        qry.insert(dict(
            [(f, ('%s', i)) for i,f in enumerate(fieldNames)]),
                   ignore=ignore)

        def processBatch(batch):
            cursor.executemany(str(qry), batch, chunkSize, 
                               partialCommits,
                               disableFKcheck=disableFKcheck)
        
        cursor = Cursor(self.db)
        batch=[]
        for r in rows:
            batch.append(callback(r))
            if len(batch) == chunkSize:
                processBatch(batch)
                batch=[]
        processBatch(batch)
        


    def delete(self, criteria=None, literalCriteria=None, **values):
        """Deletes the records matching the criteria specified.
        """
        if criteria is None:
            criteria = {}
        
        qry = Query(self)
        qry.delete()
        qry.where(criteria, literalCriteria, **values)
        cursor = Cursor(self.db)
        cursor.execute(str(qry))

    def update(self, data=None, criteria=None, literalCriteria=None):
        """Updates the records matching the criteria with the new data
        
        Arguments:
        - `data`:
        - `criteria`:
        - `literalCriteria`:
        """
        if data is None:
            data = {}
        if criteria is None:
            criteria = {}
        
        qry = Query(self)
        qry.update(data)
        qry.where(criteria, literalCriteria)
        cursor = Cursor(self.db)
        cursor.execute(qry)

    def selectQuery(self,funcs=None,fields=None,
               criteria={}, literalCriteria=None,**values):
        """Returns a select Query object
        
        Arguments:
        - `funcs`: e.g. DISTINCT, DISTINCTROW (see Query)
        - `fields`:  field name(s) of object(s) (single,
                     seq, or iterator) [none expands to
                     self.fieldNames]
        - `criteria`: together with values a dictionary of
                      where equality criteria
        - `literalCriteria`: literal additions to the WHERE clause
        - `**values`: Field=value pairs to use in the condition of the
        query.
        """
        if fields==None:
            fields=self.fieldNames

        qry=Query(self)
        qry.select(funcs,fields)
        qry.where(criteria=criteria,literalCriteria=literalCriteria,
                  **values)
        
        return qry

    def query(self, dontVisit=[]):
        """Return query based on this table
        """
        if not dontVisit:
            dontVisit = self.db.joinAvoidTables
        return Query(self, dontVisit)

    def __iter__(self, criteria=None, **values):
        """Returns an iterator over all the rows. If criteria or
        **values are specified then are used to generate WHERE
        clause.
        """
        if criteria is None:
            criteria = {}
        
        return self(iterator=True, criteria=criteria, selectExpr='*',
                    returnRecords=True, **values)

    def records(self, iterator=False):
        """Returns all the records in the table
        """
        if iterator:
            return self.__iter__()
        else:
            return self(selectExpr='*', returnRecords=True)
            
    def __call__(self, criteria=None, selectExpr=None, iterator=False,
                 expectOneRecord=False, returnLastRecord=False,
                 returnFirstRecord=False, batchSize=1000, batch=None,
                 whereLiteral=None, returnRecords=False, **values):
        """Returns Record instances or rows as lists.

        Arguments:
        - `criteria`: Dictionary of field:value. Will be
        updated with the dictionary `**values`.
        - `selectExpr`: Any valid SQL SELECT expression.
        - `iterator`: If True a generator is returned with the result
        rows.
        - `expectOneRecord`: If True and the results contains more
        than one row an exception is raised.
        - `returnLastRecord`: If True only the last row of the result
        is returned.
        - `returnFirstRecord`: If True only the first row of the
        result is returned.
        - `batchSize`: Size of the batch of results to return.
        - `batch`: If not None then must be the number of the batch
        page to return.
        - `whereLiteral`: String with conditions to add to the query.
        - `returnRecords`: If True returns Record instances instead of
        rows as lists.
        - `**values`: Field=value pairs to use in the condition of the
        query.
        """
        if criteria is None:
            criteria = {}
        
        if selectExpr is None:
            funcs = ['LITERAL']
            fields = ['*']
        else:
            funcs = ['LITERAL']
            fields = [selectExpr]

        if not isinstance(criteria, BaseCondition):
            criteria_ = {}
            criteria.update(values)
            for k,v in criteria.iteritems():
                if isinstance(v, KSqlObject):
                    v = int(v)
                elif isinstance(v, Record):
                    v = v.PKvalue
                criteria_[k] = v
        else:
            criteria_ = criteria
        
        qry = Query(self)
        qry.select(funcs, fields)
        qry.where(criteria_, literalCriteria=whereLiteral)
        
        if returnLastRecord:
            qry.orderBy(self.Timestamp, True)
            qry.limit(1)

        if returnFirstRecord:
            qry.orderBy(self.Timestamp)
            qry.limit(1)

        cursor = Cursor(self.db)

        rows = cursor.fetchall(str(qry))

        if expectOneRecord and len(rows) > 1:
            raise KdbomLookupError, "More than one record returned"

        if (not expectOneRecord and not returnRecords
            and not returnLastRecord and not returnFirstRecord):
            if iterator:
                return iter(rows)
            else:
                return rows
        else:
            if iterator:
                return (Record(table=self,PKvalue=r[0]) for r in rows)
            else:
                return [Record(table=self,PKvalue=r[0]) for r in rows]

    def PKvalues(self, criteria=None, whereLiteral=None, **values):
        """Returns a list of primary key values.
        criteria can be specified or field=value pairs can be
        specified, per Table.__call__.  If none of thos are
        specified all primary key values will be selected.
        """
        if criteria is None:
            criteria = {}
        
        criteria.update(values)
        if len(criteria) == 0 and whereLiteral is None:
            return self._pkField.values
        rows = self(criteria=criteria, whereLiteral=whereLiteral)
        return [x[0] for x in rows]

    def getValues(self, PKvalue):
        """Returns the row with `PKvalue` as primary key.
        """
        criteria = {self._pkField: PKvalue}
        return self(criteria=criteria, selectExpr="*")[0]

    GetValues = getValues

    def count(self, criteria=None, whereLiteral='', expr='*',
              **values):
        """Returns the number of rows.

        Arguments:
        - `criteria`: Dictionary of field:value. Will be
        updated with the dictionary `**values`.
        - `whereLiteral`: String with conditions to add to the query.
        - `expr`: Any valid SQL SELECT expression.
        """
        if criteria is None:
            criteria = {}
        
        qry = Query(self)
        qry.select(['COUNT', 'LITERAL'], expr)
        qry.where(criteria, whereLiteral, **values)
        cursor = Cursor(self.db)
        row = cursor.fetchone(str(qry))
        return row[0]

    def valueIdDict(self, field, **values):
        """Returns a dictionary of field_value:id
        key uniqueness is not checked
        """
        qry=self.query()
        qry.select(field)
        qry.selectID()
        qry.where(values)
        return dict(self.db.fetchall(str(qry)))

    def idValueDict(self, field, **values):
        """returns a dictionary of { ID:field_value,...}
        """
        qry=self.query()
        qry.selectID()
        qry.select(field)
        qry.where(values)
        return dict(self.db.fetchall(str(qry)))

    def ksqlObjClass(self, strField=None, tagTable=None,
                     tagLinkTable=None):
        """Returns a KSqlObject class from this table.
        """
        class KSqlObjFromTable(KSqlObject):
            _table = self
            _tagTable = tagTable
            _tagLinkTable = tagLinkTable

        return KSqlObjFromTable

    def parentTables(self):
        """Returns the list of parent tables of this table based on
        the calculated relationships.
        """
        tables = []
        for rel in self.relationships.itervalues():
            if self == rel.child.table:
                tables.append(rel.parent.table)
        return tables

    def childTables(self):
        """Returns the list of child tables of this table based on
        the calculated relationships.
        """
        tables = []
        for rel in self.relationships.itervalues():
            if self == rel.parent.table:
                tables.append(rel.child.table)
        return tables

    def __allRelatives__(self, appendTo=None, dontVisit=None,
                         childLimit=4):
        """Returns all the recursively relatives to this table based
        on the calculated relationships.
        """
        if appendTo is None:
            appendTo = []
            
        if dontVisit is None:
            dontVisit = []
        else:
            dontVisit = copy(dontVisit)

        dontVisit.append(self)

        kids = self.childTables()
        family = kids + self.parentTables()

        if len(family):
            for member in family:
                if member not in dontVisit and len(kids) <= childLimit:
                    appendTo.append(member)
                    member.__allRelatives__(appendTo, dontVisit)
            return appendTo
        return None

    def F1tables(self):
        """Returns the direct relatives to this table.
        """
        return self.parentTables() + self.childTables()        

    def __allParents__(self, appendTo=None):
        """Returns all the recursively parents to this table.
        """
        if appendTo is None:
            appendTo = []
        parents = self.parentTables()
        if len(parents):
            for parent in parents:
                appendTo.append(parent)
                parent.__allParents__(appendTo)
            return appendTo
        return None

    def idValueTuples(self, field, orderBy=None):
        qry = Query(self)
        qry.select([], [self.primary_key, field])
        if orderBy is not None and orderBy != '':
            qry.orderBy(orderBy)
        return self.db.fetchall(str(qry))

    def __getprimarykey__(self):
        """Returns the primary key field.
        """
        return self.primary_key

    def autoIncrementField(self):
        """Returns the auto incremental field.
        """
        return self._autoIncFied

    def joinClause(self, relative):
        """Returns the JOIN clause of this table and `relative` if
        possible.
        """
        if relative in self.relationships:
            return self.relationships[relative].join(relative)
        raise (Relationship_Error, "%s not an immediate relative of %s"
               % (relative, self))

    def joinPath(self, relative, dontVisit=None, joinOrder=None):
        """Returns the JOIN clauses needed to join this table with
        `relative`.

        Arguments:
        - `relative`: Table instance to join to.
        - `dontVisit`: list of Table instances to avoid when
        calculating the path.
        - `joinOrder`: No longer used.
        """
        qry = Query(self, dontVisit)
        return qry.joinWith(relative)

    def generator(self, *args, **kwargs):
        """Returns a generator of all the rows of this table.
        """
        kwargs['iterator'] = True
        return self(*args, **kwargs)

    def parentPath(self, relative, appendToPath=None):
        """Returns a list of tables representing the path from this
        table to his parent `relative`.
        """
        if appendToPath is None:
            appendToPath = []
        firstParents = self.parentTables()
        if firstParents:
            if relative in firstParents:
                return appendToPath
            for parent in firstParents:
                ancestors = parent.__allParents__()
                if ancestors:
                    if relative in ancestors:
                        appendToPath.append(parent)
                        parent.parentPath(relative,appendToPath)

        return appendToPath
        

    def addRelationship(self,myField,otherField):
        """Add link to another table based on
        myField and otherField (in related table).
        """
##         self.relationships[otherField.table]=self.db.relationClass(
##             parent=otherField,child=myField)
##         otherField.table.relationships[self]=self.db.relationClass(
##             parent=myField,child=otherField)
        rel = self.db.relationClass(parent=otherField, child=myField)
        myField.table.relationships[otherField.table] = rel
        otherField.table.relationships[myField.table] = rel
#        otherField.table.relationships[otherField.table]=self.db.relationClass(
#            parent=otherField,child=myField)
#        self.relationships[self]=self.db.relationClass(
#            parent=myField,child=otherField)



    ##def linkTable(self,other):

    def __hash__(self):
        db = hash(self.db) >> 1
        table = hash(self.name)
        return table^db

    def __eq__(self, other):
        return hash(self) == hash(other)


class Field(kdbom2.driver.Field):
    """SQL field class
    """
    
    def __init__(self, table, name, dataType, nullAllowed, indexing,
                 extra, default=''):
        """Initializes a field instance
        
        Arguments:
        - `table`: The Table instance this field belongs to.
        - `name`: The name of the field.
        - `dataType`: The datatype of the field.
        - `nullAllowed`: Does the field can be null?
        - `indexing`: The indexing type of the field.
        - `extra`: Any extra attribute of the field.
        """
        self.table = table
        self.db = table.db
        self._name = name
        self._dataType = dataType
        self._indexing = indexing
        self._extra = extra

        self.nullAllowed = nullAllowed
        self.default = default
        self._default = default
        if self.default == 'CURRENT_TIMESTAMP':
            self.default = None
        (self.isNumeric, self.isFloat,
         self.isString, self.isTime) = (False,)*4

        if re.search('int', dataType, re.IGNORECASE):
            self.isNumeric = True
        elif re.search('double', dataType, re.IGNORECASE) or \
             re.search('float', dataType, re.IGNORECASE) or \
             re.search('fixed', dataType, re.IGNORECASE) or \
             re.search('dec', dataType, re.IGNORECASE) or \
             re.search('real', dataType, re.IGNORECASE) or \
             re.search('numeric', dataType, re.IGNORECASE) or \
             dataType in ('bit', 'bool', 'boolean'):
            self.isNumeric = True
            self.isFloat = True
        elif re.search('date', dataType, re.IGNORECASE) or \
             re.search('time', dataType, re.IGNORECASE) or \
             re.search('year', dataType, re.IGNORECASE):
            self.isTime = True
        elif re.search('char', dataType, re.IGNORECASE) or \
             re.search('text', dataType, re.IGNORECASE) or \
             re.search('blob', dataType, re.IGNORECASE) or \
             re.search('^enum', dataType, re.IGNORECASE):
            self.isString = True

    def __getName(self):
        return self._name

    name = property(__getName)

    def __aggregateFunction(self, operation, groupBy=None,
                            orderBy=None, dontVisit=None, criteria=None,
                            literalCriteria=None, **values):
        """Wrapper for SQL aggregate functions.
        
        Arguments:
        - `operation`: A valid SQL aggregate function.
        - `groupBy`: One or a list of fields to group by.
        - `orderBy`: One or a list of fields to order by.
        """
        if criteria is None:
            criteria = {}
        
        validOperations = ('MIN', 'MAX', 'SUM', 'AVG', 'STD', 'COUNT',
                           'VARIANCE')
        operation = operation.upper()
        if operation not in validOperations:
            raise ArgumentError, ('Aggregate operation must be SQL '
                                  'valid.')
        qry = Query(self.table, dontVisit)
        qry.where(criteria, literalCriteria, **values)
        qry.select(groupBy)
        qry.groupBy(groupBy)
        qry.orderBy(orderBy)
        if operation == 'COUNT':
            qry.select(['COUNT', 'DISTINCT'], self)
        else:
            qry.select(operation, self)
        cursor = Cursor(self.db)
        sql = str(qry)
        
        if groupBy is None:
            res = cursor.fetchone(sql)[0]
        else:
            res = cursor.fetchall(sql)
        return res

    def aggregateFunction(self, operation, where=None):
        return self.__aggregateFunction(operation, literalCriteria=where)

    def min(self, *args, **kwargs):
        """
        """
        return self.__aggregateFunction('min', *args, **kwargs)

    def max(self, *args, **kwargs):
        """
        """
        return self.__aggregateFunction('max', *args, **kwargs)

    def sum(self, *args, **kwargs):
        """
        """
        return self.__aggregateFunction('sum', *args, **kwargs)

    def avg(self, *args, **kwargs):
        """
        """
        return self.__aggregateFunction('avg', *args, **kwargs)

    def std(self, *args, **kwargs):
        """
        """
        return self.__aggregateFunction('std', *args, **kwargs)

    def count(self, *args, **kwargs):
        """
        """
        return self.__aggregateFunction('count', *args, **kwargs)

    def variance(self, *args, **kwargs):
        """
        """
        return self.__aggregateFunction('variance', *args, **kwargs)

    def countX(self, value):
        """Returns the number of occurrences of `value` in this field.
        """
        return self.count(criteria={self: value})

    def convertForDB(self, value):
        """Returns a SQL valid value from `value`. Value needs to be
        a record of this field.
        
        Arguments:
        - `value`: Value to convert.
        """
        if self.isString:
            try:
                return str(value)
            except UnicodeEncodeError:
                return unicode(value)
                
        if self.isNumeric and not self.isFloat:
            return int(value)
        if self.isFloat:
            return float(value)
        return value

    def distinctValues(self, **kwargs):
        """Returns a list of all the distinct values of this field.
        """
        qry = Query(self.table)
        qry.select('DISTINCT', self)
        qry.where(**kwargs)

        return [x[0] for x in self.db.fetchall(str(qry))]

    def valueList(self, **kwargs):
        """Returns a list of all the values of this field.
        """
        qry = Query(self.table)
        qry.select([], self)
        qry.where(**kwargs)

        return [x[0] for x in self.db.fetchall(str(qry))]

    values = property(valueList)

    distinct_values = property(distinctValues)

    def isUnique(self):
        """Returns True if there not repeated values in this field.
        """
        return len(self.distinct_values) == len(self.values)

    def search(self, criteria, operator='='):
        """Returns a list of all the records that match `criteria`.
        """
        qry = Query(self.table)
        qry.select([], self.table.primary_key)
        qry.where(criteria={self: criteria}, operator=operator)

        return map(lambda x: Record(table=self.table, PKvalue=x[0]),
                   self.db.fetchall(str(qry)))

    def subSearch(self, substring):
        """Returns the records resulting of searching `substring` in
        this field using LIKE.
        """
        qry = Query(self.table)
        qry.select([], self.table.primary_key)
        qry.whereLike(self, substring)

        return map(lambda x: Record(table=self.table, PKvalue=x[0]),
                   self.db.fetchall(str(qry)))

    def __hash__(self):
        """Returns a UID for this instance.
        """
        table = hash(self.table) >> 1
        field = hash(self._name)
        return field^table

    def __lt__(self, other):
        """Overrides the behavior of < operator.
        """
        return Condition(self, other, '<')

    def __le__(self, other):
        """Overrides the behavior of <= operator.
        """
        return Condition(self, other, '<=')

    def __eq__(self, other):
        """Overrides the behavior of == operator.
        """
        if other is None:
            return IsNullCondition(self)
        return Condition(self, other, '==')

    def __ne__(self, other):
        """Overrides the behavior of != and <> operators.
        """
        if other is None:
            return IsNotNullCondition(self)
        return Condition(self, other, '!=')

    def __gt__(self, other):
        """Overrides the behavior of > operator.
        """
        return Condition(self, other, '>')

    def __ge__(self, other):
        """Overrides the behavior of >= operator.
        """
        return Condition(self, other, '>=')

    def __cmp__(self, other):
        return cmp(hash(self), hash(other))

    def like(self, value):
        """Shortcut method to 'field LIKE value' condition.
        """
        return LikeCondition(self, value)

    def notLike(self, value):
        """Shortcut method to 'field NOT LIKE value' condition.
        """
        return NotLikeCondition(self, value)

    def isNull(self):
        """Shortcut method to 'field IS NULL' condition.
        """
        return IsNullCondition(self)

    def isNotNull(self):
        """Shortcut method to 'field IS NOT NULL' condition.
        """
        return IsNotNullCondition(self)

    def __add__(self, other):
        """Returns an object representation of self + other
        """
        return AddOperation(self, other)

    def __sub__(self, other):
        """Returns an object representation of self - other
        """
        return SubtractOperation(self, other)

    def __mul__(self, other):
        """Returns an object representation of self * other
        """
        return MultiplyOperation(self, other)

    def __div__(self, other):
        """Returns an object representation of self / other
        """
        return DivideOperation(self, other)

    __truediv__ = __div__

    def __radd__(self, other):
        """Returns an object representation of other + self
        """
        return AddOperation(other, self)

    def __rsub__(self, other):
        """Returns an object representation of other - self
        """
        return SubtractOperation(other, self)

    def __rmul__(self, other):
        """Returns an object representation of other * self
        """
        return MultiplyOperation(other, self)

    def __rdiv__(self, other):
        """Returns an object representation of other / self
        """
        return DivideOperation(other, self)

    __rtruediv__ = __rdiv__

    def __mod__(self, other):
        """Returns an object representation of self % other
        """
        return ModuloOperation(self, other)

    def __rmod__(self, other):
        """Returns an object representation of other % self
        """
        return ModuloOperation(other, self)

    def __pow__(self, other):
        """Returns an object representation of self**other
        """
        return PowerOperation(self, other)

    def __rpow__(self, other):
        """Returns an object representation of other**self
        """
        return PowerOperation(other, self)    

    

class Record(object):
    """SQL record class
    """
    
    def __init__(self, table=None, PKvalue=None, query=None,
                 params=None):
        """Requires a table object and either a primary key value or a
        SQL query and params. The query must return only a primary key
        from this table.
        """
        object.__setattr__(self, 'data', {})
        self.table = table
        self.db = table.db
        if PKvalue is not None:
            self.PKvalue = PKvalue
        else:
            cursor = Cursor(self.db)
            row = cursor.fetchone(query, params)
            if type(row) is None:
                self.PKvalue = None
            else:
                self.PKvalue = row[0]
        self.refresh()

    def refresh(self):
        values = self.table.getValues(self.PKvalue)
        self.data = dict(zip(self.table.fieldNames, values))

    def __str__(self):
        return str(self.data)

    def __getattr__(self, field):
        if type(field) == Field:
            field = field.name
        try:
            return self.data[field]
        except KeyError:
            raise AttributeError

    def __setattr__(self, field, value):
        if type(field) == Field:
            field = field.name
        if field in self.data:
            self.table.update({field: value},
                              {self.table._pkField: self.PKvalue})
            self.data[field] = value
        else:
            object.__setattr__(self, field, value)

    def children(self, foreignTable, where=None, fieldNames=None,
                 dontVisit=None, joinOrder=None, justPKvalues=False):
        qry = Query(self.table)
        if fieldNames is None:
            fields = foreignTable.primary_key
        else:
            fields = fieldNames
        qry.select('DISTINCT', fields)
        qry.where({self.table.primary_key: self.PKvalue})
        qry.where(where)
        rows = qry.fetchall()
        if justPKvalues:
            return map(lambda x: x[0], rows)
        return [Record(table=foreignTable, PKvalue=r[0]) \
                for r in rows]

    def parents(self, foreignTable, collapseSingleton=True,
                dontVisit=None, joinOrder=None):
        qry = Query(self.table)
        qry.select('DISTINCT', foreignTable.primary_key)
        qry.where({self.table.primary_key:self.PKvalue })
        rows = [Record(table=foreignTable, PKvalue=r[0]) \
                for r in qry.fetchall()]
        if len(rows) == 1 and collapseSingleton:
            return rows[0]
        return rows

    def relatives(self,tableFilter=None,dontVisit=None):
        """returns a set of records that are either
        parents or children."""
        p = self.parents(tableFilter, collapseSingleton=False,dontVisit=dontVisit)
        c =  self.children(tableFilter,dontVisit=dontVisit)
        rv = set()
        if type(p) == types.ListType:
            rv.update([r.PKvalue for r in p])
        if type(c) == types.ListType:
            rv.update([r.PKvalue for r in c])
        return [tableFilter[pk] for pk in rv]

    def update(self, data=None):
        """Update the fields of a record with the values
        in the data dictionary"""
        if data is None:
            data = {}
        
        try:
            for key in data.keys():
                self.__setattr__(key,data[key])
        except Exception, details:
            self.db.rollback()
            raise
        else:
            self.db.commit()

    def touchTimestamp(self):
        """Update the record's Timestamp to sql NOW()
        """
        try:
            qry = Query(self.table)
            qry.update({'Timestamp': Expression('NOW()')})
            qry.where({self.table.primary_key: self.PKvalue})
            cur = self.db.cursor()
            cur.execute(str(qry))
        except:
            self.db.rollback()
            raise
        else:
            self.db.commit()        

    def delete(self):
        """Deletes the record from the databases.
        """
        self.table.delete({self.table.primary_key: self.PKvalue})

    def __eq__ (self,rec2):
        if isinstance(rec2,self.__class__):
            if self.table != rec2.table or \
                   self.PKvalue != rec2.PKvalue:
                return False
            return True
        else:
            return False

    def __repr__(self):
        return str(self.data)

    def __getitem__(self, name):
        return self.data[name]


KSqlCache = {}
KSqlCacheIdx = {}


class KSqlObjectMeta(type):
    def __getattribute__(cls, name):
        if name.startswith('_'):
            return type.__getattribute__(cls, name)
        
        if '_table' in cls.__dict__ and name in cls._table.fields:
            return cls._table.fields[name]
        return type.__getattribute__(cls, name)

    def __setattr__(cls, name, value):
        if '_table' in cls.__dict__ and name in cls._table.fields:
            raise KdbomProgrammingError, ('Cannot assign values to a '
                                          'Field object')
        type.__setattr__(cls, name, value)


class KSqlObject(object):
    __metaclass__ = KSqlObjectMeta
    
    # TODO: DOC!!!!
    _table = None
    _tagTable = None
    _tagLinkTable = None
    _strField = None

    @classmethod
    def IDquery(cls):
        """return a select Query for underlying table's
        primary key field
        """
        rv=Query(cls._table)
        rv.selectID()
        return rv

    @classmethod
    def query(cls,dontVisit=[]):
        """return a query based on underlying table
        """
        if not dontVisit:
            dontVisit = cls._table.db.joinAvoidTables
        return Query(cls._table,dontVisit)


    @classmethod
    def builder (cls,things,dontVisit=[],returnQueries=False):
        """return a set containing members of class
        that are relatives of things.  Things can be a
        list, set, tuple or generator of KSqlObjects or
        a single such object.

        If returnQueries is True a list of query objects is
        returned.
        """
        if returnQueries:
            queries=[]
        else:
            ids=set()

        if type(things) not in (types.ListType,
                                types.TupleType,
                                type(set([])),
                                types.GeneratorType):
            things=(things,)


        for thing in things:
            if isinstance(thing, cls):
                if returnQueries:
                    q=cls.IDquery()
                    q.where({q._table.primary_key:thing.ID()})
                    queries.append(q)
                else:
                    ids.add(thing.ID())
                continue

            q=cls.IDquery()
            q.avoidTables(dontVisit)
            try:
                thing._table
            except AttributeError:
                raise ValueError, ("thing (type:%s) has no _table attribute"
                                   % type(thing))

            q.where({thing._table.primary_key:thing})
            if returnQueries:
                queries.append(q)
            else:
                ids.update([x[0] for x in cls.db().fetchall(q)])
        if returnQueries:
            return queries
        else:
            if len(ids) == 0:
                return set([])
            return set(cls.generator(ids))



    @classmethod
    def cloneAndReconnect(cls,db=None):
        """return a clone of this class connected via a different db's connection.
        id db is None (or if db is the current db) the current db is cloned. 
        """
        if db == cls.db() or db == None:
            db = cls.db().clone()

        class KSqlClone (cls):
            pass

        KSqlClone.setDB(db)

        return KSqlClone
        
    @classmethod
    def setDB(cls,db):
        """set class's attributes which are Table with db set to the currnet db
        to a table with the same name in new db specified.

        This is to avoid thread clashing on a single connection.

        """
        for k in dir(cls):
            v=getattr(cls,k)
            if isinstance(v,Table):
                if id(getattr(cls,k).db)==id(cls.db()):
                    setattr(cls,k,db[v.name])

    @classmethod                 
    def count(cls,*args,**kwargs):
        """the underlying table's count method.
        """
        return cls._table.count(*args,**kwargs)

    @classmethod
    def insert(cls,*args,**kwargs):
        """call the underlying table's insert method.
        """
        return cls._table.insert(*args,**kwargs)
        
    
    @classmethod
    def insertMany(cls,*args,**kwargs):
        """the underlying table's insertMany method.
        """ 
        return cls._table.insertMany(*args,**kwargs)


    def __new__ (cls,criteria={}, cacheOnInit=False, loadFromCache=False,
                  insertIfNeeded=False,insertOnly=False,**values):
        """Initialize new KSqlObject.
        """
        if criteria==0 or criteria is None:
            return None
        elif (loadFromCache and 
            cls in KSqlCacheIdx and
            criteria in KSqlCacheIdx[cls]):
            return KSqlCache[KSqlCacheIdx[self.__class__][str(criteria)]]
        elif criteria.__class__ == cls:
            return criteria
        else:
            return super(KSqlObject,cls).__new__(cls,criteria=criteria,
                                                 cacheOnInit=cacheOnInit,
                                                 insertIfNeeded=insertIfNeeded,
                                                 insertOnly=insertOnly,**values)          



    def __init__ (self,criteria={}, cacheOnInit=False, loadFromCache=False,
                  insertIfNeeded=False,insertOnly=False,**values):
        """Initialize a new object.  When called with no arguments a
        generic object is returned.  When called with None as the only
        argument None is returned.

        A single SQL record to be loaded can be specified in a number
        of ways by specifing the criteria argument, usually as the first
        positional argument rather than as a keyword argument:

        0) If a KSqlObject is provided the same object is returned.
        1) If a Record is provided it is used to make the new KSqlObject.
        2) An integer that is a primary key value of a record in the
           underlying table.
        3) A dictionary with FieldName:FieldValue equality pairs.
        4) A kdbom.Query that returns a single primary key value.
        5) A kdbom Condition that specifies a single record in the
           underlying table.
        6) A tuple of (qryText,(params)) that will return a single
           primary key value. e.g.
            ("SELECT `MY_ID` FROM `MY_TABLE` WHERE `X`>=%s AND `X`<=%s ",
            ('5','6'))
           The place holders used depend on the quoting engine for the 
           database connection.

        KdbomLookupError is raised when there are is no matching record
        (or when there are more than one), unless insertOnly or
        insertIfNeeded is True.

        When an object is initialized, it may be cached in memory,if
        cacheOnInit is True, for later use.  Cached objects are only
        used if loadFromCache is True.  In some applications this may
        cause a lot of memory use, but lookup is much faster.

        This interface can be used to insert new records in the underlying
        table; however, this is a very slow method that should never be
        used to do a batch insert. If a matching record is not found and
        insetIfNeeded is True kdbom tries to insert a record and if that
        suceeeds the matching KSqlObject is returned. If insertOnly is True
        and a matching record is found KdbomLookupError is raised, if no
        record is found kdbom tries to insert it and return the corresponding
        KSqlObject.  
        """

        if self._table != None:

            if 'SQLfields' not in self.__dict__:
                self._dirty = False
                self._record = None
                self._deleated = False
                self._toBeInserted = False
                self.SQLfields = {}

                # load if needed
                if criteria == {}:
                    criteria = values
                if criteria is None:
                    criteria = {}
                if criteria != {}:
                    if self._table == None:
                        raise KdbomUsageError, "Can't load SQL data. Table unknown."

                    try:
                        self.load(criteria=criteria)
                    except KdbomLookupError, e:
                        if not insertIfNeeded and not insertOnly:
                            raise
                        id_ = insert_KSqlObject(table=self._table,data=criteria)
                        self.load(id_)
                    else:
                        if insertOnly:
                            raise KdbomLookupError, "Can't insert, record exists"
                else:
                    for fieldName in self._table.field_names:
                        fieldObj = self._table.fields[fieldName]
                        if fieldObj == self._table.primary_key:
                            continue
                        self.SQLfields[fieldName] = fieldObj.default
                    if self._table.primary_key is not None:
                        self.SQLfields[self._table.primary_key.name] = None
                    self._toBeInserted = True
                    self._deleated = True
                    cacheOnInit = False

                self._init_criteria = criteria
                self.__post_init__()

                if cacheOnInit:
                    if self.__class__ not in KSqlCacheIdx:
                        KSqlCacheIdx[self.__class__]={}
                        KSqlCache[self.__class__] = []
                    else:
                        while len(KSqlCache[self.__class__]) >= KSqlCacheSize:
                            for k,v in KSqlCacheIdx[self.__class__].items():
                                if v == len(KSqlCache[self.__class__]):
                                    del KSqlCacheIdx[self.__class__][k]
                                    break
                                del KSqlCache[self.__class__][-1]
                            for k,v in KSqlCacheIdx[self.__class__].items():
                                KSqlCacheIdx[self.__class__][k] += 1

                    KSqlCache[self.__class__].insert(0,self)
                    KSqlCacheIdx[self.__class__][str(criteria)]= 0 

            else:
                # mark this object as not usable as an actual data container
                self._deleated = True
                
    def __post_init__(self):
        """This is a 'no op' called at the end of __init__.  Subclasses should
        define as necessary.
        """
        pass

    def load (self, criteria=None, **values):
        """load data from the SQL layer.
        See __init__ for allowable criteria.
        """
        if criteria is None:
            criteria = {}
        
        self._record=None
        if self._deleated:
            raise KdbomUsageError, "KSqlObject has been deleted"
        
        if self._table == None:
            raise KdbomDatabaseError, "Can't load SQL data. Table unknown."
        else:
            self._db = self._table.db

        # Different ways of specifing a record to base the object on
        #
        # kdbom.record object has been specified
        if isinstance(criteria, Record):
            if criteria.table != self._table:
                raise KdbomDatabaseError,\
                      "Can't make KSqlObject from kdbom record, tables do not match"
            self._record = criteria
            self.SQLfields = self._record.data

        # Primary Key value has been specified  
        elif type(criteria) in (types.IntType, types.LongType):  
            try:
                self._record = Record(table=self._table, PKvalue=criteria)
            except Exception, e:
                raise KdbomLookupError , "no matching MySQL record found"

        # _strField Lookup
        elif type(criteria) not in (types.DictType, types.TupleType,
                                    types.ListType) and \
                not isinstance(criteria, BaseCondition):
            pk = self.__class__.stringLookup(criteria)
            try:
                self._record = Record(table=self._table,PKvalue=pk)
            except:
                raise KdbomLookupError , (
                    "Record Not Found. Table: %s, Record Name: %s" %
                    (self.__class__,criteria))
            

        # a tuple like (SQL query,(params))
        # has been specified 
        elif ((type(criteria) == types.TupleType
               and len(criteria) ==2 and
               ( type(criteria[0])==types.StringType
                 and type(criteria[1])==types.TupleType) and
               (re.match(r'SELECT\s+`?%s`?'%
                         self._table.primary_key.name,criteria[0],
                         re.IGNORECASE) != None or 
                re.match(r'SELECT\s+`?%s`?\.`?%s`?'%
                         (self._table.name,self._table.primary_key.name),criteria[0],
                         re.IGNORECASE) != None))):
                # it is assumed that we have been passed a tuple with 
                # (Qry, params)
                cur = self._db.cursor()
                rowOne = cur.fetchone(criteria[0],criteria[1])
                if type(rowOne) != types.NoneType:
                    self._record=Record(table=self._table,PKvalue=rowOne[0])                
        
        # a dictionary of field:value pairs or fcn.function arguments
        # like field=values have been specified
        else:
            if criteria == {}:
                criteria=values
            try:
                self._record = self._table(
                    criteria=criteria,expectOneRecord=True)[0]
            except IndexError:
                pass

        # record has been found, or maybe not
        if type(self._record) == types.NoneType:
            raise KdbomLookupError , "no matching MySQL record found"
        else:
            self.SQLfields = self._record.data
            self.update = self._record.update

    @classmethod
    def stringLookup(cls,toMatch):
        """Return a new KSqlObject by matching the _strField to the
        given toMatch argument.  None is returned if no match is
        found.
        """
        if cls._strField == None:
            if 'Name' in cls._table.fields:
                    cls._strField=cls._table.Name

        try:
            return cls._table(criteria={cls._strField.name:toMatch},
                              expectOneRecord=True,
                              returnRecords=True)[0].PKvalue
        except IndexError:
            return None

    @classmethod
    def exists(cls, criteria=None, **values):
        """Check SQL layer to see if specified object exists.
        Returns True or False accordingly.
        See __init__ for allowable criteria.
        """
        if criteria is None:
            criteria = {}
        
        if cls._table == None:
            raise KdbomDatabaseError, "Can't load SQL data. Table unknown."
        else:
            cls._db = cls._table.db

        # Different ways of specifing a record to base the object on
        #
        # kdbom.record object has been specified
        if isinstance(criteria, Record):
            if criteria.table != cls._table:
                # tables dont match
                return False

        # Primary Key value has been specified  
        elif type(criteria) in (types.IntType, types.LongType):  
            try:
                cls._table[criteria]
            except IndexError:
                return False
            else:
                return True

        # _strField Lookup
        elif type(criteria) not in (types.DictType, types.TupleType, types.ListType):
            try:
                pk = cls.stringLookup(criteria)
            except KdbomLookupError:
                return False
            if pk is None:
                return False
            else:
                return True
            

        # a tuple like (SQL query,(params))
        # has been specified 
        elif ((type(criteria) == types.TupleType
               and len(criteria) ==2 and
               ( type(criteria[0])==types.StringType
                 and type(criteria[1])==types.TupleType) and
               (re.match(r'SELECT\s+`?%s`?'%
                         cls._table.primary_key.name,criteria[0],
                         re.IGNORECASE) != None or 
                re.match(r'SELECT\s+`?%s`?\.`?%s`?'%
                         (cls._table.name,cls._table.primary_key.name),criteria[0],
                         re.IGNORECASE) != None))):
                # it is assumed that we have been passed a tuple with 
                # (Qry, params)
                cur = cls._db.cursor()
                rows = cur.fetchall(criteria[0],criteria[1])
                if len(rows) == 1:
                    return True
                else:
                    return False
        
        # a dictionary of field:value pairs or fcn.function arguments
        # like field=values have been specified
        else:
            if criteria == {}:
                criteria=values
            try:
                cls._table(criteria=criteria,
                           expectOneRecord=True)[0]
            except IndexError:
                return False
            else:
                return True

        return False




    def __str__ (self):
        """Returns object's Name (contents of _strField) from atributes
        or from SQLfields. If None is in the field, __SQL_NULL__ is
        returned.  If you have foolishly specified a non string field as
        the _strField, a conversion to string will be attemped.
        """
        strField = self._strField
        if strField == None:
            if 'Name' in self._table.fields:
                strField=self._table.Name
                
        if strField != None:
            rv = self.SQLfields[strField.name]
            if type(rv) == types.StringType:
                return rv
            if rv == None:
                return '__SQL_NULL__'
            else:
                return str(rv)
        else:
            return str(self.SQLfields)

    def __int__(self):
        """returns the primary key value as an integer
        """
        return int(self.ID())

    def store (self):
        """store back to the database.
        save() and store() are the same method.
        This method does not commit the change!
        """
        if self._toBeInserted:
            id_ = insert_KSqlObject(table=self._table,
                                    data=self.SQLfields)
            self._toBeInserted = False
            self._deleated = False
            data = self.SQLfields
            self.SQLfields = {}
            try:
                self.load(id_)
            except:
                self.SQLfields = data
                
        if self._record != None and self._dirty:
            try:
                self._record.update(self.SQLfields)
                self._dirty = False
            except Exception, e:
                raise KdbomDatabaseError, (
                    "SQL update failed for the following table and data-\n%s.%s:%s\n\n%s" %
                    (self._db.name,self._table.name,self.SQLfields, str(e)))

    save = store

    def ID(self):
        """Return the value of the primary key of the underlying
        MySQL record.
        """
        try:
            return self._record.PKvalue
        except:
            return None

    @classmethod
    def table(self):
        """ return the object's Table object.
        """
        return self._table
    
    @classmethod
    def db (self):
        """Return the object's Db object.
        """
        return self._table.db
    
    def touchTimestamp(self):
        """Update timestamp of underlying record
        """
        self._record.touchTimestamp()
        self.load(self.ID())

    def __getattr__ (self,name):
        """Return a value from SQL fields if possible.
        """
        if type(self) == types.TypeType:
            return self._table.__getattr__(name)


        if 'SQLfields' not in self.__dict__:
            raise KdbomProgrammingError, "Object has no SQLfields"

        if type(name) is Field:
            name = name.name
        if name in self.SQLfields:
            return self.SQLfields[name]
        else:
            raise AttributeError, str(name)

    def __setattr__ (self,name,value):
        """Set value that will be stored back when the object's store
        method is called.  Error checking is deferred until the store call.

        Remember all the persistent values are capitalized!
        (if our MySQL naming conventions are followed)
        """
        if type(name) is Field:
            name = name.name
        try:
            if name in self.SQLfields:
                self.SQLfields[name] = value
                self._dirty = True
            else:
                object.__setattr__(self, name, value)
        except:
            object.__setattr__(self, name, value)

    def __eq__ (self,other):
        """Instances are equal if their underlying record is the same and
        neither are dirty.
        """
        if other.__class__ != self.__class__:
            return False

        if (self._record == other._record):
            if (not (self._dirty or other._dirty)):
                return True
        return False

    def mostlyEqual(self,other):
        """Returns true if class is the same and SQLfields are equal ignoring
        the primary Key and any timestamp (if the timestamp field is named "Timestamp"
        """
        myFields=copy(self.SQLfields)
        otherFields = copy(other.SQLfields)

        myFields['Timestamp']=None
        myFields[self._table.primary_key.name]=None

        otherFields['Timestamp']=None
        otherFields[self._table.primary_key.name]=None

        return myFields==otherFields

    def __cmp__ (self,other):
        return cmp(str(self),str(other))

    def __ne__ (self,other):
        return not self.__eq__(other)

    def __hash__ (self):
        """return a UID for this instance based on db, db host, db port, table
        and primary key.
        """
        # shift each element of hash
        host = hash(self._record.db.host) >> 4
        port =  hash(self._record.db.port) << 3
        db = hash( self._record.db.name)  >> 2
        table = hash(self._record.table.name) >> 1 
        pk = hash(int(self.ID()))

        return pk^table^db^port^host

    def __getstate__(self):
        """Return a picklable dictonary that __setstate__ uses to restore
        a serialized KSqlObject.
        """
        if self._deleated:
            return None
        
        rv = { 'ID' : self.ID() }
        for k,v in self.__dict__.items():
            if k in ('_db','_record','_strField'):
                continue
            if type(v) == types.MethodType:
                continue
            rv[k]=v
        return rv

    def __setstate__(self,state):
        """rebuild instance from __getstate__ data.
        """
        self._record=self._table[state['ID']]
        del state['ID']
        self._db=self._table.db
        self._strField = self.__class__._strField

        for k,v, in state.items():
            self.__dict__[k] = v

    def detach(self):
        """create detached version of this object:
        _db, _table, _record, _strField are not included
        in the new version.  Database layer methods will
        fail with the returned object.
        """
        dObj=cPickle.loads(cPickle.dumps(self))
        dObj._table=None
        
        return dObj

    def delete(self):
        """delete the SQL record for this object and make the object unuseable.
        """
        self._dirty = False
        self.SQLfields = {}
        if not self._deleated:
            self._delete()
            self._deleated = True
        self._record = None        
        
    def _delete(self):
        """handle the SQL part of the delete.  Some sublasses should define to
        take care of child records.
        """
        self._record.delete()

    @classmethod
    def generator(cls,*params,**kwValues):
        """Returns a generator of objects of the same class.
        This is currently a wrapper for the module global function
        ksqlobject_generator.  See that doc string for calling
        information.
        """
        return ksqlobject_generator(cls,*params,**kwValues)

    @classmethod
    def randomInstance(cls,criteria=None,**Values):
        """Returns a randomly selected instance of this class.
        subject to criteria and Values.
        """
        qry = Query(cls._table)
        qry.where(criteria)

        qry.selectCount()
        count=qry.fetchall()[0][0]

        qry.resetSelect()
        qry.selectID()
        qry.limit(1,random.randint(1,count))

        pkValue = qry.fetchall()[0][0]

        return cls(pkValue)

    @classmethod
    def friendOf(cls,friend):
        """Return a generator of instances of this class referenced by friend.
        friend must be a KSqlObject.  Friend can be a KSqlObject, or a list
        or tuple of them.  They can't be iterators more generally than that.
        If friend is a sequence a list of generators is returned.
        "Bosom friends" have their primary keys as foreign keys in this table,
        otherwise None is returned.
        """
        if type(friend) in (types.ListType,types.TupleType):
            return [cls.friendOf(f) for f in friend ]
        else:
            if not isinstance(friend,KSqlObject):
                raise ValueError, (
                    "friend must be a KSqlObject.  %s ,given instead" %
                    friend.__class__)

            if cls._table.primary_key.name in friend._table.fields.keys():
                # real friends have their primary keys as foreign keys
                # in this table.
                keyName = cls._table.primary_key.name
                return cls.generator(criteria={keyName:friend.SQLfields[keyName]})
            else:
                return None

    @classmethod
    def tableIDStrDict(cls):
        """Return a dictionary of pkValue:_strField value pairs
        from the underlying table.
        """
        if cls._strField != None:
            return cls._table.idValueDict(cls._strField)
        else:
            return {}

    @classmethod
    def tableIDStrTuples(cls,orderBy=''):
        """return a tuple of (pkValue,_strField value) tuples.  By default
        they are ordered by the _strField values, set orderBy=None for
        arbitrary ordering.
        """
        if orderBy == '':
            orderBy == cls._strField
        return cls._table.idValueTuples(cls._strField,orderBy=orderBy)


    def children(self,foreignClass):
        """Return child KSqlObjects.
        """
        rv = {}

        foreignTable = foreignClass._table

        for c in self._record.children(foreignTable):
            rv[foreignClass(c)]=None
        return rv.keys()

    def parents(self,foreignClass):
        """Return parent KSqlObjects.
        """
        rv = {}

        foreignTable = foreignClass._table
        
        for c in self._record.parents(foreignTable,collapseSingleton=False):
            rv[foreignClass(c)]=None
        return rv.keys()

    def relatives(self,foreignClass,dontVisit=[]):
        """Return child and parent KSqlObjects.
        """
        rv = {}

        foreignTable = foreignClass._table

        for c in self._record.relatives(foreignTable,dontVisit=dontVisit):
            rv[foreignClass(c)] = None
        return rv.keys()

    def applyTag(self, tagStr):
        """Apply tag to object, inserting into database.
        Changes are pushed to database layer _immediately_.
        """
        try:
            flagID =self._tagTable(Name=tagStr,
                                   returnRecords=True)[0].PKvalue
        except IndexError:
            flagID = self._tagTable.insert(Name=tagStr)
            self._tagLinkTable.insert({self._table.primary_key: self.ID(),
                                       self._tagTable.primary_key: flagID})
        else:
            ct = self._tagLinkTable.count({self._table.primary_key: self.ID(),
                                           self._tagTable.primary_key: flagID})
            if ct == 0L:
                self._tagLinkTable.insert({self._table.primary_key: self.ID(),
                                           self._tagTable.primary_key: flagID})

    def hasTag(self,tagStr):
        """True if this object had the tag, otherwise False.
        """
        try:
            flagID = self._tagTable(Name=tagStr,
                                    returnRecords=True)[0].PKvalue
            ct = self._tagLinkTable.count({self._table.primary_key: self.ID(),
                                           self._tagTable.primary_key: flagID})
            if ct >= 1:
                return True
            else:
                return False
        except IndexError:
            return False

    def listTags(self):
        """
        """
        rv = []
        for tagRec in self._tagLinkTable({self._table.primary_key: self.ID()},
                                         returnRecords=True):
            pkName = self._tagTable.primary_key.name
            rv.append(self._tagTable[tagRec[pkName]].Name)
        return rv
    
    tags=listTags

    def unlinkTag(self,tagStr):
        """Remove the tag from the object.
        Changes are pushed to db _immediately_.
        """
        try:
            flagID = self._tagTable(Name=tagStr,
                                    returnRecords=True)[0].PKvalue
            foreignField1 = getattr(self._tagLinkTable,
                                    self._table.primary_key.name)
            foreignField2 = getattr(self._tagLinkTable,
                                    self._tagTable.primary_key.name)
            ct = self._tagLinkTable.delete({foreignField1: self.ID(),
                                            foreignField2: flagID})
            if ct >= 1L:
                return True
            else:
                return False
        except Exception, e:
            return False

    def unlinkAllTags(self):
        """Remove all tags from the object.
        Changes are pushed to db _immediately_.
        """
        foreignField = getattr(self._tagLinkTable,
                               self._table.primary_key.name)
        self._tagLinkTable.delete({foreignField: self.ID()})

    @classmethod
    def taggedSelectSQL(cls, includeTags=None,excludeTags=None,
                        count=False,extraWheres=[]):
        """Returns sql code for selection of primary keys of
        objects which are tagged with the include tags
        but do not have all of the exclude tags.  The tags should
        be given strings or as lists of strings.

        If count is true the count of distinct IDs will be selected.
        """
        if includeTags is None:
            includeTags = []
        if excludeTags is None:
            excludeTags = []
        
        if type(includeTags) in types.StringTypes:
            includeTags = (includeTags,)
        if type(excludeTags) in types.StringTypes:
            excludeTags = (excludeTags,)

        inIDs = [cls._tagTable(Name=tagStr)[0][0] \
                 for tagStr in includeTags]
        outIDs = [cls._tagTable(Name=tagStr)[0][0] \
                  for tagStr in excludeTags]

        tagCount = len(includeTags) + len(excludeTags)

        query = cls._table.query()
        if count:
            query.select(COUNT(DISTINCT(cls._table.primary_key)))
        else:
            query.selectDistinct(cls._table.primary_key)

        includeCondition = OrCondition()
        aliasIdx = 0
        for inID in inIDs:
            aliasName = 'tht%d' % aliasIdx
            aTable = cls._tagLinkTable |AS| aliasName
            aField1 = getattr(aTable, cls._table.primary_key.name)
            aField2 = getattr(aTable, cls._tagTable.primary_key.name)
            query.leftJoin(aTable,
                           aField1 == cls._table.primary_key)
            includeCondition.append(aField2 == inID)
            aliasIdx += 1

        excludeCondition = AndCondition()
        for outID in outIDs:
            aliasName = 'tht%d' % aliasIdx
            aTable = cls._tagLinkTable |AS| aliasName
            aField1 = getattr(aTable, cls._table.primary_key.name)
            aField2 = getattr(aTable, cls._tagTable.primary_key.name)
            query.leftJoin(aTable,
                           ((aField1 == cls._table.primary_key) &
                            (aField2 == outID)))
            excludeCondition.append(IsNullCondition(aField2))
            aliasIdx += 1

        query.where(includeCondition & excludeCondition)
        if extraWheres:
            query.whereLiteral(' AND '.join(extraWheres))

        return query

    @classmethod
    def idsByTag(cls, includeTags=None,excludeTags=None,showQuery=False):
        """Returns a generator of primary keys of objects which
        are tagged with the include tags but do not have the
        exclude tags.  The tags should be given strings or as
        lists of strings.
        
        The underlying query will be printed if showQuery is True.
        """

        qry=cls.taggedSelectSQL(includeTags=includeTags,excludeTags=excludeTags)
        if showQuery:
            print qry
        return ( r[0] for r in cls.db().fetchall(qry))


    @classmethod
    def getByTag(cls, includeTags=None,excludeTags=None,showQuery=False):
        """Returns a generator of objects which are tagged with the include tags
        but do not have the exclude tags.  The tags should be given strings or
        as lists of strings.
        
        The underlying query will be printed if showQuery is True.
        """

        qry=cls.taggedSelectSQL(includeTags=includeTags,excludeTags=excludeTags)
        if showQuery:
            print qry
        return ksqlobject_generator(cls,qry)

    @classmethod
    def countTagged(cls, includeTags=None,excludeTags=None,showQuery=False):
        """Returns the number of objects which are tagged with the include tags
        but do not have the exclude tags.  The tags should be given strings or
        as lists of strings.
        
        The underlying query will be printed if showQuery is True.
        """
        qry=cls.taggedSelectSQL(includeTags=includeTags,
                                excludeTags=excludeTags,
                                count=True)
        if showQuery:
            print qry
        return cls.db().fetchall(qry)[0][0]
        

    @classmethod
    def knownTags(cls):
        """Returns a list of tag names in the tag table.
        """
        return [r.Name for r in cls._tagTable.records() ]

    @classmethod
    def checkTags(cls,tags):
        """Add tags that do not already exist to the tag table. 
        """
        inTags=set(tags)
        newTags=inTags.difference(set(cls.knownTags()))
        if len(newTags)>0:
            cls._tagTable.insertMany(('Name',),newTags)

    @classmethod
    def tagId(cls,tagStr):
        """Returns the ID of the given tag.
        """
        return cls._tagTable(Name=tagStr,
                             returnRecords=True)[0].PKvalue

    @classmethod
    def tagIDdict(cls):
        """return a dict of (ID,tag) pairs.
        """
        return dict( (r.Name,r.PKvalue) for r in cls._tagTable.records())


    @classmethod
    def iterator(cls):
        """Iterate over all instances
        """
        for x in cls._table.PKvalues():
            yield cls(x)

    def __getitem__(self, name):
        """Returns the value in the field `name`
        """
        if len(self.SQLfields) == 0:
            raise KdbomError('KSqlObject record unusable.')

        return self.SQLfields[name]

    def __setitem__(self, name, value):
        """Sets the field `name` to `value`
        """
        if len(self.SQLfields) == 0:
            raise KdbomError('KSqlObject record unusable.')
        
        self.SQLfields[name] = value
        self._dirty = True

    @classmethod
    def makeLikeMe(cls,*others):
        """if others are not an instance of self.__class__
        return an object of the same class based on the data in other.
        Others can be just one thing, or an iterable.  If it is a one thing
        it is transformed and returned, otherwise a list is returned.

        If other is already this class it is returned but not copied. 
        """

        if len(others) != 1:
                return [cls.makeLikeMe(o) for o in others]
        else:
            other=others[0]
            
            if isinstance(other,cls):
                return other
            else:
                try:
                    return cls(other)
                except:
                    raise KdbomError, ("Couldn't make %s instance from %s: %s"
                                       % (cls,other.__class__,str(other)[:200]))

def autoClassArgs(func):
    """A decorator for bound methods of KSqlObject subclasses.
    Changes positional arguments into the the class of the bound
    object, using KSqlObject.makeLikeMe.
    """
    @functools.wraps(func)
    def wrapper(*args,**kwds):
        boundObj = args[0]
        cls = boundObj.__class__
        myArgs=[]
        for arg in args[1:]:
            myArgs.append(cls.makeLikeMe(arg))
        return func(boundObj,*myArgs,**kwds)
    return wrapper


@kdbom2.concat_docstring_from_db
def tryDBconnect(db=None,serversPorts=None,user=None,
                 reuseDBconnection=None,getTables=True,tryLocalPort=3306,
                 fatal=False,verboseFailure=False,debugConnect=False,
                 autocommit=True, **kwds):
    """Try to connect to a database and return the db instance.  One or more server/port
    combinations can be tried.  If no connection is successful a dummy object is returned
    the dummy database will return None for all atribute and dict/key lookups,
    unless fatal is true in which case KdbomDatabaseError is raised.

    db = databse name (string)
    server = servers hostname (string)
    port = server listening port (int)
          
    After all given (server,port) combinations are tried one or more localhost
    connections over port tryLocalPort will be attempted unless that is set
    to None. tryLocalPort can be an integre type or a sequence of integers.

    serversPorts should be a list of (server,port) tuples, or a single 2-tuple.
    serversPorts can also be a single string to make replacement of db()
    calls more straight forward (defaultPort is then used for the port).

    If reuseDBconnection is specified, this is just a call to the db
    class constructor.

    ########################################################
    # Actual database connection details are passed to the #
    # driver DB.__init__ method as **kwds.                 #
    #                                                      #
    # __doc__ for that constructor follows, if driver has  #
    # been set.                                            #
    ########################################################
    """

    if debugConnect:
        import sys
        print >> sys.stderr, {'db':db,
                              'serversPorts':serversPorts,
                              'user':user,
                              'reuseDBconnection':reuseDBconnection,
                              'getTables':getTables,
                              'tryLocalPort':tryLocalPort,
                              'fatal':fatal,
                              'verboseFailure':verboseFailure,
                              'debugConnect':debugConnect}

    if serversPorts is None:
        serversPorts = []
    # for backwards compatability
    if reuseDBconnection == None and 'reuseDB' in kwds:
        reuseDBconnection = kwds['reuseDB']

    connected = False
    database = None

    if reuseDBconnection != None:
        database = DB(db=db, reuseDBconnection=reuseDBconnection,
                      getTables=getTables, autocommit=autocommit)
        if database != None:
            connected = True
    else:
        if type (serversPorts) in types.StringTypes:
            serversPorts = [(serversPorts, 3306)]
        if type(serversPorts) != types.ListType:
            if type(serversPorts) is None:
                serversPorts = []
            elif (len(serversPorts) == 2
                and type(serversPorts[0]) in types.StringTypes
                and type(serversPorts[1]) in (types.IntType,types.LongType)):
                serversPorts = [serversPorts]
            elif type(serversPorts) == types.TupleType:
                serversPorts = list(serversPorts)
            else:
                raise ArgumentError, ("serversPorts should be a list of tuples not: %s" %
                                      serversPorts)
        try:
            for p in tryLocalPort:
                serversPorts.append(('localhost',p))
        except TypeError:
            serversPorts.append(('localhost',tryLocalPort))

        for host,port in serversPorts:
            if not connected:
                try:
                    database = DB(db=db, host=host, port=port, debug=debugConnect,
                                  user=user, getTables=getTables,
                                  autocommit=autocommit, **kwds)
                    connected = True
                except KdbomDatabaseError, e:
                    connected = False
                    raise e
                except Exception, e:
                    connected = False

    if not connected:
        if not verboseFailure:
            raise KdbomDatabaseError, "%s DB not loaded" % db
        else:
            raise
    return database


def tryDBcreate(dbname, **kwargs):
    """Creates a new database `dbname` using the appropiate driver
    method and returns a DB instance of the new database.
    """
    emptyDbConn = tryDBconnect(**kwargs)
    return emptyDbConn.createDatabase(dbname)
    

def ksqlobject_generator(subclass,query=None,params=(),db=None,criteria=None,
                         whereLiteral='',cache=False,**values):
    """Return a generator for KSqlObjects. query can be either:
    1) an SQL query string that returns the primary key values (note the
    params tuple will be passed to the database API in the usual way or
    2) a list/tuple of primary key values.

    The query (if any) will be executed using a cursor derived from db
    (a kdbom db instance) or if db=none the database of the  subclasse's
    underlying table.

    If query is none and there are no **Values all the records in the subclass's
    _table will be used, but if **Values are specified, a simple query wil be built
    automatically. See docs for kdbom.Table._buildSelect and __call__.

    """
    #whereLiteral=whereLiteral.replace('_','\_')  # not needed ?
    whereLiteral=whereLiteral.replace('%','%%%%') # needed to protect from python

    if criteria is None:
        criteria = values
    else:
        criteria.update(values)

    if isinstance(query, BaseCondition):
        qry = subclass.IDquery()
        qry.where(query)
        query = str(qry)
        
    elif type(query) == types.NoneType or \
         (not isinstance(query, Query) and len(query)==0):
        query = subclass.IDquery()
        query.where(criteria, whereLiteral)
        query = str(query)

    if type(query) in types.StringTypes or \
           isinstance(query,Query):
        if db != None:
            cur = db.cursor()
        else:
            cur = subclass._table.db.cursor()

        for row in cur.fetchall(query, params):
            yield subclass(row[0])
    else:
        for PKvalve in getIterable(query):
            if cache and (subclass,PKvalve) in KSqlCache:
                yield KSqlCache[(subclass,PKvalve)]
            else:
                o=subclass(PKvalve)
                if cache:
                    KSqlCache[(subclass,PKvalve)]=o
                yield o

def ksqlobject_list(subclass, query, params=(), db=None):
    return list(ksqlobject_generator(subclass, query, params=params,
                                     db=db))

def insert_KSqlObject(table=None,mustHave=(),
                       data=None,**values):
    if data is None:
        data = {}
        
    data.update(values)
    for needs in mustHave:
        if needs not in data:
            raise KdbomDatabaseError , needs + ' required'

    for k,v in data.items():
        if isinstance(v,KSqlObject):
            data[k]=int(v)
        elif isinstance(v, Record):
            data[k] = v.PKvalue

    _id = table.insert(data)
    table.db.commit()
    return _id

class DetachedKSqlObject(KSqlObject):
    """KSqlObject class with the SQL parts stripped out.
    _table is always 
    """
    _table=None

    def __init__(self,**kwargs):
        """
        """
        for k,v in kwargs.items():
            self.__dict__[k]=v


    def ID(self):
        """
        """
        return self.__dict__['_attachment_ID']


    def attach(self,cls):
        """get the version of this from SQL layer
        using the KSqlObject class specified as cls.
        """
        return cls(self.ID())

    def __hash__(self):
        # shift each element of hash
        return  hash(str(self.SQLfields))


