"""Kael's Database Object Model - Version 2 (kdbom2)
We pronounce it: Ka-Boom-Too.

kdbom2 is a package for mapping relational database objects into python objects.  Database schema,
tabels, fields, rows and queries are all represented in the kdbom2 set of classes.

Similar projects include: SqAlchemy,<more>

Here are a few intersing features of kdbom2:
* Kdbom2 code is portable to different database backends. (driver dependant. see:kdbom2.drivers)
* Relationships between tables are discovered automatically based on FOREIGN KEYs and CONSTRAINTs.
* SQL JOIN paths are automatically generated when a query requires them.
* Queries can be built using only python.
* Queries can be modulaly build and modified.
* Conditions can written in python and combined using python '&' and '|',
e.g. ((FieldX > 4000) & (FieldY == 'Big Boss Man'))|(FieldY == 'Danger Man') is a valid compound contition and kdbom2's Query class will render it in SQL properely in a WHERE clause or in the field list section of a SELECT query. 
* There is a rich KSqlObject class with instances corresponding to single data rows.
* When large numbers of operations are required, the rich classes can be bypassed, and kdbom2 helps you use faster cursor operations like 'executemany'

Summary of kdbom2 modules and sub packages:
* adaptor - small module to generate SQL CREATE TABLE statements given kdbom.Table objects.
* drivers - SQL abstraction for different database backends.
* exceptions - kdbom2.exception classes.
* kdbom - database (Db), Table, Field, Row, KSqlObject classes.  Connection and data retrevial and manipulation convience functions.
* query - the Query object lives here (a curious module).
* util - a subset of the Fischer Lab utility module that kdbom2 uses.
* tests - the test suite.

We hope you investigate kdbom2 and find it useful.

More information:
Documentation -
Soucre Code Repositiony -
Source Code Standards:
    Our conventions are based PEP 8, with mixedCase variable, function and method names, and CapWord class names.  Module level constants are usualy UPPER_CASE_WITH_UNDERSCORES.  Four spaces are used for indentations.
    To distinguish python and SQL names, our lab uses SQL table and field names that are Capitalized_Words_With_Underscores.  I recommend using a system like that to help keep the namespaces straight. 
    


Send Feedback To:
kdbom2@kael.net

License:




"""
__version__ = tuple([int(x) for x in
                     '$Revision: 1.4 $'.split()[1].split('.')])
__author__ = "Kael Fischer and Julio Carlos Menendez"

del x



import kdbom2.drivers.mysql
driver = kdbom2.drivers.mysql

def setDriver(driverModule):
    driver = driverModule


def concat_docstring_from_db(f):
    newdocstring = getattr(f, '__doc__')
    newdocstring += '\n\nCurrently using %s driver.\n' % driver.NAME
    newdocstring += getattr(driver.DB.__init__, '__doc__')
    setattr(f, '__doc__', newdocstring)
    return f

